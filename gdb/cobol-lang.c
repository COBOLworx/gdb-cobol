/*
 * Copyright (c) 2021-2023 Symas Corporation
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following disclaimer
 *   in the documentation and/or other materials provided with the
 *   distribution.
 *   distribution.
 * * Neither the name of the Symas Corporation nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
   Portions of this software were derived from pre-existing code in GDB,
   which contained this notice:

   Copyright (C) 1992-2022 Free Software Foundation, Inc.

   This file is part of GDB.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

#include "defs.h"

#include <ctype.h>

#include "block.h"
#include "c-lang.h"
#include "c-lang.h"
#include "charset.h"
#include "cp-support.h"
#include "demangle.h"
#include "gdbarch.h"
#include "infcall.h"
#include "objfiles.h"
#include "psymtab.h"
#include "cobol-lang.h"
#include "typeprint.h"
#include "valprint.h"
#include "varobj.h"
#include <algorithm>
#include <string>
#include <vector>
#include "cli/cli-style.h"
#include "parser-defs.h"
#include "cobol-parse.h"
#include "assert.h"

/* Single instance of the Cobol language class.  */
static cobol_language cobol_language_defn;

/* See language.h.  */

void
cobol_language::printstr(struct ui_file *stream, struct type *type,
                         const gdb_byte *string, unsigned int length,
                         const char *user_encoding, int force_ellipses,
                         const struct value_print_options *options) const
  {
  // This is slightly dangerous, I suppose.  But if somebody did x/s VAR, then
  // VAR should probably be sv_recent_bysm.  And we need it, because our
  // alphanumerics are not nul-terminated, and we need the capacity:
  block_symbol bsym = cobol_language::get_recent_bsym();
  struct value *value = value_of_variable (bsym.symbol, bsym.block);
  cobol_value_field field(value);
  const char *encoding = ENCODING;
  generic_printstr( stream, type, field.data(), field.capacity(), encoding,
                    force_ellipses, '"', 0, options);
  }

/* See cobol-lang.h.  */

#include <map>

void
cobol_language::val_print_struct
(struct value *val, struct ui_file *stream, int recurse,
 const struct value_print_options *options) const
  {
  fprintf(stderr, "%s(): We don't know what to do\n", __func__);
  assert(false);
  }

/* See cobol-lang.h.  */

void
cobol_language::print_enum(struct value *val, struct ui_file *stream,
                           int recurse,
                           const struct value_print_options *options) const
  {
  fprintf(stderr, "%s(): We don't know what to do\n", __func__);
  assert(false);
  }

static const struct generic_val_print_decorations cobol_decorations =
  {
  /* Complex isn't used in Cobol, but we provide C-ish values just in
     case.  */
  "",
  " + ",
  " * I",
  "true",
  "false",
  "()",
  "[",
  "]"
  };

void
cobol_language::value_print(struct value *val,
                            struct ui_file *stream,
                            const struct value_print_options *options) const
  {
  struct type *type = check_typedef(val->type());
  struct gdbarch *gdbarch = type->arch();
  cblc_refer_t *val_refer = (cblc_refer_t *)val->get_auxdata();

  const char *type_name = type->name() ? type->name() : "<unknown>";

  if( type->is_pointer_or_reference() )
    {
    // Arrive here when somebody commanded "print &var1"

    gdb_printf(stream, "(");
    type_print(val->type(), "", stream, -1);
    gdb_printf(stream, ") ");

    const gdb_byte *valaddr = val->contents_for_printing().data();
    CORE_ADDR addr = unpack_pointer(type, valaddr);
    fputs_styled(paddress(gdbarch, addr), address_style.style(), stream);
    print_address_symbolic(gdbarch, addr, stream, asm_demangle, " ");
    }
  else
    {
    value_print_options vpo_options = *options;
    value_print_options *our_options = &vpo_options;

    int format = our_options->format;
    if( options->raw )
      {
      // For some reason, when print/r is specified after, say, print/p, the
      // options->format value is still 'p'.  This can mess up the GDB system
      // calls in, say, print_val_as_list, which takes options as a parameter.
      
      // So, when dealing with options->raw, we set -> format to \0
      format = 'r';
      our_options->format = '\0';
      }

    if( val_refer && val_refer->address_of )
      {
      // val_refer->address_of was because of the ampersand in "print
      // &cobol-var".
      format = 'a';
      } 

    switch(format)
      {
      case 'a':
        if( strcmp(type_name, "cblc_field_t") == 0 )
          {
          print_data_address(val, stream, options);
          if( !val_refer->address_of )
            {
            unsigned int print_max_chars = get_print_max_chars(options);
            size_t len = 0;
            cobol_value_field field(val);
            size_t chars = 0;
            while(len < field.capacity())
              {
              gdb_printf(stream, " %2.2x", field.data()[len]);
              chars += 3;
              len += 1;
              if(chars > print_max_chars)
                {
                break;
                }
              }
            if( len != field.capacity() )
              {
              gdb_printf(stream, "...");
              }
            }
          }
        else
          {
          gdb_printf("cobol_language::value_print(): That variable "
                     "is of type %s, which we don't know how to handle\n",
                     type_name);
          }
        break;

      case 'r':
        {
        if( strcmp(type_name, "cblc_field_t") == 0 )
          {
          print_val_as_list(val, stream, our_options);
          }
        else
          {
          gdb_printf("cobol_language::value_print(): That variable "
                     "is of type %s, which we don't know how to handle\n", type_name);
          }
        break;
        }

      case 'p':
        {
        if( strcmp(type_name, "cblc_field_t") == 0 )
          {
          print_cblc_field_t_heirarchical(val, stream, options);
          }
        else
          {
          gdb_printf("cobol_language::value_print(): That variable "
                     "is of type %s, which we don't know how to handle\n", type_name);
          }
        break;
        }

      default:
        {
        // We only know how to print COBOL cblc_field_t variables:

        if( strcmp(type_name, "cblc_field_t") == 0 )
          {
          print_cblc_field_t(val, stream, options);
          }
        else if( strcmp(type_name, "cblc_string") == 0 )
          {
          print_string(val, stream, options);
          }
        else
          {
          c_value_print(val, stream, options);
          }
        break;
        }
      }
    }
  return;
  }

/* See language.h.  */
#include <err.h>
void
cobol_language::value_print_inner
(
 struct value *val, struct ui_file *stream, int recurse,
 const struct value_print_options *options) const
  {
  struct type *type = check_typedef(val->type());
  const char *type_name = type->name() ? type->name() : "<unknown>";

  if( strcmp(type_name, "cblc_field_t") == 0 )
    {
    print_cblc_field_t(val, stream, options);
    return;
    }

  struct value_print_options opts = *options;
  opts.deref_ref = 1;

  if(opts.prettyformat == Val_prettyformat_default)
    opts.prettyformat =(opts.prettyformat_structs
                        ? Val_prettyformat : Val_no_prettyformat);

  switch(type->code())
    {
    case TYPE_CODE_INT:
      /* Recognize the unit type.  */
      if(type->is_unsigned() && type->length() == 0
          && type->name() != NULL && strcmp(type->name(), "()") == 0)
        {
        gdb_puts("()", stream);
        break;
        }
      goto generic_print;

    default:
    generic_print:
      generic_value_print(val, stream, recurse, &opts, &cobol_decorations);
    }
  }

/* A helper function for UNOP_COMPLEMENT.  */

struct value *
eval_op_cobol_complement(struct type *expect_type, struct expression *exp,
                         enum noside noside,
                         enum exp_opcode opcode,
                         struct value *value)
  {
  if(value->type()->code() == TYPE_CODE_BOOL)
    return value_from_longest(value->type(), value_logical_not(value));
  return value_complement(value);
  }

/* See language.h.  */

void
cobol_language::language_arch_info(struct gdbarch *gdbarch,
                                   struct language_arch_info *lai) const
  {
  const struct builtin_type *builtin = builtin_type(gdbarch);
  
  /* Helper function to allow shorter lines below.  */
  auto add  = [&](struct type * t) -> struct type *
    {
    lai->add_primitive_type(t);
    return t;
    };
  
  type_allocator alloc (gdbarch);
  struct type *bool_type = add (init_boolean_type (alloc, 8, 1, "bool"));
  add (init_character_type (alloc, 32, 1, "char"));
  add (init_integer_type (alloc, 8, 0, "i8"));
  struct type *u8_type = add (init_integer_type (alloc, 8, 1, "u8"));
  add (init_integer_type (alloc, 16, 0, "i16"));
  add (init_integer_type (alloc, 16, 1, "u16"));
  add (init_integer_type (alloc, 32, 0, "i32"));
  add (init_integer_type (alloc, 32, 1, "u32"));
  add (init_integer_type (alloc, 64, 0, "i64"));
  add (init_integer_type (alloc, 64, 1, "u64"));
  add (init_integer_type (alloc, 128, 0, "i128"));
  add (init_integer_type (alloc, 128, 1, "u128"));
  
  unsigned int length = 8 * builtin->builtin_data_ptr->length ();
  add (init_integer_type (alloc, length, 0, "isize"));
//  struct type *usize_type
//    = add (init_integer_type (alloc, length, 1, "usize"));
  
  add (init_float_type (alloc, 32, "f32", floatformats_ieee_single));
  add (init_float_type (alloc, 64, "f64", floatformats_ieee_double));
  add (init_integer_type (alloc, 0, 1, "()"));
  
  //struct type *tem = make_cv_type (1, 0, u8_type, NULL);
  //add (cobol_slice_type ("&str", tem, usize_type));
  
  lai->set_bool_type (bool_type);
  lai->set_string_char_type (u8_type);  
  }

static int
count_levels(CobolVarNode *parent, int nlevel)
  {
  if( !parent || !parent->get_our_number() )
    {
    return nlevel;
    }
  nlevel = count_levels(parent->get_parent(), nlevel+1);
  return nlevel;
  }

static void
show_parent(struct ui_file *stream, const cblc_field_t *parent, int nlevel)
  {
  if( parent )
    {
    if( parent->parent )
      {
      cblc_field_t grandpa;
      memcpy( &grandpa, 
              cobol_fetch_bytes_from((CORE_ADDR)parent->parent,
                               sizeof(cblc_field_t)),
              sizeof(cblc_field_t));
      show_parent(stream, &grandpa, nlevel-1);
      }
    for(int i=0; i<nlevel-1; i++)
      {
      gdb_printf(stream, " ");
      }
    gdb_printf(stream, "%2.2d %s\n", parent->get_level(), parent->get_name());
    }
  }

static void
pps_piece(struct ui_file *stream, int n, char ch)
  {
  if( n <= 4 )
    {
    for(int i=0; i<n; i++)
      {
      gdb_printf(stream, "%c", ch);
      }
    }
  else
    {
    gdb_printf(stream, "%c(%d)", ch, n);
    }
  }

static char *
recompress_picture(char const * const picture)
  {
  static size_t buffer_size = 0;
  static char *buffer = nullptr;
  const char *p = picture;
  char *b;
  char ch = '\0';

  // First, calculate the length of the expanded string
  p = picture;
  size_t length = 1;  // Room for terminating nul
  while(*p)
    {
    ch = *p++;
    if( ch == '(' )
      {
      length -= 1;
      length += strtol(p, const_cast<char **>(&p), 10);
      if( *p )
        {
        p += 1;
        }
      }
    else
      {
      length += 1;
      }
    }
  if( length > buffer_size )
    {
    buffer_size = length;
    free(buffer);
    buffer = (char *)malloc(buffer_size);
    }
  
  // With room in buffer for the expanded picture clause, expand it:
  p = picture;
  b = buffer;
  while(*p)
    {
    if( *p == '(' )
      {
      p += 1;
      length = strtol(p, const_cast<char **>(&p), 10);
      if( *p )
        {
        p += 1;
        }
      for(size_t i=0; i<length; i++)
        {
        *b++ = ch;
        }
      }
    else
      {
      ch   = toupper(*p++);
      *b++ = ch;
      }
    }
  *b++ = '\0';

  // Now, having decompressed the string, recompress it.  We are going to 
  // just do that in place, since we are guaranteed that the compressed string
  // will be no bigger than the original one.  We have that guarantee bedcause
  // we turn 99999 into 9(5), but we leave 9999 alone, rather than turn it into
  // 9(4)

  p = buffer; // We get data from here
  b = buffer; // We put it there

  // Prime the pump
  ch = *p++;
  length = 1;
  while(*p)
    {
    if(*p == ch)
      {
      length += 1;
      }
    else
      {
      if(length >= 5)
        {
        b += sprintf(b, "%c(%ld)", ch, length);
        }
      else
        {
        for(size_t i=0; i<length; i++)
          {
          *b++ = ch;
          }
        }
      ch = *p;
      length = 1;
      }
    p += 1;
    }
  // Tie off the end
  if(length >= 5)
    {
    b += sprintf(b, "%c(%ld)", ch, length);
    }
  else
    {
    for(size_t i=0; i<length; i++)
      {
      *b++ = ch;
      }
    }
  *b++ = '\0';
  return buffer;
  }

static void
print_picture_string(struct ui_file *stream, cobol_value_field &field) 
  {
  const char *picture = field.picture();
  if( picture && *picture )
    {
    const char *recomp = recompress_picture(picture);
    gdb_printf(" PIC %s", recomp);
    if( field.attr() & blank_zero_e )
      {
      gdb_printf(stream, " BLANK WHEN ZERO");
      }
    }
  else
    {
    switch( field.type() )
      {
      case FldAlphanumeric:
        {
        if( field.attr() & all_alpha_e )
          {
          switch(field.capacity())
            {
            case 1:
              gdb_printf(stream, " PIC A");
              break;
            case 2:
              gdb_printf(stream, " PIC AA");
              break;
            case 3:
              gdb_printf(stream, " PIC AAA");
              break;
            case 4:
              gdb_printf(stream, " PIC AAAA");
              break;
            default:
              gdb_printf(stream, " PIC A(%ld)", field.capacity());
              break;
            }
          }
        else
          {
          switch(field.capacity())
            {
            case 1:
              gdb_printf(stream, " PIC X");
              break;
            case 2:
              gdb_printf(stream, " PIC XX");
              break;
            case 3:
              gdb_printf(stream, " PIC XXX");
              break;
            case 4:
              gdb_printf(stream, " PIC XXXX");
              break;
            default:
              gdb_printf(stream, " PIC X(%ld)", field.capacity());
              break;
            }
          }
        if( field.attr() & rjust_e )
          {
          gdb_printf(stream, " JUSTIFIED RIGHT");
          }
        break;
        }

      case FldNumericBinary:
      case FldNumericDisplay:
      case FldNumericBin5:
      case FldPacked:
        {
        gdb_printf(stream, " PIC ");
        if( field.attr() & signable_e )
          {
          gdb_printf(stream, "S");
          }
        if( field.attr() & scaled_e )
          {
          // 9999PPP yields 4/-3
          // PPP9999 yields 4/3
          if( field.rdigits() < 0 )
            {
            pps_piece(stream,  field.digits(), '9');
            pps_piece(stream, -field.rdigits(), 'P');
            }
          else
            {
            pps_piece(stream,  field.rdigits(), 'P');
            pps_piece(stream,  field.digits(), '9');
            }
          }
        else
          {
          pps_piece(stream, field.digits() - field.rdigits(), '9');
          if( field.rdigits() )
            {
            gdb_printf(stream, "V");
            pps_piece(stream, field.rdigits(), '9');
            }
          }
        if( field.attr() & separate_e && field.type() != FldPacked )
          {
          if( field.attr() & leading_e )
            {
            gdb_printf(stream, " LEADING SEPARATE");
            }
          else
            {
            gdb_printf(stream, " TRAILING SEPARATE");
            }
          }
        else
          {
          if( field.attr() & leading_e )
            {
            gdb_printf(stream, " LEADING");
            }
          }
        break;
        }

      default:
        {
        error("print_picture_string(): doesn't understand %s",
              cobol_language::field_type_as_string(field.type()));
        break;
        }
      }
    }
  }

static void
remove_trailing_zeroes(char *psz)
  {
  char *p = psz + strlen(psz) -1 ;
  while( p > psz && *p == '0' )
    {
    *p-- = '\0';
    }
  }

static bool
all_char(const char *s, char ch)
  {
  bool retval = true;
  while(*s)
    {
    if( *s++ != ch )
      {
      retval = false;
      }
    }
  return retval;
  }

static bool
all_low(const char *s, size_t len)
  {
  bool retval = true;
  const char *s_end = s + len;
  while(s < s_end)
    {
    if( *s++ != LOW_VALUE )
      {
      retval = false;
      }
    }
  return retval;
  }

static bool
numdisp_is_zero(cobol_value_field &field)
  {
  char *s = field.initial();
  bool retval = true;
  while(*s)
    {
    char ch = *s++;
    if( isdigit(ch) && ch != ZERO )
      {
      retval = false;
      break;
      }
    }
  return retval;
  }

static char *
rep_string(const char *s)
  {
  // This routine is all but designed to make computer scientists who worry
  // about computational complexity grasp their pearls and gasp in horror.
  
  // I am generally one of those.  But this routine will make certain things
  // that happen in COBOL look a lot neater to the person using the debugger.
  
  // I have a sneaking suspicion -- just a suspicion -- that there is probably
  // a marvelous, worthy-of-a-journal-article look-what-Ken-Thompson-did-with-grep
  // kind of algorithm for this.  But I don't see it.  So....
  
  // COBOL provides ALL "A" and ALL "ABC", which when applied to a 10-char
  // string, gives "AAAAAAAAAA" and "ABCABCABCA".
  
  // This routine identifies the "ABC" in "ABCABCABCA".  
  
  // The complexity is somewhere between NlogN and N-squared
  
  static size_t bufsize = 0;
  static char *buf = NULL;
  size_t length = strlen(s);
  if( length + 1 > bufsize )
    {
    free(buf);
    bufsize = length + 1;
    buf = (char *)malloc(bufsize);
    }
  strcpy(buf, s);

  char *retval = NULL;

  // We know that ALL "Z" has already been caught, so we will start with a 
  // stride of 2
    size_t stride = 2;
  
  // We need a limiting length.  By setting it to length-2, we avoid finding
  // "ABCDEFA" as ALL "ABCDEF".  That's technically accurate, but it led to
  // results that seemed wrong until I thought about them.  So I chose to avoid
  // them:
  while(stride <= length-2)
    {
    // We break up 'buf' into blocks of 'stride' characters
    size_t init;
    for( init=0; init<stride; init++ )
      {
      // starting at 'init', we walk through 'buf' with 'stride' length boots,
      // making sure the 'init' character of each block matches the 'init'
      // character of the first block:
      size_t walker = init+stride;
      while(walker < length)
        {
        if( buf[walker-stride] != buf[walker] )
          {
          // This block doesn't match the previous one
          break;
          }
        walker += stride;
        }
      if( walker < length) 
        {
        // We have mismatched blocks, so there is no need to check
        // any more 'init' for this stride
        break;
        }
      }
    if( init == stride )
      {
      // Son of gun! We found an ALL.  We actually did!
      buf[stride] = '\0';
      retval = buf;
      break;
      }
    
    stride += 1;
    }

  return retval;
  }

static void
print_type_value_display(struct ui_file *stream,
                         cobol_value_field &field)
  {
  // COBOL data descriptors have VALUE clauses.  This routine displays those
  // value clauses.  The inevitable confusion with the oft-used word "value"
  // is regrettable, but nonetheless unavoidable.
  
  // This routine is awkwardly written.  The author begs forgiveness.  In his
  // defense, he points out that as in so many things COBOL, it is an awkward
  // problem.
  char *initial_raw = field.initial();

  char initial[128];
  bool is_negative;
  char *rep;
  if( initial_raw)
    {
    switch( field.type() )
      {
      case FldGroup:
      case FldAlphanumeric:
      case FldAlphaEdited:
      case FldNumericEdited:
        {
        gdb_printf(stream, " VALUE ");
        if( all_low(initial_raw, field.capacity()) )
          {
          gdb_printf(stream, "LOW-VALUES");
          }
        else if( all_char(initial_raw, QUOTE) )
          {
          gdb_printf(stream, "QUOTES");
          }
        else if( all_char(initial_raw, SPACE) )
          {
          gdb_printf(stream, "SPACES");
          }
        else if( all_char(initial_raw, ZERO) )
          {
          gdb_printf(stream, "ZEROES");
          }
        else if( all_char(initial_raw, HIGH_VALUE) )
          {
          gdb_printf(stream, "HIGH-VALUES");
          }
        else if( all_char(initial_raw, *initial_raw) )
          {
          gdb_printf(stream, "ALL \"%c\"", *initial_raw);
          }
        else if( (rep = rep_string(initial_raw)) )
          {
          gdb_printf(stream, "ALL \"%s\"", rep);
          }
        else
          {
          gdb_printf(stream, "\"");
          size_t characters = field.capacity();
          // Let's trim off any spaces to the right; it makes the display
          // neater.
          while( characters != 0 )
            {
            if( initial_raw[characters-1] != ' ' )
              {
              break;
              }
            characters-= 1;
            }
          
          for(size_t i=0; i<characters; i++)
            {
            gdb_printf(stream, "%c", initial_raw[i]);
            }
          gdb_printf(stream, "\"");
          }
        return;
        break;
        }

      case FldNumericDisplay:
        {
        // We have one special case:
        if( numdisp_is_zero(field) )
          {
          gdb_printf(" VALUE ZERO");
          return;
          }
        is_negative = false;
        for(int i=0; i<field.capacity(); i++)
          {
          if( initial_raw[i] == '-' )
            {
            is_negative = true;
            }
          char ch = initial_raw[i];
          if( ch >= ('0'+0x40) && ch <= ('9'+0x40) )
            {
            is_negative = true;
            ch &= ~0x40;
            }
          initial[i] = ch;
          }
        break;
        }

      case FldNumericBinary:
        {
        // initial_raw is a big-endian number:
        is_negative = !!(initial_raw[0] & 0x80);
        __int128 value = 0;
        if( is_negative )
          {
          value = -1;
          }
        unsigned char *pv = (unsigned char *)&value;
        for(size_t i=0; i<field.capacity(); i++)
          {
          pv[field.capacity()-1-i] = (unsigned char)initial_raw[i];
          }
        if( is_negative )
          {
          value = -value;
          }
        for(int i=0; i<field.digits(); i++)
          {
          int ch = value % 10;
          value /= 10;
          initial[field.digits()-1-i] = '0' + ch;
          }
        initial[field.digits()] = '\0';
        break;
        }

      case FldPacked:
        {
        // initial_raw is a big-endian packed decimal:
        int nybble = initial_raw[field.capacity()-1] & 0x0F;
        is_negative = nybble == 0X0B || nybble == 0X0D;
        
        __int128 value = 0;
        for(size_t i=0; i<field.capacity(); i++)
          {
          nybble = (initial_raw[i] >> 4) & 0x0F;
          if( nybble <= 9 )
            {
            value *= 10 ;
            value += nybble;
            }
          nybble = initial_raw[i] & 0x0F ;
          if( nybble <= 9 )
            {
            value *= 10 ;
            value += nybble;
            }
          }
        for(int i=0; i<field.digits(); i++)
          {
          int ch = value % 10;
          value /= 10;
          initial[field.digits()-1-i] = '0' + ch;
          }
        initial[field.digits()] = '\0';
        break;
        }

      case FldIndex:
      case FldNumericBin5:
        {
        // initial_raw is a little-endian number:
        is_negative = !!(initial_raw[field.capacity()-1] & 0x80);
        __int128 value = 0;
        if( is_negative )
          {
          value = -1;
          }
        unsigned char *pv = (unsigned char *)&value;
        for(size_t i=0; i<field.capacity(); i++)
          {
          pv[i] = (unsigned char)initial_raw[i];
          }
        if( is_negative )
          {
          value = -value;
          }
        if( field.digits() )
          {
          for(int i=0; i<field.digits(); i++)
            {
            int ch = value % 10;
            value /= 10;
            initial[field.digits()-1-i] = '0' + ch;
            }
          initial[field.digits()] = '\0';
          }
        else
          {
          gdb_printf(stream, " VALUE ");
          if( is_negative )
            {
            gdb_printf(stream, "-%ld", (int64_t)value);
            }
          else
            {
            gdb_printf(stream, "%ld", (int64_t)value);
            }
          return;
          }
        break;
        }

      case FldFloat:
        {
        char ach[128];
        switch(field.capacity())
          {
          case 4:
            {
            // We will convert based on the fact that for float32, any seven-digit
            // number converts to float32 and then back again unchanged.

            // We will also format numbers so that we produce 0.01 and 1E-3 on the low
            // side, and 9999999 and then 1E+7 on the high side
            // 10,000,000 = 1E7
            _Float32 floatval = *(_Float32 *)initial_raw;
            strfromf32(ach, sizeof(ach), "%.9E", floatval);
            char *p = strchr(ach, 'E');
            if( !p )
              {
              // Probably INF -INF NAN or -NAN, so ach has our result
              }
            else
              {
              p += 1;
              int exp = atoi(p);
              if( exp >= 6 || exp <= -5 )
                {
                // We are going to stick with the E notation, so ach has our result
                }
              else
                {
                // We are going to produce our number in such a way that we specify
                // seven signicant digits, no matter where the decimal point lands.
                // Note that exp is in the range of 6 to -2

                int precision = 9 - exp;
                sprintf(ach, "%.*f", precision, (double)floatval );
                }
              remove_trailing_zeroes(ach);
              }
            break;
            }

         case 8:
            {
            // We will convert based on the fact that for float32, any 15-digit
            // number converts to float64 and then back again unchanged.

            // We will also format numbers so that we produce 0.01 and 1E-3 on the low
            // side, and 9999999 and then 1E+15 on the high side
            _Float64 floatval = *(_Float64 *)initial_raw;
            strfromf64(ach, sizeof(ach), "%.17E", floatval);
            char *p = strchr(ach, 'E');
            if( !p )
              {
              // Probably INF -INF NAN or -NAN, so ach has our result
              }
            else
              {
              p += 1;
              int exp = atoi(p);
              if( exp >= 6 || exp <= -5 )
                {
                // We are going to stick with the E notation, so ach has our result
                }
              else
                {
                // We are going to produce our number in such a way that we specify
                // seven signicant digits, no matter where the decimal point lands.
                // Note that exp is in the range of 6 to -2

                int precision = 17 - exp;

                sprintf(ach, "%.*f", precision, (double)floatval );
                }
              remove_trailing_zeroes(ach);
              }
            break;
            }

         case 16:
            {
            // We will convert based on the fact that for float32, any 15-digit
            // number converts to float64 and then back again unchanged.

            // We will also format numbers so that we produce 0.01 and 1E-3 on the low
            // side, and 9999999 and then 1E+15 on the high side
            _Float128 floatval = *(_Float128 *)initial_raw;
            strfromf128(ach, sizeof(ach), "%.36E", floatval);
            char *p = strchr(ach, 'E');
            if( !p )
              {
              // Probably INF -INF NAN or -NAN, so ach has our result
              }
            else
              {
              p += 1;
              int exp = atoi(p);
              if( exp >= 6 || exp <= -5 )
                {
                // We are going to stick with the E notation, so ach has our result
                }
              else
                {
                // We are going to produce our number in such a way that we specify
                // seven signicant digits, no matter where the decimal point lands.
                // Note that exp is in the range of 6 to -2

                int precision = 36 - exp;
                char achFormat[24];
                sprintf(achFormat, "%%.%df", precision);
                strfromf128(ach, sizeof(ach), achFormat, floatval);
                }
              remove_trailing_zeroes(ach);
              }

            break;
            }
          }
        gdb_printf(stream, " VALUE %s", ach);
        return ;
        break;
        }

      default:
        {
        error("print_type_value_display(): doesn't understand %s",
              cobol_language::field_type_as_string(field.type()));
        break;
        }
      }

    gdb_printf(stream, " VALUE ");
    if(is_negative)
      {
      gdb_printf(stream, "-");
      }

    int ldigits = field.digits() ;
    int rdigits = 0;
    int zeroes_leading = 0;
    int zeroes_trailing = 0;

    if( field.attr() & scaled_e )
      {
      if( field.rdigits() < 0  )
        {
        // 9999PPP yields 4/-3
        zeroes_trailing = -field.rdigits();
        }
      else
        {
        // PPP9999 yields 4/3
        zeroes_leading = field.rdigits();
        rdigits = ldigits;
        ldigits = 0;
        gdb_printf(stream, ".");
        }
      }
    else
      {
      ldigits = field.digits() - field.rdigits();
      rdigits = field.rdigits();
      }
    int index = 0;
    if( field.attr() & separate_e && field.attr() & leading_e)
      {
      index += 1;
      }
    for( int i=0; i<ldigits; i++ )
      {
      char ch = initial[index++];
      if( ch >= ('0'+0x40) && ch <= ('9'+0x40) )
        {
        ch &= ~0x40;
        }

      if( isdigit(ch) )
        {
        gdb_printf(stream, "%c", ch);
        }
      }
    for(int i=0; i<zeroes_trailing; i++)
      {
      gdb_printf(stream, "%c", '0');
      }

    if( !(field.attr() & scaled_e) && field.rdigits() )
      {
      gdb_printf(stream, ".");
      }

    for(int i=0; i<zeroes_leading; i++)
      {
      gdb_printf(stream, "%c", '0');
      }

    for(int i=0; i<rdigits; i++)
      {
      char ch = initial[index++];
      if( ch >= ('0'+0x40) && ch <= ('9'+0x40) )
        {
        ch &= ~0x40;
        }

      if( isdigit(ch) )
        {
        gdb_printf(stream, "%c", ch);
        }
      }
    }
  }

static const char *
byte_or_bytes(size_t n)
  {
  static char retval[64];
  sprintf(retval,
          "%ld byte%s",
          n,
          n == 1 ? "" : "s");
  return retval;
  }

static char *
alphalimit(const char *str, size_t strlen, bool figconst)
  {
  char *retval;
  if( figconst )
    {
    retval = (char *)malloc(11);
    switch(str[0])
      {
      case 'L':
        strcpy(retval, "LOW-VALUE");
        break;
      case 'S':
        strcpy(retval, "SPACE");
        break;
      case 'Z':
        strcpy(retval, "ZERO");
        break;
      case 'Q':
        strcpy(retval, "QUOTE");
        break;
      case 'H':
        strcpy(retval, "HIGH_VALUE");
        break;
      default:
        strcpy(retval, "Dubner?");
        break;
      }
    }
  else
    {
    retval = (char *)malloc(strlen+1);
    memcpy(retval, str, strlen);
    retval[strlen] = '\0';
    }
  return retval;
  }

static void
pt_walker(struct ui_file *stream, 
          CobolVarNode *cvn,
          const struct block *block,
          const struct type_print_options *flags,
          int nlevel_incoming
          )
  {
  // This does the actual work of ptype.  It optionally recurses, in the case
  // where the original command was ptype/p, which is indicated by
  // flags.print_raw set to one
  struct symbol *sym = cvn->get_sym();
  gdb_assert(sym);

  value *val = evaluate_var_value (EVAL_NORMAL, block, sym);
  gdb_assert(val);
  cobol_value_field field(val);

  int nlevels;
  if( nlevel_incoming == 0 )
    {
    nlevels = count_levels(cvn->get_parent(), 0);
    show_parent(stream, field.parent(), nlevels);
    }
  else
    {
    nlevels = nlevel_incoming;
    }

  for(int i=0; i<nlevels; i++)
    {
    gdb_printf(stream, " ");
    }

  gdb_printf(stream, "%2.2d %s", field.level(), field.name());

  switch(field.type())
    {
    case FldGroup:
      print_type_value_display(stream, field);
      gdb_printf(stream, " (Group %s)", byte_or_bytes(field.capacity()));
      break;

    case FldAlphanumeric:
      print_picture_string(stream, field);
      print_type_value_display(stream, field);
      if( field.attr() & all_alpha_e )
        {
        gdb_printf(stream, " (Alphabetic %s)", byte_or_bytes(field.capacity()));
        }
      else
        {
        gdb_printf( stream, 
                    " (Alphanumeric %s)",
                    byte_or_bytes(field.capacity()));
        }
      break;

    case FldNumericDisplay:
      print_picture_string(stream, field);
      gdb_printf(stream, " DISPLAY");
      print_type_value_display(stream, field);
      gdb_printf(stream, " (%s)", byte_or_bytes(field.capacity()));
      break;

    case FldAlphaEdited:
      print_picture_string(stream, field);
      print_type_value_display(stream, field);
        gdb_printf( stream, 
                    " (Alphanumeric-edited %s)",
                    byte_or_bytes(field.capacity()));
      break;

    case FldNumericEdited:
      print_picture_string(stream, field);
      print_type_value_display(stream, field);
        gdb_printf( stream, 
                    " (Numeric-edited %s)",
                    byte_or_bytes(field.capacity()));
      break;

    case FldNumericBinary:
      print_picture_string(stream, field);
      gdb_printf(stream, " BINARY");
      print_type_value_display(stream, field);
      gdb_printf(stream, " (%s)", byte_or_bytes(field.capacity()));
      break;

    case FldNumericBin5:
      if( field.digits() )
        {
        print_picture_string(stream, field);
        gdb_printf(stream, " COMP-5");
        }
      else
        {
        switch(field.capacity())
          {
          case 1:
            gdb_printf( stream, " BINARY-CHAR");
            break;
          case 2:
            gdb_printf( stream, " BINARY-SHORT");
            break;
          case 4:
            gdb_printf( stream, " BINARY-LONG");
            break;
          case 8:
            gdb_printf( stream, " BINARY-DOUBLE");
            break;
          default:
            gdb_printf( stream, " BINARY-%ld", field.capacity());
            break;
          }
        if( !(field.attr() & signable_e) )
          {
          gdb_printf(stream, " UNSIGNED");
          }
        }
      print_type_value_display(stream, field);
      gdb_printf(stream, " (%s)", byte_or_bytes(field.capacity()));
      break;

    case FldPacked:
      print_picture_string(stream, field);
      if( field.attr() & separate_e )
        {
        gdb_printf(stream, " COMP-6");
        }
      else
        {
        gdb_printf(stream, " COMP-3");
        }
      print_type_value_display(stream, field);
      gdb_printf(stream, " (%s)", byte_or_bytes(field.capacity()));
      break;

    case FldIndex:
      gdb_printf(stream, " INDEX");
      print_type_value_display(stream, field);
      gdb_printf(stream, " (%s)", byte_or_bytes(field.capacity()));
      break;

    case FldPointer:
      gdb_printf(stream, " POINTER");
      gdb_printf(stream, " (%s)", byte_or_bytes(field.capacity()));
      break;

    case FldFloat:
      gdb_printf( stream,
                  " FLOAT-%s-%ld",
                  field.attr() & ieeedec_e ? "DECIMAL" : "BINARY",
                  8 * field.capacity()
                  );
      print_type_value_display(stream, field);
      gdb_printf(stream, " (%s)", byte_or_bytes(field.capacity()));
      break;

    case FldClass:
      {
      cblc_field_t *parent = field.parent();
      enum 
        {
        unknown_e,
        numeric_e,
        float_e,
        alphanumeric_e
        } etype;

      const char *type = "Unknown";
      etype = unknown_e;
      switch( parent->type )
        {
        case FldGroup:
        case FldAlphanumeric:
        case FldLiteralA:
        case FldAlphaEdited:
          etype = alphanumeric_e;
          type = "Alphabetic";
          break;

        case FldFloat:
          etype = float_e;
          type = "Float";
          break;

        case FldNumericBinary:
        case FldPacked:
        case FldNumericBin5:
        case FldNumericDisplay:
        case FldNumericEdited:
        case FldLiteralN:
        case FldIndex:
          etype = alphanumeric_e;
          type = "Numeric";
          break;

        case FldInvalid:
        case FldClass:
        case FldConditional:
        case FldForward:
        case FldSwitch:
        case FldDisplay:
        case FldBlob:
        case FldPointer:
        default:
          break;
        }
      gdb_printf( stream,
                  " (%s)",
                  type);
      char *list = cobol_fetch_string_from(field.initial_address());
      switch(etype)
        {
        case numeric_e:
        case float_e:
        case alphanumeric_e:
          {
          char *walker = list;
          while(*walker)
            {
            bool fig1;
            bool fig2;
            char *first;
            char *last;
            char *first_e;
            char *last_e;
            size_t first_len;
            size_t last_len;

            char *pend;

            first = walker;
            first_len = strtoull(first, &pend, 10);
            fig1 = *pend == 'F';
            first = pend+1;
            first_e = first + first_len;
            
            last = first_e;

            last_len = strtoull(last, &pend, 10);
            fig2 = *pend == 'F';
            last = pend+1;
            last_e = last + last_len;

            walker = last_e;

            first = alphalimit(first, first_len, fig1);
            last  = alphalimit(last, last_len, fig2);
            if( strcmp(first,last) )
              {
              gdb_printf(stream, " %s THRU %s", first, last);
              }
            else
              {
              gdb_printf(stream, " %s", first);
              }
            if( *walker )
              {
              gdb_printf(stream, ",");
              }

            free(first);
            free(last);
            }
          break;
          }

        default:
          break;
        }


      gdb_assert(list);
      break;
      }

    default:
      gdb_printf( stream,
                  "\nWe don't know how to ptype a %s",
                  cobol_language::field_type_as_string(field.type()));
      break;
    }

  if(flags->raw)
    {
    // raw is the flag that says the original command was ptype/p, which
    // means we need to recursively march through our children:
    for(size_t i=0; i<cvn->number_of_children(); i++)
      {
      gdb_printf(stream, "\n");
      pt_walker(stream, 
                cvn->get_child(i),
                block,
                flags,
                nlevels + 1
                );
      }
    }
  }

/* See language.h.  */

void
cobol_language::print_type(struct type *type, const char *varstring,
                           struct ui_file *stream, int show, int level,
                           const struct type_print_options *flags) const
  {
  // Get the symbol we want from the sv_recent_bysm hack.  We needed to do that
  // because `ptype symbol` passes the symbol type, but the actual symbol gets
  // lost by this point.  And we want to ptype individual variables, not 
  // variable types in the sense that C/C++ does.
  block_symbol bsym = cobol_language::get_recent_bsym();
  struct value *value_ = value_of_variable (bsym.symbol, bsym.block);

  // Use that information to prime the pump for pt_walker, which might operate
  // recursively through cvn's children:
  CobolVarNode *cvn = variable_blocks.get_node_from_value(value_);
  gdb_assert(cvn);
  pt_walker(stream, 
            cvn,
            bsym.block,
            flags,
            0
            );
  }

struct value *
cobol_language::extract_typed_address()
  {
  // see value.c
  // Get the symbol we want from the sv_recent_bysm hack.  We needed to do that
  // because `ptype symbol` passes the symbol type, but the actual symbol gets
  // lost by this point.  And we want to ptype individual variables, not 
  // variable types in the sense that C/C++ does.
  block_symbol bsym = cobol_language::get_recent_bsym();
  struct value *value = value_of_variable (bsym.symbol, bsym.block);
  return value;
//  static cobol_value_field field(value);
//  LONGEST retval = (LONGEST)(field.data());
//  return retval;
  }

/* See language.h.  */

void
cobol_language::emitchar(int ch, struct type *chtype,
                         struct ui_file *stream, int quoter) const
  {
  fprintf(stderr, "%s(): We don't know what to do\n", __func__);
  assert(false);
  }

void
cobol_language::printchar (int ch, struct type *chtype,
           struct ui_file *stream) const
  {
  fprintf(stderr, "%s(): We don't know what to do\n", __func__);
  assert(false);
  }

/* See language.h.  */

bool
cobol_language::is_string_type_p(struct type *type) const
  {
  return strcmp(type->name(), "string") == 0 ;
  }

void
cobol_language::cobol_subscript(cblc_refer_t *refer, struct value *subscript_string)
  {
  struct type *rtype = check_typedef(subscript_string->type());
  gdb_assert(rtype && rtype->code() == TYPE_CODE_ARRAY);

  char *str = (char *)subscript_string->contents().data ();

  char *p = str;
  int i=0;
  while(i < MAXIMUM_TABLE_DIMENSIONS)
    {
    while( isspace(*p) )
      {
      p += 1;
      }
    if( !isdigit(*p) )
      {
      break;
      }
    int next_subscript = strtol(p, &p, 10);
    refer->subscripts[i++] = next_subscript;
    }
  refer->is_dirty = true;
  gdb_assert(i < MAXIMUM_TABLE_DIMENSIONS);
  }

void
cobol_language::cobol_refmod(cblc_refer_t *refer, struct value *refmod_string)
  {
  struct type *rtype = check_typedef(refmod_string->type());
  gdb_assert(rtype && rtype->code() == TYPE_CODE_ARRAY);

  char *str = (char *)refmod_string->contents().data ();
  char *colon = strchr(str, ':');
  gdb_assert(colon);

  refer->refmod_from = atoi(str);
  refer->refmod_len  = atoi(colon+1);
  refer->is_dirty = true;
  }

char *
cobol_language::demangler(const char *mangled)
  {
  // This routine reverses the name mangling done in the compiler.  All cobol
  // variables were converted to lower-case; we leave that alone so that cobol's
  // case insensitivity is maintained.  (That is: "print VAR" is the same as
  // "print var")
  // Names that started with a digit had an underscore prepended
  // Internal hyphens were replaced with '$'.
  static char demangled[256];
  if( strlen(mangled) >= 2 )
    {
    if( mangled[0] == '_' && isdigit(mangled[1]) )
      {
      strcpy(demangled, mangled+1);
      }
    else
      {
      strcpy(demangled, mangled);
      }
      
    char *p = demangled;
    while(*p)
      {
      if( *p == '$' )
        {
        *p = '-';
        }
      p += 1;
      }
    }
  return strdup(demangled);
  }

char *
cobol_language::mangler(const char *cobol_name_)
    {
    // The caller should free the returned value

    /*  This has to match the code in the cobol1 front end of the gcobol
        compiler.

        Names get converted to lower-case.
        Names starting with a digit get a prepended underscore.
        Hyphens get converted to dollar signs. */

    if( !cobol_name_ )
      {
      return nullptr;
      }

    char *cobol_name = (char *)xmalloc(strlen(cobol_name_) + 2);
    if( isdigit(cobol_name_[0]) )
      {
      cobol_name[0] = '_';
      strcpy(cobol_name+1, cobol_name_);
      }
    else
      {
      strcpy(cobol_name, cobol_name_);
      }
    char *p = cobol_name;
    while( *p )
      {
      if( *p == '-' )
        {
        *p = '$';
        }
      p += 1;
      }

    return cobol_name;
    }
