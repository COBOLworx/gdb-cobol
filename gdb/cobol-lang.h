/*
 * Copyright (c) 2021-2023 Symas Corporation
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following disclaimer
 *   in the documentation and/or other materials provided with the
 *   distribution.
 * * Neither the name of the Symas Corporation nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
   Portions of this software were derived from pre-existing code in GDB,
   which contained this notice:

   Copyright (C) 1992-2022 Free Software Foundation, Inc.

   This file is part of GDB.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

#ifndef COBOL_LANG_H
#define COBOL_LANG_H

#include "demangle.h"
#include "language.h"
#include "value.h"
#include "c-lang.h"
#include "block.h"
#include "cobol-vars.h"
#include "inferior.h"

#define MAXIMUM_TABLE_DIMENSIONS 7

#define LOW_VALUE (0)
#define SPACE (' ')
#define QUOTE ('\"')
#define ZERO ('0')
#define HIGH_VALUE (0xFF)

struct cblc_refer_t;
struct cblc_refer_t
  {
  int             subscripts[MAXIMUM_TABLE_DIMENSIONS];
  int             refmod_from;
  int             refmod_len;
  int             flags;          // 0x0001 is the "ALL" flag
  bool            is_dirty;
  bool            address_of;
  int             additional_offset;
  CORE_ADDR       core_addr;

  cblc_refer_t()
    : refmod_from(0)
    , refmod_len(0)
    , flags(0)
    , is_dirty(true)
    , address_of(false)
    , additional_offset(0)
    , core_addr(0)
    {
    memset(subscripts, 0xff, sizeof(subscripts));
    }
  };

struct cobol_block_symbol
  {
  struct block_symbol bsym;
  struct cblc_refer_t refer;
  } ;

/*
 * Enumerated bit mask of variable attributes.
 *
 * Note that this enum has to match the one in gcc-cobol/symbols.h, which does
 * change from time to time.
 */
enum cbl_field_attr_t {
  none_e            = 0x0000000000,
  figconst_1_e      = 0x0000000001, // This needs to be 1 - don't change the position
  figconst_2_e      = 0x0000000002, // This needs to be 2
  figconst_4_e      = 0x0000000004, // This needs to be 4
  rjust_e           = 0x0000000008, // justify right
  ljust_e           = 0x0000000010, // justify left
  zeros_e           = 0x0000000020, // zero fill
  signable_e        = 0x0000000040,
  constant_e        = 0x0000000080, // pre-assigned constant
  function_e        = 0x0000000100,
  quoted_e          = 0x0000000200,
  filler_e          = 0x0000000400,
  _spare_e          = 0x0000000800, // Not in symbol table
  intermediate_e    = 0x0000001000, // Used for intermediate arithmetic
  embiggened_e      = 0x0000002000, // redefined numeric made 64-bit by USAGE POINTER
  all_alpha_e       = 0x0000004000, // FldAlphanumeric, but all A's
  all_x_e           = 0x0000008000, // picture is all X's
  all_ax_e          = 0x000000a000, // picture is all A's or all X's
  prog_ptr_e        = 0x0000010000, // FUNCTION-POINTER or PROGRAM-POINTER
  scaled_e          = 0x0000020000,
  refmod_e          = 0x0000040000, // Runtime; indicates a refmod is active
  based_e           = 0x0000080000, // pointer capacity, for ADDRESS OF or ALLOCATE
  any_length_e      = 0x0000100000, // inferred length of linkage in nested program
  global_e          = 0x0000200000, // field has global scope
  external_e        = 0x0000400000, // field has external scope
  blank_zero_e      = 0x0000800000, // BLANK WHEN ZERO
  // data division uses 2 low bits of high byte
  linkage_e         = 0x0001000000, // field is in linkage section
  local_e           = 0x0002000000, // field is in local section
  leading_e         = 0x0004000000, // leading sign (signable_e alone means trailing)
  separate_e        = 0x0008000000, // separate sign
  envar_e           = 0x0010000000, // names an environment variable
   dnu_1_e          = 0x0020000000, // unused: this attribute bit is available
  bool_encoded_e    = 0x0040000000, // data.initial is a boolean string
  hex_encoded_e     = 0x0080000000, // data.initial is a hex-encoded string
  depends_on_e      = 0x0100000000, // A group hierachy contains a DEPENDING_ON
  initialized_e     = 0x0200000000, // Don't call parser_initialize from parser_symbol_add
  has_value_e       = 0x0400000000, // Flag to hierarchical descendents to ignore .initial
  ieeedec_e         = 0x0800000000, // Indicates a FldFloat is IEEE 754 decimal, rather than binary
  big_endian_e      = 0x1000000000, // Indicates a value is big-endian
  same_as_e         = 0x2000000000, // Field produced by SAME AS (cannot take new members)
  record_key_e      = 0x4000000000,
  typedef_e         = 0x8000000000, // IS TYPEDEF
  strongdef_e       = typedef_e + intermediate_e, // STRONG TYPEDEF (not temporary)
};

enum cbl_variable_scope_t {
  vs_stack,
  vs_static,
  vs_external,              // Creates a PUBLIC STATIC variable of file scope
  vs_external_reference,    // References the previous
};

#define FIGCONST_MASK (figconst_1_e|figconst_2_e|figconst_4_e)
#define DATASECT_MASK (linkage_e | local_e)

enum cbl_figconst_t
    {
    normal_value_e = 0,
    low_value_e    = 1, // The order is important, because
    zero_value_e   = 3, // at times we compare, for example, low_value_e to
    space_value_e  = 4,
    quote_value_e  = 5, //
    high_value_e   = 6, // high_value_e to determine that low is less than high
    };

enum cbl_field_type_t {
  // It is critical that this match the GCOBOL symbols.h enumeration.
  FldInvalid,           // uninitialized
  FldGroup,
  FldAlphanumeric,      // For X(n).
  FldNumericBinary,     // For 999v9 comp       big-endian, 1 to 16 bytes
  FldFloat,             // 4-, 8-, and 16-byte floating point.  See ieeedec_e and big_endian_e flags
  FldPacked,            // For 999v9 comp-3     internal decimal, packed decimal representation;
  FldNumericBin5,       // For 999v9 comp-5     little-endian, 1 to 16 bytes. (Native binary)
  FldNumericDisplay,    // For 999v9            one decimal character per byte
  FldNumericEdited,     // For 999.9            PIC BPVZ90/,.+-CRDB*cs; must have one of  B/Z0,.*+-CRDBcs
  FldAlphaEdited,       //                      PIC AX9B0/; must have at least one A or X, and at least one B0/
  FldLiteralA,          // Alphanumeric literal
  FldLiteralN,          // Numeric literal
  FldClass,
  FldConditional,       // Target for parser_relop()
  FldForward,
  FldIndex,
  FldSwitch,
  FldDisplay,
  FldPointer,
  FldBlob,
};

#define NUMERIC_DISPLAY_SIGN_BIT (0x40)

std::string cobol_field_attr_as_string(cbl_field_attr_t attr);
void *cobol_fetch_bytes_from(CORE_ADDR addr, size_t len);
char *cobol_fetch_string_from(CORE_ADDR addr);

struct cblc_field_t
  {
  // This structure must match the code in structs.cc and gcbobolio.h in gcc-cobol
  unsigned char *data;        // The runtime data. There is no null terminator
  size_t         capacity;    // The size of "data"
  size_t         allocated;   // The number of bytes available for capacity
  size_t   offset;            // Offset from our ancestor (see note below)
  char    *name;              // The null-terminated name of this variable
  char    *picture;           // The null-terminated picture string.
  char    *initial;           // The null_terminated initial value
  struct cblc_field_t *parent;// This field's immediate parent field
  size_t occurs_lower;        // non-zero for a table
  size_t occurs_upper;        // non-zero for a table
  size_t attr;                // See cbl_field_attr_t
  signed char type;           // A one-byte copy of cbl_field_type_t
  signed char level;          // This variable's level in the naming heirarchy
  signed char digits;         // Digits specified in PIC string; e.g. 5 for 99v999
  signed char rdigits;        // Digits to the right of the decimal point. 3 for 99v999
  int    dummy;               // GCC seems to want an even number of 32-bit values

  const char *get_name() const
    {
    char *retval = cobol_fetch_string_from((CORE_ADDR)name);
    return retval;
    }

  int get_level() const
    {
    int retval = level;
    return retval;
    }

  };

#define ENCODING "CP1252"

enum rounded_t { unrounded_e, rounded_e };

struct cblc_refer_t;

class cobol_value_field
  {
  private:
    struct value *val       ; // Non-null when constructed from a VALUE
    cblc_field_t *pfield    ; // Non-null when constructed from another field
    cblc_field_t field      ;
    bool got_data           ;
    bool got_core           ;
    bool got_capacity       ;
    bool got_offset         ;
    bool got_name           ;
    bool got_picture        ;
    bool got_initial        ;
    bool got_parent         ;
    bool got_occurs_lower   ;
    bool got_occurs_upper   ;
    bool got_attr           ;
    bool got_type           ;
    bool got_level          ;
    bool got_digits         ;
    bool got_rdigits        ;

    void construct_init()
      {
      // This is common constructor code both for from value and from another
      // cblc_field_t:
      memset(&field, 0, sizeof(cblc_field_t));
      got_data          = false;
      got_core          = false;
      got_capacity      = false;
      got_offset        = false;
      got_name          = false;
      got_picture       = false;
      got_initial       = false;
      got_parent        = false;
      got_occurs_lower  = false;
      got_occurs_upper  = false;
      got_attr          = false;
      got_type          = false;
      got_level         = false;
      got_digits        = false;
      got_rdigits       = false;
      }

  public:

    struct cblc_refer_t  refer     ;

    cobol_value_field(value *val_)
      {
      val = val_;
      pfield = nullptr;
      construct_init();
      // Let's populate our member refer from the one buried in the value:
      cblc_refer_t *val_refer = (cblc_refer_t *)val->get_auxdata();
      if( val_refer )
        {
        // There are circumstances when the val_refer doesn't exist.  One
        // example is when the nodes are being walked to create an heirarchical
        // display
        refer.is_dirty = true;
        refer.refmod_from = val_refer->refmod_from ;
        refer.refmod_len  = val_refer->refmod_len  ;
        memcpy( &refer.subscripts,
                &val_refer->subscripts,
                sizeof(refer.subscripts) );
        }
      }

    cobol_value_field(cblc_field_t *parent)
      {
      val = nullptr;
      pfield = parent;
      construct_init();
      }

    ~cobol_value_field()
      {
      free(field.data);
      free(field.name);
      free(field.picture);
      free(field.parent);
      free(field.initial);
      }

    void value_print( struct ui_file *stream,
                      const struct value_print_options *options);

    CORE_ADDR
    core_addr()
      {
      if( !got_core )
        {
        got_core = true;
        resolve_refer();
        if( val )
          {
          struct value *base = value_struct_elt(&val, {}, "data", NULL, "cblc_field_t");
          refer.core_addr  = value_as_address(base);
          refer.core_addr += refer.additional_offset;
          }
        else if( pfield )
          {
          refer.core_addr  = (CORE_ADDR)pfield->data;
          refer.core_addr += refer.additional_offset;
          }
        }
      return refer.core_addr;
      }

    unsigned char *
    data()
      {
      if(!got_data)
        {
        got_data = true;
        resolve_refer();
        CORE_ADDR addr = core_addr();
        field.data = (unsigned char *)cobol_fetch_bytes_from( addr, capacity());
        }
      return field.data;
      }

    CORE_ADDR initial_address()
      {
      CORE_ADDR addr;
      if( val )
        {
        struct value *base = value_struct_elt(&val, {}, "initial", NULL, "cblc_field_t");
        addr = value_as_address(base);
        }
      else if( pfield )
        {
        addr = (CORE_ADDR)pfield->initial;
        }
      return addr;
      }

    char *initial()
      {
      if(!got_initial)
        {
        got_initial = true;

        CORE_ADDR addr;
        if( val )
          {
          struct value *base = value_struct_elt(&val, {}, "initial", NULL, "cblc_field_t");
          addr = value_as_address(base);
          }
        else if( pfield )
          {
          addr = (CORE_ADDR)pfield->initial;
          }
        if( addr )
          {
          field.initial = (char *)cobol_fetch_bytes_from( addr, original_capacity()+1);
          field.initial[original_capacity()] = '\0';
          }
        }
      return field.initial;
      }

    char *picture()
      {
      if( !got_picture )
        {
        got_picture = true;

        CORE_ADDR addr;
        if( val )
          {
          struct value *base = value_struct_elt(&val, {}, "picture", NULL, "cblc_field_t");
          addr = value_as_address(base);
          }
        else if( pfield )
          {
          addr = (CORE_ADDR)pfield->picture;
          }
        field.picture = cobol_fetch_string_from(addr);
        }
      return field.picture;
      }

    cblc_field_t *parent()
      {
      if( !got_parent )
        {
        got_parent = true;
        CORE_ADDR addr;
        if( val )
          {
          struct value *base = value_struct_elt(&val, {}, "parent", NULL, "cblc_field_t");
          addr = value_as_address(base);
          }
        else if( pfield )
          {
          addr = (CORE_ADDR)pfield->parent;
          }
        field.parent = (cblc_field_t *)cobol_fetch_bytes_from(addr, sizeof(cblc_field_t));
        }
      return field.parent;
      }

    char *name()
      {
      if(!got_name)
        {
        got_name = true;

        CORE_ADDR addr;
        if( val )
          {
          struct value *base = value_struct_elt(&val, {}, "name", NULL, "cblc_field_t");
          addr = value_as_address(base);
          }
        else if( pfield )
          {
          addr = (CORE_ADDR)pfield->name;
          }
        field.name = cobol_fetch_string_from(addr);
        }
      return field.name;
      }

    size_t capacity()
      {
      // If this is a refmod, then refmod_len is the winner.  But if
      // the refmod_len is zero, then it defaults to the capacity:

      if(!got_capacity)
        {
        got_capacity = true;
        if( val  )
          {
          field.capacity = (size_t)value_as_long(
                        value_struct_elt( &val, 
                                          {}, 
                                          "capacity", 
                                          NULL,
                                          "cblc_field_t"));
          }
        else if( pfield )
          {
          field.capacity = pfield->capacity;
          }
        }

      if( refer.refmod_from )
        {
        // If refmod_len is zero, it means the maximum:
        size_t len = refer.refmod_len ? refer.refmod_len : field.capacity;

        // To avoid catastrophe, we trim it to the maximum possible based
        // on the refmod_from and field.capacity values:

        size_t max = std::min(len, field.capacity-(refer.refmod_from-1) );

        return max;
        }

      return field.capacity;
      }

    size_t original_capacity()
      {
      if(!got_capacity)
        {
        got_capacity = true;
        if( val  )
          {
          field.capacity = (size_t)value_as_long(
                        value_struct_elt( &val, 
                                          {},
                                          "capacity",
                                          NULL,
                                          "cblc_field_t"));
          }
        else if( pfield )
          {
          field.capacity = pfield->capacity;
          }
        }

      return field.capacity;
      }

    cbl_field_attr_t attr()
      {
      if(!got_attr)
        {
        got_attr = true;
        if( val )
          {
          field.attr = (cbl_field_attr_t)value_as_long(
                        value_struct_elt( &val,
                                          {},
                                          "attr",
                                          NULL,
                                          "cblc_field_t"));
          }
        else if( pfield )
          {
          field.attr = pfield->attr;
          }

        // there is no real need to pester an ordinary
        // user with the initialized_e bit.  It's just noise.
        field.attr = (cbl_field_attr_t) 
                                   ((long)field.attr & (long)(~initialized_e));
        }
      return (cbl_field_attr_t)field.attr;
      }

    cbl_field_type_t type()
      {
      // In the case of a refmod, the type is always alphanumeric:
      if( refer.refmod_from )
        {
        return FldAlphanumeric;
        }

      if( !got_type )
        {
        got_type = true;
        if( val )
          {
          field.type = (signed char)value_as_long(
                      value_struct_elt(&val, {}, "type", NULL, "cblc_field_t"));
          }
        else if( pfield )
          {
          field.type = pfield->type;
          }
        }
      return (cbl_field_type_t)field.type;
      }

    int digits()
      {
      if(!got_digits)
        {
        got_digits = true;
        if( val )
          {
          field.digits = (signed char)value_as_long(
                         value_struct_elt(&val,
                                          {},
                                          "digits",
                                          NULL,
                                          "cblc_field_t"));
          }
        else if( pfield )
          {
          field.digits = pfield->digits;
          }
        }
      return (int)field.digits;
      }

    int rdigits()
      {
      if(!got_rdigits)
        {
        got_rdigits = true;
        if( val )
          {
          field.rdigits = (signed char)value_as_long(
                          value_struct_elt( &val,
                                            {},
                                            "rdigits",
                                            NULL,
                                            "cblc_field_t"));
          }
        else if( pfield )
          {
          field.digits = pfield->rdigits;
          }
        }
      return (int)field.rdigits;
      }

    int level()
      {
      if( !got_level )
        {
        got_level = true;
        if( val )
          {
          field.level = (signed char)value_as_long(
                        value_struct_elt( &val,
                                          {},
                                          "level",
                                          NULL,
                                          "cblc_field_t"));
          }
        else if( pfield )
          {
          field.level = pfield->level;
          }
        }
      return (int)field.level;
      }

  void resolve_refer();

  std::string format_for_display();
  } ;

/* Class representing the Cobol language.  */

class cobol_language : public language_defn
  {
  public:
    cobol_language ()
      : language_defn (language_cobol)
      { /* Nothing.  */ }

    /* See language.h.  */

    const char *
    name () const override
      {
      return "cobol";
      }

    /* See language.h.  */

    const char *
    natural_name () const override
      {
      return "Cobol";
      }

    /* See language.h.  */

    enum case_sensitivity
    case_sensitivity () const override
      {
      return case_sensitive_off;
      }

    const std::vector<const char *> &
    filename_extensions () const override
      {
      static const std::vector<const char *> extensions = { ".cob", ".cbl",
                                                            ".COB", ".CBL"
                                                          };
      return extensions;
      }

    /* See language.h.  */

    void language_arch_info (struct gdbarch *gdbarch,
                             struct language_arch_info *lai) const override;

    /* See language.h.  */

    bool sniff_from_mangled_name
    (const char *mangled, gdb::unique_xmalloc_ptr<char> *demangled)
    const override
      {
      //// *demangled = gdb_demangle (mangled, DMGL_PARAMS | DMGL_ANSI);
      return *demangled != NULL;
      }

    /* See language.h.  */

    gdb::unique_xmalloc_ptr<char>
    demangle_symbol (const char *mangled,
                     int options) const override
      {
      return NULL; //// gdb_demangle (mangled, options);
      }

    /* See language.h.  */

    void print_type (struct type *type, const char *varstring,
                     struct ui_file *stream, int show, int level,
                     const struct type_print_options *flags) const override;

    /* See language.h.  */

    gdb::unique_xmalloc_ptr<char> watch_location_expression
    (struct type *type, CORE_ADDR addr) const override
      {
      type = check_typedef (check_typedef (type)->target_type ());
      std::string name = type_to_string (type);
      return xstrprintf ("*(%s as *mut %s)", core_addr_to_string (addr),
                         name.c_str ());
      }

    /* See language.h.  */

    void value_print
    ( struct value *val,
      struct ui_file *stream,
      const struct value_print_options *options) const override;

    void value_print_inner
    (struct value *val, struct ui_file *stream, int recurse,
     const struct value_print_options *options) const override;

    /* See language.h.  */

    struct block_symbol lookup_symbol_nonlocal(
      const char *name,
      const struct block *block,
      const domain_search_flags domain) const override
      {

      // Copied from the c-language routine of the same name:

      struct block_symbol result;

      /* NOTE: dje/2014-10-26: The lookup in all objfiles search could skip
         the current objfile.  Searching the current objfile first is useful
         for both matching user expectations as well as performance.  */

      result = lookup_symbol_in_static_block (name, block, domain);
      if (result.symbol != NULL)
        return result;

      /* If we didn't find a definition for a builtin type in the static block,
         search for it now.  This is actually the right thing to do and can be
         a massive performance win.  E.g., when debugging a program with lots of
         shared libraries we could search all of them only to find out the
         builtin type isn't defined in any of them.  This is common for types
         like "void".  */
      if (domain == SEARCH_VFT)
        {
        struct gdbarch *gdbarch;

        if (block == NULL)
          gdbarch = current_inferior()->arch();
        else
          gdbarch = block->gdbarch();
        result.symbol = language_lookup_primitive_type_as_symbol (this,
                        gdbarch, name);
        result.block = NULL;
        if (result.symbol != NULL)
          return result;
        }
      result = lookup_global_symbol (name, block, domain);
      return result;
      }

    /* See language.h.  */

    int parser (struct parser_state *ps) const override;

    /* See language.h.  */

    void emitchar (int ch, struct type *chtype,
                   struct ui_file *stream, int quoter) const override;

    /* See language.h.  */

    void
    printchar (int ch, struct type *chtype,
               struct ui_file *stream) const override;

    /* See language.h.  */

    void printstr (struct ui_file *stream, struct type *elttype,
                   const gdb_byte *string, unsigned int length,
                   const char *encoding, int force_ellipses,
                   const struct value_print_options *options) const override;

    /* See language.h.  */

    void
    print_typedef (struct type *type, struct symbol *new_symbol,
                   struct ui_file *stream) const override
      {
      type = check_typedef (type);
      gdb_printf (stream, "type %s = ", new_symbol->print_name ());
      type_print (type, "", stream, 0);
      gdb_printf (stream, ";");
      }

    /* See language.h.  */

    bool is_string_type_p (struct type *type) const override;

    /* See language.h.  */

    bool
    range_checking_on_by_default () const override
      {
      return true;
      }

    static char *demangler(const char *mangled);
    static char *mangler(const char *unmangled);

    static bool catch_next(bool initial_command, const char *count_string);
    static void mirrored_fsm_prepare( int skip_subroutines,
                                      int single_inst,
                                      int count);
    static int  mirrored_should_stop();
    static int suppress_notify_normal_stop();
    static void set_state_of_next(int n);
    static int  get_state_of_next();
    static int  should_we_issue_extra_next();
    static int  waiting_for_temporary_breakpoint();
    static int  get_mirrored_count();
    static block_symbol get_recent_bsym();
    static const char *modify_arg_for_break(const char **arg);
    static void cobol_subscript (cblc_refer_t *refer,
                             struct value *subscript_string);
    static void cobol_refmod (cblc_refer_t *refer,
                          struct value *refmod_string);
    static void string_to_alpha_edited( char *dest,
                                        const char *source,
                                        int slength,
                                        char *picture);
    static __int128 dirty_to_binary(const char *dirty,
                                    int length,
                                    int *rdigits);
    static __int128 power_of_ten(int n);
    static bool binary_to_string(char *result, int digits, __int128 value);
    static void cobol_assign_value_from_string( struct value *val_lhs,
                                                const char *str);
    static bool string_to_numeric_edited(char *dest,
                                         char *source,
                                         int rdigits,
                                         int is_negative,
                                         const char *picture);
    static void parse_assignment_string(struct value *val_lhs,
                                        const char *str_original_);
    static const char *field_type_as_string(cbl_field_type_t type);
    static struct value *extract_typed_address();

  private:

    /* Helper for value_print_inner, arguments are as for that function.
       Prints structs and untagged unions.  */

    void val_print_struct (struct value *val, struct ui_file *stream,
                           int recurse,
                           const struct value_print_options *options) const;

    /* Helper for value_print_inner, arguments are as for that function.
       Prints discriminated unions (Cobol enums).  */

    void print_enum (struct value *val, struct ui_file *stream, int recurse,
                     const struct value_print_options *options) const;

    void print_val_as_list( struct value *val,
                            struct ui_file *stream,
                            const struct value_print_options *opts) const;

    void print_cblc_field_t( struct value *val,
                             struct ui_file *stream,
                             const struct value_print_options *opts) const;

    void print_string( struct value *val,
                       struct ui_file *stream,
                       const struct value_print_options *opts) const;

    void print_cblc_field_t_heirarchical(
      struct value *val,
      struct ui_file *stream,
      const struct value_print_options *opts) const;

    void print_data_address(
      struct value *val,
      struct ui_file *stream,
      const struct value_print_options *opts) const;

    void walk_and_print(const struct block *block,
                        CobolVarNode *cvn,
                        int print_level,
                        struct ui_file *stream,
                        const struct value_print_options *options) const;

    struct cblc_field_t populate_from(struct value *val) const;

  };


#endif /* COBOL_LANG_H */
