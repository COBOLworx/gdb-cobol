#include "defs.h"
#include <ctype.h>
#include "expression.h"
#include "value.h"
#include "parser-defs.h"
#include "language.h"
#include "cobol-lang.h"
#include "bfd.h" /* Required by objfiles.h.  */
#include "symfile.h" /* Required by objfiles.h.  */
#include "objfiles.h" /* For have_full_symbols and have_partial_symbols.  */
#include "block.h"
#include "completer.h"
#include "expop.h"
#include "cobol-parse.h"
#include "cobol-vars.h"
#include "source.h"
#include "linespec.h"
#include "arch-utils.h"

#define parse_type(ps) builtin_type (ps->gdbarch ())

static char quote_character = '"';
#define SPACE (' ')

static CobolVars *cobol_vars;
static struct parser_state *pstate = NULL;

// This is a kludge so that the ptype command can operate on a specific COBOL
// variable rather than a generic cblc_field_t, which isn't of much use to
// a COBOL programmer trying to debug something.
static block_symbol sv_recent_bsym;

static const struct block *
current_block()
  {
  const struct block *block = NULL;
  CORE_ADDR pc = 0;

  try
    {
    frame_info_ptr frame    = get_current_frame();
    block = get_frame_block (frame, &pc);
    return block;
    }
  catch(...)
    {
    return NULL;
    }
  }

static bool
valid_numerical_constant(const char *str, long &the_value)
  {
  bool retval = true;
  the_value = 0;
  bool is_negative = false;
  int the_radix = input_radix;

  if( strlen(str)> 0 && str[0] == '-' )
    {
    is_negative = true;
    str += 1;
    }

  if( strlen(str) > 0 && str[0] == '0' )
    {
    the_radix = 8;
    if( strlen(str) > 1 && tolower(str[1]) == 'x' )
      {
      the_radix = 16;
      str += 2;
      }
    }

  while( *str )
    {
    char ch = *str++;
    long ch_value;
    if( isdigit(ch) )
      {
      ch_value = ch - '0';
      }
    else if( isxdigit(ch) )
      {
      ch_value = tolower(ch) - 'a' + 10;
      }
    else
      {
      retval = false;
      break;
      }
    if( ch_value >= the_radix )
      {
      retval = false;
      break;
      }
    the_value *= the_radix;
    the_value += ch_value;
    }

  the_value *= is_negative ? -1 : 1 ;
  return retval;
  }

static
size_t count_characters_in_identifier(const char *str)
  {
  // An identifier consists of a "piece" followed by 
  // [ ]+[IN|OF][ ]+ or [ ]*[/][ ]
  // 
  // We need to know how many characters are involved in that
  enum 
    {
    in_space0,
    in_piece,
    in_space1,
    in_of,
    in_in,
    in_space2,
    } state = in_space0;

  const char *p = str;

  while(*p)
    {
    char ch = tolower(*p++);
    switch(state)
      {
      case in_space0:
        if( ch != ' ' )
          {
          p -= 1;
          // Special case:  If the first character in a piece of a variable is
          // a hyphen, we are done.  If well-formed, it is a 
          //     "break ... -force-condition", or something like that
          if( *p == '-' )
            {
            goto run_away;
            }
          state = in_piece;
          }
        break;
      case in_piece:
        if( ch == ' ' )
          {
          state = in_space1;
          }
        else if( ch == '/' )
          {
          state = in_space0;
          }
        break;
      case in_space1:
        if( ch == 'o' )
          {
          state = in_of;
          }
        else if( ch == 'i' )
          {
          state = in_in;
          }
        break;
      case in_of:
        if( ch == 'f' )
          {
          state = in_space2;
          }
        else
          {
          p -= 2;
          goto run_away;
          }
        break;
      case in_in:
        if( ch == 'n' )
          {
          state = in_space2;
          }
        else
          {
          p -= 2;
          goto run_away;
          }
        break;
      case in_space2:
        // There has to be at least one space
        if( ch == ' ' )
          {
          state = in_space0;
          }
        else
          {
          // Somebody is trying to trick us with something like
          //    "break abc ofxxx"
          p -= 3;
          goto run_away;
          }
        break;
      }
    }
  run_away:
  return p - str;
  }

static const char *
named_string(const char *name_of_string)
  {
  // returns a pointer to a malloced copy of the contents of the variable:

  // For reference:  I figured the following code out at one point.  I am saving
  // it here so that I have it written down for possible future use.
          //// This is one way of looking up a symbol:
          //struct block_symbol bsym;
          //bsym = lookup_symbol( entry_point, 
          //                      NULL,           // block
          //                      SEARCH_VFT,
          //                      NULL);

  const char *retval = NULL;
  value *val;
  try
    {
    expression_up expr = parse_expression (name_of_string, nullptr, 0);
    val =  expr->evaluate ();
    }
  catch(...)
    {
    val = NULL;
    }

  if(val)
    {
    retval = cobol_fetch_string_from(val->address());
    }
  return retval;
  }

const char *
cobol_language::modify_arg_for_break(const char **arg)
  {
  if( arg == nullptr || *arg == nullptr)
    {
    return nullptr;
    }

  // We are dealing with five possibilities at this point.  
  // 1) This is a request for "-qualified main...", which is what we see when
  //    the user enters the "start" command. We look for a variable 
  //    '_cobol_entry_point'.  If found, the contents will be the desired
  //    "source:line" for the first executable statement in the first executed
  //    program-id of the executable.
  // 2) This is a request to "break \"quack\"", which means we look for a
  //    the variable '_prog_entry_point_quack'.  If found, the contents will be
  //    the desired "source:line" for the first executable instruction in the
  //    function 'quack'.  We use that to replace \"quack\" in the arg string
  // 3) This is a request to "break quack".  We search to see if quack is a 
  //    section or paragraph name, and if found, replace "quack" with the
  //    source:line of the paragraph
  // 4) If not found as a section or paragraph, we search for
  //   '_prog_entry_point_quack'.  This addresses the reality that the same
  //    name can be used for both a section or paragraph, and for a program-id
  //    outside.  An unadorned name is tested first as a section/paragraph name,
  //    and only if not found is it checked as a program-id.  As noted in 2),
  //    it can be forced as a program-id by using single or double quotes.
  // 5) If none of those are found, then we return NULL.
  //
  //    In the case of finding something, we make a copy of arg in a static
  //    string location.  We replace the symbol with the associated source:line
  //    pair, and return that.  As used in breakpoint.C, it is safe to return a
  //    pointer to the static variable.

  static std::string retval;
  retval.clear();
  VSTRING tokens = split(*arg, ' ');
  if( tokens.size() >= 2 
      && tokens[0] == "-qualified"
      && tokens[1] == "main"       )
    {
    // We are assuming the request is for
    //      "-qualified main inferior 1"
    // which is what happens when somebody issues a START command.

    const char *replacement = named_string("_cobol_entry_point");
    if( replacement )
      {
      retval = replacement;
      free((void *)replacement);
      }
    if( retval.length() )
      {
      *arg += strlen(*arg);
      return retval.c_str();
      }
    else
      {
      // They wanted a START, but we couldn't find our global variable. This
      // happens when the COBOL executable was built in some other fashion than
      // GCOBOL with the -main switch.
      retval = *arg;
      *arg += strlen(*arg);
      return retval.c_str();
      }
    }
  // It wasn't a START:
  if( tokens[0][0] != '\"' && tokens[0][0] != '\'' )
    {
    // Because the symbol isn't quoted, we look for a section or paragraph of
    // that name:
    size_t arg_chars = count_characters_in_identifier(*arg);

    const struct block *block_of_interest = current_block();
    if( block_of_interest )
      {
      retval = *arg;

      cobol_vars = variable_blocks.populate(block_of_interest);

      // Here are our possibilities:
      
      // AAA <remainder>
      // AAA OF BBB <remainder>
      // AAA IN BBB <remainder>
      // AAA / BBB  <remainder>
      // AAA/  BBB  <remainder>
      // AAA  /BBB  <remainder>
      // AAA/BBB    <remainder>

      std::string procedure;
      std::string section;

      size_t remainder_at=0;

      if( tokens.size() >= 1 )
        {
        // This catches AAA and AAA/BBB
        procedure = tokens[0];
        remainder_at = 1;
        }
      if( tokens.size() >= 2 )
        {
        if( tokens[0][tokens[0].size()-1] == '/' )
          {
          // This is the AAA/ BBB case
          procedure = tokens[0] + tokens[1];
          remainder_at = 2;
          }
        else if( tokens[1][0] == '/' )
          {
          // This is the AAA /BBB case
          procedure = tokens[0] + tokens[1];
          remainder_at = 2;
          }
        }
      if( tokens.size() >= 3 )
        {
        if(    tokens[1] == "/"
            || tokens[1] == "OF"
            || tokens[1] == "of"
            || tokens[1] == "IN"
            || tokens[1] == "in"
            || tokens[1] == "oF"
            || tokens[1] == "Of"
            || tokens[1] == "iN"
            || tokens[1] == "In" )
          {
          procedure = tokens[0] + "/" + tokens[2];
          remainder_at = 3;
          }
        }
      size_t nfound = procedure.find('/');
      if( nfound != std::string::npos )
        {
        section = procedure.substr(nfound+1);
        procedure = procedure.substr(0,nfound);
        }

      // fprintf(  stderr,
                // "%s OF %s %ld\n",
                // procedure.c_str(),
                // section.c_str(),
                // remainder_at);

      std::string mapped = cobol_vars->find_paragraph(lowered(procedure),
                                                      lowered(section));
      if( !mapped.empty() )
        {
        struct symtab_and_line pc_sal = string_to_sal(mapped.c_str());

        //fprintf(stderr, "%p", &pc_sal);
        
        const char *filename = pc_sal.symtab->fullname();
        
        int line = pc_sal.line;
        char ach[32];
        sprintf(ach, "%d", line);

        retval = mapped;

        retval = filename;
        retval += ":";
        retval += ach;

        for( size_t i=remainder_at; i<tokens.size(); i++ )
          {
          retval += ' ';
          retval += tokens[i];
          }
        }
      }

    if( retval.size() )
      {
      // Adjust the incoming pointer to reflect the characters we just ate
      *arg += arg_chars;
      return retval.c_str();
      }
    }

  // Arriving here means it wasn't a START, nor was an attempt to find the
  // symbol as a section or paragraph successful.  We are checking to see if the
  // symbol is the name of a program-id for which we have a source:line pair
  // available:
  if(tokens[0][0] == '\"' || tokens[0][0] == '\'')
    {
    // Trim off leading quote:
    tokens[0] = tokens[0].substr(1);
    }
  size_t lm_1 = tokens[0].length()-1;
  if(lm_1)
    {
    if(tokens[0][lm_1] == '\"' || tokens[0][lm_1] == '\'')
      {
      // Trim off trailing quote:
      tokens[0] = tokens[0].substr(0, lm_1);
      }
    }

  std::string mangled = mangler(tokens[0].c_str());
  //std::string cep_mangled = "_prog_entry_point_" + mangled;
  std::string cep_mangled = "_prog_entry_point_" + tokens[0];
  const char *replacement = named_string(cep_mangled.c_str());
  if( replacement )
    {
    retval = replacement;
    free((void *)replacement);
    // Rebuild the original request, after replacing the symbol with source:line
    for(size_t i=1; i<tokens.size(); i++)
      {
      retval += ' ';
      retval += tokens[i];
      }
    }

  if(retval.size())
    {
    *arg += strlen(*arg);
    return retval.c_str();
    }

  retval = *arg;
  *arg += strlen(*arg);
  return retval.c_str();
  }

int
cobol_language::parser(struct parser_state *par_state) const
  {
  /* Setting up the parser state.  */
  scoped_restore pstate_restore = make_scoped_restore (&pstate);
  gdb_assert (par_state != NULL);

  pstate = par_state;
  block_symbol bsym = {};

  // It wasn't an ordinary symbol.  Let's see if it is a '$' variable:
  if( par_state->lexptr[0] == '$' )
    {
    // Assume a simple single thing, like "$pc"
    struct stoken str;
    str.ptr    = par_state->lexptr;
    str.length = strlen(par_state->lexptr);
    par_state->push_dollar(str);
    pstate->set_operation (pstate->pop ());
    pstate->prev_lexptr = pstate->lexptr;
    pstate->prev_lexptr = pstate->lexptr;
    pstate->lexptr += strlen(pstate->lexptr);
    return 0;
    }
  // Check if it is a cobol variable name:
  const struct block *block_of_interest = par_state->expression_context_block;
  if( par_state->expression_context_block )
    {
    block_of_interest = par_state->expression_context_block;
    }
  else
    {
    block_of_interest = current_block();
    }
  cobol_vars = variable_blocks.populate(block_of_interest);
  if( cobol_vars )
    {
    // Dubner doesn't know how to parse using modern technology, so he is
    // fumbling along in a pure "get it working" mode

    // Start looking at the statement to be parsed:
    char ch_first = par_state->lexptr[0];
    bool is_unary_address = ch_first == '&';

    if( is_unary_address )
      {
      par_state->lexptr += 1;
      }

    cblc_refer_t refer;

    // Let's look for a COBOL variable:
    char *root_name = strdup(par_state->lexptr);
    char *p_equal = strchr(root_name, '=');
    // Note that p_equal is possibly a pointer inside the strduped root_name,
    // so we need to free it after we are done with
    if( p_equal )
      {
      // There is an equal sign in the string.  Terminate at that point for
      // now; we'll pick it up again later.
      *p_equal = '\0';
      }
    char *p = strchr(root_name, '(');
    if( p )
      {
      *p = '\0';
      }

    // The clumsy parsing up above is intended to cope with
    //     AAA OF BBB OF CCC = ....
    // and
    //     AAA OF BBB OF CCC(....
    //
    // cobol_vars->lookup_symbol is designed to work with AAA OF BBB OF CCC,
    // but with everything after that stripped off.
    VBLOCK_SYMBOL bsyms = cobol_vars->lookup_symbol(root_name);

    if( bsyms.size() == 1 )
      {
      bsym = bsyms[0];
      }
    else if( bsyms.size() > 1 )
      {
      fprintf(stderr, "There are %ld possibilities, "
                      "and we need to figure out what to do about that!\n",
                      bsyms.size());
      }

    if( bsym.symbol )
      {
      // The text is the name of an identifiable COBOL variable

      // First, push the base symbol
      cobol_block_symbol cbsym;
      cbsym.bsym  = bsym;
      cbsym.refer = refer;
      pstate->push_new<expr::cobol_var_value_operation> (cbsym);

      // See if there is a subscript modifier:
      char *p1     = strchr((char *)par_state->lexptr, '(');
      char *pcolon = strchr((char *)par_state->lexptr, ':');
      char *p2     = strchr((char *)par_state->lexptr, ')');
      if( p1<p2 && (!pcolon || pcolon > p2) )
        {
        if( false ) // Just to make sure the following reference code compiles
          {
          // We have paired parentheses, without a colon
          std::string subscripts = std::string(p1+1).substr(0, p2-(p1+1));
          struct typed_stoken tokens[1];
          tokens[0].type    = C_STRING;
          tokens[0].ptr     = (char *)subscripts.c_str();
          tokens[0].length  = (int)subscripts.length();
          struct stoken_vector vec[1];
          vec[0].len = 1;
          vec[0].tokens = tokens;
          pstate->push_c_string(C_STRING, vec);
          pstate->wrap2<expr::cobol_subscript_string_operation> ();
          }
        else
          {
          p1 += 1;
          while( p1 < p2 )
            {
            while( isspace(*p1) )
              {
              p1 += 1;
              }
            if( *p1 == ')' )
              {
              break;
              }
            int next_subscript = strtol(p1, &p1, 10);
            pstate->push_new<expr::long_const_operation>(
                  parse_type (pstate)->builtin_long,
                  next_subscript);
            pstate->wrap2<expr::cobol_subscript_entry_operation> ();
            }
          }
        p2 += 1;
        }
      else
        {
        p2 = NULL;
        }
      if( !p2 )
        {
        p2 = (char *)par_state->lexptr;
        }
      p1     = strchr(p2, '(');
      pcolon = strchr(p2, ':');
      p2     = strchr(p2, ')');
      if( p1<p2 && pcolon && pcolon > p1 && pcolon < p2 )
        {
        if( false )
          {
          // We have paired parentheses, with a colon
          std::string subscripts = std::string(p1+1).substr(0, p2-(p1+1));
          struct typed_stoken tokens[1];
          tokens[0].type    = C_STRING;
          tokens[0].ptr     = (char *)subscripts.c_str();
          tokens[0].length  = (int)subscripts.length();
          struct stoken_vector vec[1];
          vec[0].len = 1;
          vec[0].tokens = tokens;
          pstate->push_c_string(C_STRING, vec);
          pstate->wrap2<expr::cobol_refmod_string_operation> ();
          }
        else
          {
          // We have paired parentheses, with a colon
          int refmod_from = atoi(p1+1);
          int refmod_len  = atoi(pcolon+1);

          // if refmod_from is zero, make it the default value of 1.
          // length can be zero; it'll be handled later by anybody who needs
          // by setting it to cblc_field_t::capacity
          if(!refmod_from)
            {
            refmod_from = 1;
            }
          pstate->push_new<expr::long_const_operation>(
                    parse_type(pstate)->builtin_long,
                    refmod_from);
          pstate->wrap2<expr::cobol_refmod_from_operation>();
          pstate->push_new<expr::long_const_operation>(
                    parse_type (pstate)->builtin_long,
                    refmod_len);
          pstate->wrap2<expr::cobol_refmod_length_operation> ();
          }
        }

      
      if( is_unary_address )
        {
        // There is a '&' in front of the symbol: "print &aaa"
        pstate->wrap<expr::cobol_unop_addr_operation> ();
        }
      else 
        {
        // We have the bysm set up as a cobol_var_value_operation.  Let's see
        // if there was an equals sign
        if( p_equal )
          {
          // There was!  We need to set up an assignment operation

          // Find the equal sign again
          p_equal = strchr((char *)par_state->lexptr, '=');
          gdb_assert(p_equal);

          // We are going to feed the entire RHS to the
          // cobol_assign_from_string_operation. That code will decide what to
          // do with '"123"' versus an unquoted '123'
          std::string rhs = p_equal+1;
          pstate->push_new<expr::string_operation>(rhs);
          pstate->wrap2<expr::cobol_assign_from_string_operation>();
          }
        }
      }

    xfree(root_name);
    if( bsym.symbol )
      {
      sv_recent_bsym = bsym;
      pstate->set_operation (pstate->pop ());
      pstate->prev_lexptr = pstate->lexptr;
      pstate->lexptr += strlen(pstate->lexptr);
      return 0;
      }
    }

  // Let's see if the string is a valid number

  long the_value;
  if( valid_numerical_constant(pstate->lexptr, the_value) )
    {
    pstate->push_new<expr::long_const_operation>(
              parse_type(pstate)->builtin_long,
              the_value);
    pstate->set_operation (pstate->pop ());
    pstate->prev_lexptr = pstate->lexptr;
    pstate->lexptr += strlen(pstate->lexptr);
    return 0;
    }

  // When all else fails, look for an non-cobol symbol.  I discovered this to
  // be necessary when "print var" showed me an element of an 
  // "enum expression_operator".  I never actually found it.  But if the COBOL
  // program has a "var" in it, and the following test is done first, then the
  // COBOL var isn't displayed.

  bsym = lookup_symbol( par_state->lexptr,
                        par_state->expression_context_block,
                        SEARCH_VFT,
                        NULL);
  
  if( bsym.symbol )
    {
    pstate->push_new<expr::var_value_operation> (bsym);
    pstate->set_operation (pstate->pop ());
    pstate->prev_lexptr = pstate->lexptr;
    pstate->lexptr += strlen(pstate->lexptr);
    return 0;
    }

  gdb_exception except;
  except.reason = RETURN_ERROR;
  except.error = GENERIC_ERROR;
  error(_("No symbol \"%s\" in current context."), par_state->lexptr);
  throw(except);
  return 1;
  }

block_symbol
cobol_language::get_recent_bsym()
  {
  return sv_recent_bsym;
  }

static void
alpha_to_alpha_move(cobol_value_field &dest,
                    const char *from,
                    bool move_all = false)
  {
  // This is a helper function, called when it is known that both source
  // and dest are alphanumeric
  char *to         = (char *)dest.data();
  size_t source_length = strlen(from);
  size_t dest_length   = dest.capacity();
  size_t count     = std::min(source_length, dest_length);

  if( source_length >= dest_length )
    {
    // We have more source characters than places to put them
    if( dest.attr() & rjust_e )
      {
      // Destination is right-justified, so we
      // discard the leading source characters:
      memmove(to,
              from + (source_length - count),
              count);
      }
    else
      {
      // Destination is right-justified, so we
      // discard the trailing source characters:
      memmove(to,
              from,
              count);
      }
    }
  else
    {
    // We have too few source characters to fill the destination.
    if( dest.attr() & rjust_e )
      {
      // The destination is right-justified
      if( move_all )
        {
        // and the source is move_all.  We will repeat the input until
        // it fills the output, starting from the right side.
        // Note the oddball termination condition, recalling that i
        // and source.length are unsigned
        size_t isource = source_length-1;
        for(size_t i=dest_length-1; i<dest_length; i--)
          {
          to[i] = from[isource--];
          if( isource >= source_length )
            {
            isource = source_length-1;
            }
          }
        }
      else
        {
        // The destination is right-justified, and the source is an
        // ordinary string too short to fill it.  So, we space-fill
        // the leading characters.
        // We do the move first, in case this is an overlapping move
        // involving characters that will be space-filled
        memmove(to + (dest_length-count),
                from,
                count);
        memset(to, SPACE, dest_length-count);
        }
      }
    else
      {
      // The source is smaller than the destination
      // The destination is left-justified
      if( move_all )
        {
        // and the source is move_all.  We will repeat the input until
        // it fills the output, starting from the left side.
        size_t isource = 0;
        for(size_t i=0; i<dest_length; i++)
          {
          to[i] = from[isource++];
          if( isource >= source_length )
            {
            isource = 0;
            }
          }
        }
      else
        {
        // The destination is right-justified, and the source is an
        // ordinary string too short to fill it.  So, we space-fill
        // the trailing characters.
        // We do the move first, in case this is an overlapping move
        // involving characters that will be space-filled
        memmove(to,
                from,
                count);
        memset( to + count,
                SPACE,
                dest_length-count);
        }
      }
    }
  }

static void
binary_to_big_endian(   unsigned char *dest,
                        int            bytes,
                        __int128       value
                        )
    {
    if( value < 0 )
        {
        memset(dest, 0xFF, bytes);
        }
    else
        {
        memset(dest, 0x00, bytes);
        }

    dest += bytes-1;
    while( bytes-- )
        {
        *dest-- = (unsigned char) value;
        value >>= 8;
        }
    }

static void
binary_to_little_endian(   unsigned char *dest,
                        int            bytes,
                        __int128       value
                        )
    {
    if( value < 0 )
        {
        memset(dest, 0xFF, bytes);
        }
    else
        {
        memset(dest, 0x00, bytes);
        }
    memcpy(dest, &value, bytes);
    }

static void
scale_and_assign_internal(  cobol_value_field &field,
                            __int128        value,
                            int             source_rdigits,
                            enum rounded_t  rounded,
                            bool           *perror)
  {
  bool size_error = false;

  int target_rdigits = field.rdigits();
  if( field.attr() & scaled_e )
    {
    // Our target is scaled.  No matter which way we are going, the result
    // going into memory has no decimal places.
    target_rdigits = 0;

    // We have some additional scaling of value to do to make things line up.

    if( field.rdigits() >= 0 )
      {
      // Our target is something like PPPPPP999, meaning that var->actual_length
      // is 3, and field.rdigits() is 6.

      // By rights, our caller should have given us something like 123 with
      // source_rdigits of 9.  So, we multiply by 10**9 to put the 123 just
      // to the left of the decimal point, so that they line up with the
      // target_rdigits of zero we are targeting:
      source_rdigits -= field.digits() + field.rdigits();
      if(source_rdigits < 0)
        {
        // We overshot
        value *= cobol_language::power_of_ten(-source_rdigits);
        source_rdigits = 0;
        }
      }
    else
      {
      // Our target is something like 999PPPPPP, so there is a ->digits
      // of 3 and field.rdigits() of -6.

      // If our caller gave us 123000000, we need to effectively divide
      // it by 1000000 to line up the 123 with where we want it to go:

      source_rdigits += (-field.rdigits());
      }
    // Either way, we now have everything aligned for the remainder of the
    // processing to work:
    }

  // Convert the scale of value to match the scale of var
  if( source_rdigits < target_rdigits )
    {
    // Multiply value by ten until the source_rdigits match
    value *= cobol_language::power_of_ten(target_rdigits - source_rdigits);
    source_rdigits = target_rdigits;
    }

  if( source_rdigits > target_rdigits )
    {
    // We're going to divide value by 10 until we are within
    // one rdigit:
    value /= cobol_language::power_of_ten(source_rdigits - (target_rdigits+1));
    source_rdigits = (target_rdigits+1);
    // value now has one extra digit to the right:
    if( rounded == rounded_e )
      {
      // At the present time, we can only perform ROUNDED-AWAY-FROM-ZERO
      if( value < 0 )
        {
        value -= 5;
        }
      else
        {
        value += 5;
        }
      }
    value /= 10;
    }

  // Value is now scaled to the target's target_rdigits

  int is_negative = value < 0 ;

  if( !(field.attr() & signable_e) && is_negative )
    {
    if(false)
      {
      // I believe the COBOL spec allows for throwing INCOMPATIBLE-DATA
      // errors.  <sound effect: can being kicked down road>
      // printf(  "runtime exception: assigning negative "
               // "value to unsigned variable %s\n",
               // var->name);
      }
    // Take the absolute value of value
    value = -value;
    is_negative = false;
    }

  // And now we put value where it belongs
  switch( field.type() )
    {
    case FldGroup:
    case FldLiteralA:
    case FldAlphanumeric:
      // This is sort of a Hail Mary play.  We aren't supposed to do this
      // conversion if rdigits is non-zero.  But we shouldn't have gotten
      // here if rdigits is non-zero.  So, we'll just go with the flow.

      // Note that sending a signed value to an alphanumeric strips off
      // any '+' or '-' signs.
      size_error = cobol_language::binary_to_string((char *)field.data(), field.capacity(), value);
      break;

    case FldNumericDisplay:
      if( field.attr() & signable_e )
        {
        // Things get exciting when a numeric-display value is signable

        if( field.attr() & separate_e )
          {
          // Whether positive or negative, a sign there will be:
          char sign_ch = is_negative ? '-' : '+' ;
          if( field.attr() & leading_e )
            {
            // The sign character goes into the first location
            size_error = cobol_language::binary_to_string((char *)(field.data()+1), field.capacity()-1, value);
            field.data()[0] = sign_ch;
            }
          else
            {
            // The sign character goes into the last location
            size_error = cobol_language::binary_to_string((char *)field.data(), field.capacity()-1, value);
            field.data()[field.capacity()-1] = sign_ch;
            }
          }
        else
          {
          // The sign information is not separate, so we put it into the number
          size_error = cobol_language::binary_to_string((char *)field.data(), field.capacity(), value);
          if( is_negative )
            {
            if( field.attr() & leading_e )
              {
              // The sign bit goes into the first digit:
              field.data()[0] |= NUMERIC_DISPLAY_SIGN_BIT;
              }
            else
              {
              // The sign bit goes into the last digit:
              field.data()[field.capacity()-1] |= NUMERIC_DISPLAY_SIGN_BIT;
              }
            }
          }
        }
      else
        {
        // It's a simple positive number
        size_error = cobol_language::binary_to_string((char *)field.data(), field.capacity(), value);
        }

      break;

    case FldNumericEdited:
      {
      if( value == 0 && (field.attr() & blank_zero_e) )
        {
        memset(field.data(), SPACE, field.capacity());
        }
      else
        {
        char ach[128];

        // At this point, value is scaled to the target's rdigits

        size_error = cobol_language::binary_to_string(ach, field.digits(), value);
        ach[field.digits()] = '\0';

        // Convert that string according to the PICTURE clause
        size_error |= cobol_language::string_to_numeric_edited(  (char *)field.data(),
                      ach,
                      target_rdigits,
                      is_negative,
                      field.picture());
        }

      break;
      }

    case FldNumericBinary:
      binary_to_big_endian(   field.data(),
                              field.capacity(),
                              value);
      break;

    case FldNumericBin5:
    case FldIndex:
    case FldLiteralN:
      binary_to_little_endian(field.data(),
                              field.capacity(),
                              value);
      break;

    case FldPacked:
      {
      // The value is already scaled.  We need to put the digits into place:
      int nplaces = 2 * field.capacity();
      if( !(field.attr() & separate_e) )
        {
        nplaces -= 1;
        // We need to establish the sign nybble:
        if( field.attr() & signable_e )
          { 
          if( value < 0 )
            {
            field.data()[nplaces/2] = 0x0D;
            }
          else
            {
            field.data()[nplaces/2] = 0x0C;
            }
          }
        else
          {
          field.data()[nplaces/2] = 0x0F;
          }
        }
      if( value < 0 )
        {
        value = -value;
        }
      while(nplaces--)
        {
        int digit = value % 10;
        value /= 10;
        if( nplaces & 1)
          {
          // This digit is the rightward nybble:
          field.data()[nplaces/2] = digit;
          }
        else
          {
          field.data()[nplaces/2] += (digit<<4);
          }
        }

      break;
      }

    case FldAlphaEdited:
      {
      char ach[128];
      size_error = cobol_language::binary_to_string(ach, field.capacity(), value);
      ach[field.capacity()] = '\0';

      // Convert that string according to the PICTURE clause
      cobol_language::string_to_alpha_edited(
        (char *)field.data(),
        ach,
        strlen(ach),
        field.picture());
      break;
      }

    default:
      printf("can't convert in %s()\n", __func__);
      exit(1);
      break;
    }
  if( perror )
    {
    *perror = size_error;
    }
  }

void
cobol_language::cobol_assign_value_from_string(struct value *val_lhs, const char *str)
  {
  // fprintf(stderr, "Original String is %s\n", str);
  // Call this routine when val_lhs is known good.  str must be either quoted
  // or unquoted.  The quotes can be single or double.  The processing will
  // depend on whether it is quoted or not, and on the type of the LHS variable.
  // For example, if FldGroup or Alphanumeric, str will be moved over "as is",
  // quoted or not.  When assigned to a numeric, the quotes will be stripped
  // before the conversion takes place.
  cobol_value_field field_lhs(val_lhs);

  // Normalize the input string into several normalized forms.
  std::string str_original = cobol_stuff::trimr(std::string(str));
  std::string str_lowered = lowered(str_original);
  std::string str_no_quotes = str_original;
  bool was_quoted = 0;
  char first_char = 0;
  char final_char = 0;
  __int128 numeric_value;
  int rdigits;
  bool moved = true;

  if( str_original.length() >= 2 )
    {
    first_char = str_original[0];
    final_char  = str_original[str_original.length()-1];
    }

  if( first_char == '\'' && final_char == '\'' )
    {
    // We are working with an apostrophe-quoted phrase
    was_quoted = true;
    str_no_quotes = str_no_quotes.substr(1, str_no_quotes.size()-2);
    // Replace doubled apostrophes with singles:
    for(;;)
      {
      size_t nfound = str_no_quotes.find("''");
      if( nfound == std::string::npos )
        {
        break;
        }
      str_no_quotes = str_no_quotes.substr(0, nfound+1)
                      + str_no_quotes.substr(nfound+2);
      }
    }

  if( first_char == '\"' && final_char == '\"' )
    {
    // We are working with an quote-quoted phrase
    was_quoted = true;
    str_no_quotes = str_no_quotes.substr(1, str_no_quotes.size()-2);
    // Replace doubled quotes with singles:
    for(;;)
      {
      size_t nfound = str_no_quotes.find("\"\"");
      if( nfound == std::string::npos )
        {
        break;
        }
      str_no_quotes = str_no_quotes.substr(0, nfound+1)
                      + str_no_quotes.substr(nfound+2);
      }
    }

  // Assigns 'str' to 'val'.  'str' is human input, so it gets treated as dirty

  //  The cast of characters:
  //  str_original  is the original RHS string
  //  str_lowered   is the same string, converted to lowercase
  //  str_no_quotes is the original, but with leading and trailing single or
  //                double quotes removed
  //  was_quoted is true if the original was quoted.

  cbl_figconst_t source_figconst;

  if(   str_lowered == "zero"
        || str_lowered == "zeros"
        || str_lowered == "zeroes")
    {
    source_figconst = zero_value_e;
    }
  else if( str_lowered == "space" || str_lowered == "spaces")
    {
    source_figconst = space_value_e;
    }
  else if( str_lowered == "high-value" || str_lowered == "high-values")
    {
    source_figconst = high_value_e;
    }
  else if( str_lowered == "low-value" || str_lowered == "low-values")
    {
    source_figconst = low_value_e;
    }
  else if( str_lowered == "quote" || str_lowered == "quotes")
    {
    source_figconst = quote_value_e;
    }
  else
    {
    source_figconst = normal_value_e;
    }

  char figconst_char;
  // Special processing for figurative constants:
  switch(source_figconst)
    {
    case low_value_e:
      figconst_char = 0;
      break;
    case zero_value_e:
      figconst_char = '0';
      break;
    case space_value_e:
      figconst_char = ' ';
      break;
    case quote_value_e:
      figconst_char = quote_character;
      break;
    case high_value_e:
      figconst_char = 0xFF;
      break;
    default:
      figconst_char = 0;
      break;
    }

  switch( field_lhs.type() )
    {
    case FldGroup:
    case FldAlphanumeric:
      if(source_figconst)
        {
        // This is a figurative constant, so fill the destination
        // with the correct byte value:
        memset(field_lhs.data(), figconst_char, field_lhs.capacity());
        }
      else
        {
        // Because we are moving to an alphanumeric, we just do exactly what
        // is asked of us:
        alpha_to_alpha_move(field_lhs, str_no_quotes.c_str());
        }
      break;

    case FldAlphaEdited:
      {
      if(source_figconst)
        {
        // Create a string made up of figconst characters:
        str_no_quotes.clear();
        for(size_t i=0; i<field_lhs.capacity(); i++)
          {
          str_no_quotes += figconst_char;
          }
        }
      cobol_language::string_to_alpha_edited( (char *)field_lhs.data(),
                                              str_no_quotes.c_str(),
                                              str_no_quotes.length(),
                                              field_lhs.picture());
      break;
      }

    case FldNumericEdited:
      if(source_figconst)
        {
        // This is a figurative constant, so fill the destination
        // with the correct byte value:
        memset(field_lhs.data(), figconst_char, field_lhs.capacity());
        }
      else
        {
        if( was_quoted )
          {
          // It was quoted, so don't fuss around; just shove in exactly
          // what was specified.
          alpha_to_alpha_move(field_lhs, str_no_quotes.c_str());
          }
        else
          {
          // It wasn't quoted, so we treat the string like a number:
          bool is_negative = false;
          numeric_value = cobol_language::dirty_to_binary(str_no_quotes.c_str(),
                                        str_no_quotes.length(),
                                        &rdigits );
          if( numeric_value < 0 )
            {
            numeric_value = -numeric_value;
            is_negative = true;
            }

          // rdigits is the number of decimals in the source.  We need to make
          // it line up with the target:

          if(rdigits < field_lhs.rdigits())
            {
            numeric_value *= cobol_language::power_of_ten( field_lhs.rdigits()-rdigits);
            }
          else if(rdigits > field_lhs.rdigits())
            {
            numeric_value /= cobol_language::power_of_ten( rdigits-field_lhs.rdigits());
            }

          if( numeric_value == 0 && field_lhs.attr() & blank_zero_e )
            {
            memset(field_lhs.data(), SPACE, field_lhs.capacity());
            }
          else
            {
            // In order to process it as numeric edited, we need to convert
            // that converted binary number back to a string:
            char ach[128];
            cobol_language::binary_to_string(ach, field_lhs.digits(), numeric_value);
            ach[field_lhs.digits()] = '\0';

            // Convert that string according to the PICTURE clause
            cobol_language::string_to_numeric_edited( (char *)field_lhs.data(),
                                      ach,
                                      field_lhs.rdigits(),
                                      is_negative,
                                      field_lhs.picture());
            }
          }
        }
      break;

    case FldNumericDisplay:
    case FldNumericBinary:
    case FldNumericBin5:
    case FldIndex:
    case FldPacked:
    case FldPointer:
      numeric_value = cobol_language::dirty_to_binary(str_no_quotes.c_str(),
                                    str_no_quotes.length(),
                                    &rdigits);
      scale_and_assign_internal(  field_lhs,
                                  numeric_value,
                                  rdigits,
                                  unrounded_e,
                                  NULL);
      break;

    case FldFloat:
      {
      switch( field_lhs.capacity() )
        {
        case 4:
          {
          _Float32 value = strtof32(str_no_quotes.c_str(), NULL);
          memcpy(field_lhs.data(), &value, 4);
          break;
          }
        case 8:
          {
          _Float64 value = strtof64(str_no_quotes.c_str(), NULL);
          memcpy(field_lhs.data(), &value, 8);
          break;
          }
        case 16:
          {
          _Float128 value = strtof128(str_no_quotes.c_str(), NULL);
          memcpy(field_lhs.data(), &value, 16);
          break;
          }
        }
      
      break;
      }

    default:
      moved = false;
      break;
    }
  if( !moved )
    {
    error("<error: unimplemented conversion>");
    }

  target_write_memory(field_lhs.core_addr(),
                      field_lhs.data(),
                      field_lhs.capacity());
  }

static void
cobol_assign_value_from_value(struct value *val_lhs, struct value *val_rhs)
  {
  // Call this routine when val_lhs and val_rhs are known values:
  cobol_value_field field_lhs(val_lhs);
  cobol_value_field field_rhs(val_rhs);
  std::string str_rhs = field_rhs.format_for_display();
  cobol_language::cobol_assign_value_from_string(val_lhs, str_rhs.c_str());
  }

void
cobol_language::parse_assignment_string(struct value *val_lhs, const char *str_original_)
  {
  cobol_value_field field(val_lhs);
//  __int128 numeric_value;

  // Normalize the input string into several normalized forms.
  std::string str_original = cobol_stuff::trim(str_original_);

  // Before proceeding let's see if the string we were handed is a COBOL
  // variable name:

  const struct block *expression_context_block = nullptr;
  CORE_ADDR expression_context_pc = 0;
  /* If no context specified, try using the current frame, if any.  */
  if( !expression_context_block )
    expression_context_block = get_selected_block (&expression_context_pc);

  CobolVars *cobol_vars =
    variable_blocks.populate(expression_context_block);
  gdb_assert(cobol_vars);
  VBLOCK_SYMBOL bsyms = cobol_vars->lookup_symbol(str_original.c_str());
  block_symbol bsym;

  if( bsyms.size() == 1 )
    {
    bsym = bsyms[0];
    value *val_rhs = evaluate_var_value(EVAL_NORMAL, bsym.block, bsym.symbol);
    cobol_assign_value_from_value(val_lhs, val_rhs);
    return;
    }
  else if( bsyms.size() > 1 )
    {
    error("There are %ld possibilities for %s,\n", bsyms.size(), str_original.c_str());
    }

  // str_original is not a variable name 
  cobol_language::cobol_assign_value_from_string(val_lhs, str_original.c_str());
  }
