/*
 * Copyright (c) 2021-2023 Symas Corporation
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following disclaimer
 *   in the documentation and/or other materials provided with the
 *   distribution.
 * * Neither the name of the Symas Corporation nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
   Portions of this software were derived from pre-existing code in GDB,
   which contained this notice:

   Copyright (C) 1992-2022 Free Software Foundation, Inc.

   This file is part of GDB.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

#ifndef COBOL_VARS_H
#define COBOL_VARS_H

#include <map>
#include <unordered_map>

typedef std::vector<std::string> VSTRING;
typedef std::vector<block_symbol> VBLOCK_SYMBOL;

extern std::string lowered(const std::string &s);

class CobolParagraphName
  {
  private:
    std::string m_original;
    std::string m_paragraph;          // unmangled
    std::string m_section;            // unmangled
    std::string m_mangled_paragraph;  
    std::string m_mangled_program;    
    size_t      m_unique_id;
    size_t      m_symbol_index;

  public:
    void        original(const char *s){m_original = s;}
    std::string original(){return m_original;}
    void        paragraph(const char *s){m_paragraph = lowered(std::string(s));}
    void        paragraph(const std::string &s){m_paragraph = lowered(s);}
    std::string paragraph() const {return m_paragraph;}
    void        section(const char *s){m_section = lowered(std::string(s));}
    void        section(const std::string &s){m_section = lowered(s);}
    std::string section() const {return m_section;}
    void        mangled_paragraph(const char *s){m_mangled_paragraph = s;}
    std::string mangled_paragraph() const {return m_mangled_paragraph;}
    void        mangled_program(const char *s){m_mangled_program = s;}
    std::string mangled_program() const {return m_mangled_program;}
    void        unique_id(size_t n){m_unique_id = n;}
    size_t      unique_id() const {return m_unique_id;}
    void        symbol_index(size_t n){m_symbol_index = n;}
    size_t      symbol_index() const {return m_symbol_index;}
  };

class CobolSectionName
  {
  private:
    std::string m_original;
    std::string m_section;          // demangled
    std::string m_mangled_section;
    std::string m_mangled_program;    
    size_t      m_unique_id;
    size_t      m_symbol_index;

  public:
    void        original(const char *s){m_original = s;}
    std::string original(){return m_original;}
    void        section(const char *s){m_section = lowered(std::string(s));}
    void        section(const std::string &s){m_section = lowered(s);}
    std::string section() const {return m_section;}
    void        mangled_section(const char *s){m_mangled_section = s;}
    std::string mangled_section() const {return m_mangled_section;}
    void        mangled_program(const char *s){m_mangled_program = s;}
    std::string mangled_program() const {return m_mangled_program;}
    void        unique_id(size_t n){m_unique_id = n;}
    size_t      unique_id() const {return m_unique_id;}
    void        symbol_index(size_t n){m_symbol_index = n;}
    size_t      symbol_index() const {return m_symbol_index;}

  };

class CobolVarNode
  {
  private:
    std::string                  dotted_name      ;
    std::string                  our_name         ;
    size_t                       our_number       ;
    size_t                       parent_number    ;
    symbol                      *sym              ;
    CobolVarNode                *parent           ;
    std::vector<CobolVarNode *>  children         ;

  public:
    CobolVarNode()
      {
      dotted_name = "";
      our_name = "";
      our_number = 0;
      parent_number = 0;
      sym    = NULL;
      parent = NULL;
      children.clear();
      }
  void set_sym(symbol *s){sym = s;}
  void set_dotted_name(std::string &s){dotted_name = s;}
  void set_name(std::string &s){our_name = s;}
  void set_our_number(size_t n){our_number = n;}
  void set_parent_number(size_t n){parent_number = n;}
  void set_parent(CobolVarNode * node){parent = node;}
  void add_child(CobolVarNode * node){children.push_back(node);}
  size_t number_of_children()const{return children.size();};
  CobolVarNode *get_child(size_t i){return children[i];}
  symbol *get_sym(){return sym;}
  size_t get_our_number(){return our_number;}
  size_t get_parent_number(){return parent_number;}
  std::string   get_name(){return our_name;}
  std::string   get_dotted_name(){return dotted_name;}
  CobolVarNode* get_parent(){return parent;}
  } ;

typedef std::vector<CobolVarNode*> VCOBOL_VAR_NODE;

class NameToNode
  {
  private:
    // lower-case a/b/c version of A OF B OF C
    std::string     normalized_name  ;

    CobolVarNode   *node   ;

  public:
    NameToNode(){node = NULL;}
    void set_name(std::string s){normalized_name = s;}
    void set_node(CobolVarNode *n){node = n;}
    CobolVarNode *get_node(){return node;}
    std::string get_name(){return normalized_name;}

  friend bool operator==(const NameToNode &n1, const NameToNode &n2);
  friend bool operator!=(const NameToNode &n1, const NameToNode &n2);
  friend bool operator<(const NameToNode &n1, const NameToNode &n2);
  friend bool operator>(const NameToNode &n1, const NameToNode &n2);
  friend bool operator<=(const NameToNode &n1, const NameToNode &n2);
  friend bool operator>=(const NameToNode &n1, const NameToNode &n2);

  };

class CobolVars
  {
  private:
    const struct block *m_block;         // The block holding these symbols
    std::vector<CobolVarNode *> m_nodes;
    std::vector<CobolSectionName *> section_names;
    std::vector<CobolParagraphName *> paragraph_names;
    // The map_of_performs is a std::map because we are going to take advantage
    // of the sorted nature of the map.
    std::map<unsigned long, std::string> map_of_performs;
    CobolVarNode *root;
    std::vector<NameToNode *>m_name_nodes;

    void build_name_nodes(CobolVarNode *node);
    void dump_names(FILE *f);
    std::string normalize_identifier(const char *lexptr);
    VSTRING identifier_pieces(const std::string &s);
    void AddSectionName(const char *sect_name);
    void AddParagraphName(const char *para_name);
    void sort_paragraph_and_section_names();

  public:
    void populate(const struct block *blk);
    VBLOCK_SYMBOL lookup_symbol(const char *lexptr);
    VCOBOL_VAR_NODE lookup_node(const char *lexptr);
    CobolVarNode *lookup_node_from_dotted_name(const std::string &dotted_name);
    std::string find_paragraph( const std::string &paragraph,
                                const std::string &section);
    std::string find_minsym(unsigned long addr);
    const char *ProgramName(){return m_block->function() ? m_block->function()->m_name : NULL;}
  };

class CobolVarBlocks
  {
  private:
    std::unordered_map<const struct block *, CobolVars *> map_of_CobolVars;

  public:
    CobolVars *find(const struct block *blk);
    CobolVars *populate(const struct block *blk);
    CobolVarNode *get_node_from_value(const struct value *);
  };

class ProcedureCallAndReturn
  {
  private:
    std::unordered_map<std::string, size_t> m_the_map;        // For proccall
    std::unordered_map<std::string, size_t> m_the_other_map;  // For proccallb
    std::string m_stashed;
    int m_triggered;

  public:
    ProcedureCallAndReturn() : m_triggered(0){}
    void Populate(const char *filename);
    const char *MatchingReturnLocation(const char *s);
    void StashReturnLocation(const char *s){m_stashed=s;}
    bool MatchesStash(const char *s);
  };

// This global variable is where we keep blocks as we learn about them.
extern CobolVarBlocks variable_blocks;

// This global variable is where we keep the relationships between
// PERFORM <proc> locations and the matching location of the associated tbreak
// necessary to NEXT over the PERFORM
extern ProcedureCallAndReturn procedure_call_and_return;

extern struct symtab_and_line string_to_sal(const char *str);
unsigned long string_to_pc(const char *str);

// Okay, so sometimes Python is actually nice:
VSTRING split(const std::string &str, char ch);
namespace cobol_stuff
{
std::string trim(const std::string &str);
std::string trimr(const std::string &str);
}

/* Functions implemented in lexer */
CobolVars * cobol_scan_init( parser_state *par_state );
void cobol_scan_fini();


#endif

