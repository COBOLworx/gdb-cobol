/*
 * Copyright (c) 2021-2023 Symas Corporation
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following disclaimer
 *   in the documentation and/or other materials provided with the
 *   distribution.
 * * Neither the name of the Symas Corporation nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
   Portions of this software were derived from pre-existing code in GDB,
   which contained this notice:

   Copyright (C) 1992-2022 Free Software Foundation, Inc.

   This file is part of GDB.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

#include "defs.h"

#include <ctype.h>

#include "block.h"
#include "c-lang.h"
#include "c-lang.h"
#include "charset.h"
#include "cp-support.h"
#include "demangle.h"
#include "gdbarch.h"
#include "infcall.h"
#include "objfiles.h"
#include "psymtab.h"
#include "typeprint.h"
#include "valprint.h"
#include "varobj.h"
#include <algorithm>
#include <string>
#include <vector>
#include "cli/cli-style.h"
#include "parser-defs.h"
#include "source.h"
#include "linespec.h"
#include "arch-utils.h"
#include "top.h"

#include "cobol-lang.h"
#include "cobol-parse.h"
#include "cobol-vars.h"
#include "ui.h"

#define SECTION_TAG   "_sect."
#define PARAGRAPH_TAG "_para."
#define PROCCALL_TAG  "_proccall."
#define PROCCALLB_TAG  "_proccallb."
#define PROCRET_TAG   "_procret."
#define PROCRETB_TAG  "_procretb."
#define FILLER_TAG    "_filler"

// This global variable is where we keep blocks as we learn about them.
CobolVarBlocks variable_blocks;

// This global variable is where we keep PERFORM call and return relationships
// so we can implement NEXT OVER PERFORM <proc>
ProcedureCallAndReturn procedure_call_and_return;

/*  COBOL has the concept of paragraphs, and it's very common to 
   "PERFORM <para>".  A paragraph is not a function that can be called. Instead,
   PERFORM <para> is implemented by means of jmp instructions.
   
   Implementing NEXT-over-PERFORM means, therefore, identifying a PERFORM
   statement, putting a temporary breakpoint at the point of "jmp to return
   location", replacing the call to "step_1" with a call to "continue_command",
   and after the breakpoint occurs issuing another "step_1" to bring the 
   execution point back to the statement following the PERFORM.
   
   // These variables are needed to implement the NEXT-over-PERFORM for the
   // cases where NEXT issued by the terminal or from a script, or encountered
   // during a sequence started with a NEXT <N> command.
   */

// sv_state_of_next=0 means ordinary processing
// sv_state_of_next=1 means a temporary breakpoint has been set, and when
//                    reached an extra next must be issued
// sv_state_of_next=2 means a temporary breakpoint has been set, and when
//                    reached we should just stop with a prompt.
static int sv_state_of_next = 0;
static int sv_next_state_type = 0;
static int sv_mirrored_skip_subroutines = 0;
static int sv_mirrored_single_inst      = 0;
static int sv_mirrored_count            = 0;
static int sv_waiting_for_temporary_breakpoint = 0;

std::string
lowered(const std::string &s)
  {
  std::string retval;
  for(size_t i=0; i<s.size(); i++)
    {
    retval += tolower(s[i]);
    }
  return retval;
  }

bool
operator==(const NameToNode &n1, const NameToNode &n2)
  {
  std::string nn1 = lowered(n1.normalized_name);
  std::string nn2 = lowered(n2.normalized_name);
  return nn1 == nn2;
  }
bool
operator!=(const NameToNode &n1, const NameToNode &n2)
  {
  std::string nn1 = lowered(n1.normalized_name);
  std::string nn2 = lowered(n2.normalized_name);
  return nn1 != nn2;
  }
bool
operator<(const NameToNode &n1, const NameToNode &n2)
  {
  std::string nn1 = lowered(n1.normalized_name);
  std::string nn2 = lowered(n2.normalized_name);
  return nn1 < nn2;
  }
bool
operator>(const NameToNode &n1, const NameToNode &n2)
  {
  std::string nn1 = lowered(n1.normalized_name);
  std::string nn2 = lowered(n2.normalized_name);
  return nn1 > nn2;
  }
bool
operator<=(const NameToNode &n1, const NameToNode &n2)
  {
  std::string nn1 = lowered(n1.normalized_name);
  std::string nn2 = lowered(n2.normalized_name);
  return nn1 <= nn2;
  }
bool
operator>=(const NameToNode &n1, const NameToNode &n2)
  {
  std::string nn1 = lowered(n1.normalized_name);
  std::string nn2 = lowered(n2.normalized_name);
  return nn1 >= nn2;
  }

CobolVars *
CobolVarBlocks::find(const struct block *block)
  {
  // Look for block in our map of blocks:
  std::unordered_map<const struct block *, CobolVars *>::const_iterator it =
    map_of_CobolVars.find(block);
  if( it!= map_of_CobolVars.end() )
    {
    return it->second;
    }
  else
    {
    return NULL;
    }
  }

CobolVars *
CobolVarBlocks::populate(const struct block *block)
  {
  CobolVars *retval = NULL;
  if( block )
    {
    retval = find(block);
    if( !retval )
      {
      // We haven't seen this block before, so it is time to create a
      // container for it, and populate it:
      retval = new CobolVars;
      map_of_CobolVars[block] = retval;
      retval->populate(block);
      }
    }

  return retval;
  }

void
CobolVars::build_name_nodes(CobolVarNode *node)
  {
  // nodes with the number zero are not placed in the name_nodes list.  This
  // means the root node doesn't go here.
  if(node->get_our_number())
    {
    // When you arrive here, build up the formal name for this node:
    std::string formal_name = "";
    CobolVarNode *walk_up = node;
    while(walk_up->get_parent())
      {
      formal_name += '/';
      for(size_t i=0; i<walk_up->get_name().size(); i++)
        {
        formal_name += tolower(walk_up->get_name()[i]);
        }
      walk_up = walk_up->get_parent();
      }
    if(node->get_parent())
      {
      // The root node does not get a string.  All others terminate with '/'
      formal_name += '/';
      }

    // Put that information into the name_nodes list:
    NameToNode *name_node = new NameToNode;
    name_node->set_name(formal_name);
    name_node->set_node(node);
    m_name_nodes.push_back(name_node);
    }

  for(size_t i=0; i<node->number_of_children(); i++)
    {
    build_name_nodes(node->get_child(i));
    }
  }

void
CobolVars::dump_names(FILE *f)
  {
  for(size_t i=0; i<m_name_nodes.size(); i++)
    {
    fprintf(stderr, "%s(): %s", __func__, m_name_nodes[i]->get_name().c_str());
    if(f)
      {
      fprintf(f, "%s(): %s", __func__, m_name_nodes[i]->get_name().c_str());
      }
    CobolVarNode *cvn = m_name_nodes[i]->get_node();
    if( cvn )
      {
      symbol *sym = cvn->get_sym();
      fprintf(stderr, " %s", cvn->get_name().c_str());
      fprintf(stderr, " %s", sym->m_name);
      if(f)
        {
        fprintf(f, " %s", cvn->get_name().c_str());
        fprintf(f, " %s", sym->m_name);
        }
      }
    fprintf(stderr, "\n");
    if(f)
      {
      fprintf(f, "\n");
      }
    }
  }

static
bool
name_node_ptr_compare(NameToNode *n1, NameToNode *n2)
  {
  return *n1 < *n2 ;
  }

static bool
compare_sect_names( CobolSectionName *p1, CobolSectionName *p2 )
  {
  return p1->section() < p2->section();
  }

static bool
compare_para_names( CobolParagraphName *p1, CobolParagraphName *p2 )
  {
  if( p1->paragraph() < p2->paragraph() )
    {
    return true;
    }
  else if( p1->paragraph() >= p2->paragraph() )
    {
    return false;
    }
  return p1->section() < p2->section() ;
  }

void CobolVars::sort_paragraph_and_section_names()
  {
  std::sort(section_names.begin(), section_names.end(), compare_sect_names);
  std::sort(paragraph_names.begin(), paragraph_names.end(), compare_para_names);
  }

static
char *
next_dot(char *s)
  {
  char *retval = NULL;
  if( s )
    {
    retval = strchr(s, '.');
    if( retval )
      {
      *retval = '\0';
      retval += 1;
      }
    }
  return retval;
  }

void
CobolVars::AddSectionName(const char *section_name_in)
  {
  // section names are of the form
  //    _sect.              SECTION_TAG
  //                        empty paragraph space
  //    _14_workhorse_sect9 mangled name (unmangled workhorse-sec)
  //    prog1               mangled program name
  //    0                   unique program id
  //    71                  symbol table index of section label
  // skip the initial PARAGRAPH_TAG,
  char *para_name           = xstrdup(section_name_in + strlen(SECTION_TAG));
  char *section_name        = next_dot(para_name         );
  char *program_name        = next_dot(section_name      );
  char *program_identifier  = next_dot(program_name      );
  char *symtab_index        = next_dot(program_identifier);

  if(    symtab_index 
      && this->ProgramName()
      && strcmp(this->ProgramName(), program_name) == 0 )
    {
    // We have a well-formed section name, and it's in this context block

    // For a CobolParagraphName entry, we need
    //   std::string m_paragraph;         // demangled paragraph
    //   std::string m_section;           // demangled section
    //   std::string m_mangled_paragraph; // original mangled paragraph

    char *demangled_section = cobol_language::demangler(section_name);

    CobolSectionName *csn = new CobolSectionName;
    csn->original(section_name_in);
    csn->section(demangled_section);
    csn->mangled_section(section_name);
    csn->mangled_program(program_name);
    csn->unique_id(atoi(program_identifier));
    csn->symbol_index(atoi(symtab_index));

    section_names.push_back(csn);

    free(demangled_section);

    }
  free(para_name);
  }

void
CobolVars::AddParagraphName(const char *para_name_in)
  {
  // The para_name_in generated by GCOBOL is

  // _para._6_para_14._14_workhorse_sect9.prog1.0.69
  //    _para.          PARAGRAPH_TAG
  //    _6_para_14      mangled paragraph (unmangled is para-1)
  //    workhorse_sect9 mangled section (unmangled is workhorse-sect)
  //    prog1           mangled name from COBOL program-id
  //    0               monotonic numeric program identification number
  //    69              The compiler's symbol-table index for the paragraph label

  // skip the initial PARAGRAPH_TAG,
  char *para_name           = xstrdup(para_name_in + strlen(PARAGRAPH_TAG));
  char *section_name        = next_dot(para_name         );
  char *program_name        = next_dot(section_name      );
  char *program_identifier  = next_dot(program_name      );
  char *symtab_index        = next_dot(program_identifier);

  char *demangled_program_name = cobol_language::demangler(program_name);

  if( symtab_index 
      && this->ProgramName()
      && strcasecmp(this->ProgramName(), demangled_program_name) == 0 )
    {
    // We have a well-formed paragraph name, and it's in this context block

    char *demangled_sect = cobol_language::demangler(section_name);
    char *demangled_para = cobol_language::demangler(para_name);

    CobolParagraphName *cpn = new CobolParagraphName;
    cpn->original(para_name_in);
    cpn->section(demangled_sect);
    cpn->paragraph(demangled_para);
    cpn->mangled_paragraph(para_name);
    cpn->mangled_program(program_name);
    cpn->unique_id(atoi(program_identifier));
    cpn->symbol_index(atoi(symtab_index));
    paragraph_names.push_back(cpn);

    free(demangled_para);
    free(demangled_sect);
    }
  free(demangled_program_name);
  free(para_name);
  }

#define ALL_DICT_SYMBOLS(dict, iter, sym)			\
	for ((sym) = mdict_iterator_first ((dict), &(iter));	\
	     (sym);						\
	     (sym) = mdict_iterator_next (&(iter)))

void
CobolVars::populate(const struct block *block)
  {
  m_block = block;

  struct mdict_iterator miter;

  struct symbol *sym;

  // It is my belief that symbols will come in family-tree order, with
  // ancestors followed by descendents.  But I am going to write this code
  // as if the ordering is not predetermined.  So, we build a list of
  // CobolVarNode nodes in a first pass, and then we link them together
  // in a second.

  std::unordered_map<size_t, CobolVarNode *>our_index_node;

  // The root node is the zeroth node.  It makes the following processing simpler,
  // because 01 and 77 variables won't have parents, so their parent_numbers
  // will be zero, and so they will automatically become children of the root.

  CobolVarNode *new_node;
  new_node = new CobolVarNode;
  m_nodes.push_back(new_node);
  our_index_node[0] = new_node;
  root = new_node;

  // Here is the first pass, where we build the list of nodes

  FILE *fpop = NULL;
  if( getenv("POPULATE") )
    {
    fpop = fopen(getenv("POPULATE"), "w");
    }

  ALL_DICT_SYMBOLS( block->multidict(), miter, sym )
    {
    if( fpop )
      {
      fprintf(stderr, "ALL_DICT_SYMBOLS: %s\n", sym->m_name);
      fprintf(  fpop, "ALL_DICT_SYMBOLS: %s\n", sym->m_name);
      }

    if( sym->m_name[0] == '.' || sym->m_name[0] == '_' )
      {
      // Symbols in the original COBOL source code can't start with '.' or 
      // '_', with the exception of "_filler..."
      if( strstr(sym->m_name, FILLER_TAG) != sym->m_name )
        {
        continue;
        }
      }

    if( fpop )
      {
      fprintf(stderr, "%s(): %s\n", __func__, sym->m_name);
      fprintf(  fpop, "%s(): %s\n", __func__, sym->m_name);
      }

    // We only care about what's after the first period:
    const char *first_dot = strchr(sym->m_name, '.');
    if( first_dot )
      {
      const char *second_dot = strchr(first_dot+1, '.');
      // We only care about symbols with two periods:
      if(second_dot)
        {
        // We are expecting either 12_34 or just 12.

        char *first_ender;
        size_t our_index = strtol(first_dot+1, &first_ender, 10);
        if( *first_ender == '_' )
          {
          // When *first_ender is not '_', just leave it; the result will be
          // a parent_index of zero, which does the job for us.
          first_ender += 1;
          }
        size_t parent_index = strtol(first_ender, NULL, 10);

        if( our_index )
          {
          std::string dotted_name = sym->m_name;
          std::string s_name = dotted_name.substr(0, first_dot - sym->m_name);

          new_node = new CobolVarNode;
          new_node->set_dotted_name(dotted_name);
          new_node->set_name(s_name);
          new_node->set_sym(sym);
          new_node->set_our_number(our_index);
          new_node->set_parent_number(parent_index);
          if( false && fpop )
            {
            fprintf(stderr, "      dotted_name: %s\n", dotted_name.c_str());
            fprintf(stderr, "             name: %s\n", s_name.c_str());
            fprintf(stderr, "    parent_number: %ld\n", parent_index);
            fprintf(stderr, "       our_number: %ld\n", our_index);

            fprintf(  fpop, "      dotted_name: %s\n", dotted_name.c_str());
            fprintf(  fpop, "             name: %s\n", s_name.c_str());
            fprintf(  fpop, "    parent_number: %ld\n", parent_index);
            fprintf(  fpop, "       our_number: %ld\n", our_index);
            }
          m_nodes.push_back(new_node);
          our_index_node[our_index] = new_node;
          }
        }
      }
    }

  // End the list:
  new_node = new CobolVarNode;
  m_nodes.push_back(new_node);

  // In this second pass, we create the parent and children links between the nodes:

  for(std::vector<CobolVarNode *>::iterator node = m_nodes.begin();
      node != m_nodes.end();
      node++)
    {
    CobolVarNode *child_node = *node;

    // We leave the root node, with our_number == 0, alone
    if( child_node->get_our_number() )
      {
      // Find the parent of this child:
      size_t parent_number = child_node->get_parent_number();
      std::unordered_map<size_t, CobolVarNode *>::const_iterator it
        = our_index_node.find(parent_number);

      if( fpop )
        {
        fprintf(stderr,
                "Second pass; working on %s.  Parent is %ld\n",
                child_node->get_dotted_name().c_str(),
                parent_number);
        fprintf(fpop,
                "Second pass; working on %s.  Parent is %ld\n",
                child_node->get_dotted_name().c_str(),
                parent_number);
        }

      // It's possible not to find the parent number.  For example, when the
      // record is an FD record, the parent is the index of the FldFile, and not
      // another FldField.

      if( it != our_index_node.end() )
        {
        CobolVarNode *parent_node = it->second;

        child_node->set_parent(parent_node);
        parent_node->add_child(child_node);
        }
      else
        {
        child_node->set_parent_number(0);
        child_node->set_parent(root);
        root->add_child(child_node);
        }
      }
    }

  // Third pass: We walk our brand-new tree in order to build the
  // normalized-names to symbols list:

  build_name_nodes(root);

  // To facilitate the binary search code, we will put impossible symbols at
  // the very beginning and end of the list.  Note that every valid name starts
  // with a '/', so our fenceposts start with '.', which is smaller, and '0',
  // which is bigger.
  NameToNode *name_node;
  name_node = new NameToNode;
  name_node->set_name(".-fencepost");
  m_name_nodes.push_back(name_node);
  name_node = new NameToNode;
  name_node->set_name("0-fencepost");
  m_name_nodes.push_back(name_node);

  // That list needs to be sorted by name in order to be useful:
  std::sort(m_name_nodes.begin(), m_name_nodes.end(), name_node_ptr_compare);

  // We need to pick up the paragraph and section tags, finding their
  // startpoints and endpoints

  struct symtab_and_line cursal
                = get_current_source_symtab_and_line (current_program_space);
  const char *filename = cursal.pspace->exec_bfd()->filename;

  for( objfile *objfile : current_program_space->objfiles () )
    {
    if( strcmp( objfile->original_name, filename) == 0 )
      {
      if( fpop )
        {
        fprintf(stderr,"minimal_symbols from %s\n", objfile->original_name);
        fprintf(  fpop,"minimal_symbols from %s\n", objfile->original_name);
        }
      for (minimal_symbol *msymbol : objfile->msymbols ())
        {
        if( fpop )
          {
          fprintf(stderr,
                  "minimum symbol natural_name %s\n",
                  msymbol->natural_name());
          fprintf(  fpop,
                  "minimum symbol natural_name %s\n",
                  msymbol->natural_name());
          }
        if( strstr(msymbol->natural_name(), PARAGRAPH_TAG)
                  == msymbol->natural_name() )
          {
          if( fpop )
            {
            fprintf(stderr,
                    "minimum symbol paragraph %s\n",
                    msymbol->natural_name());
            fprintf(  fpop,
                    "minimum symbol paragraph %s\n",
                    msymbol->natural_name());
            }
          AddParagraphName(msymbol->natural_name());
          }
        else if( strstr(msymbol->natural_name(), SECTION_TAG)
                  == msymbol->natural_name() )
          {
          if( fpop )
            {
            fprintf(stderr,
                    "minimum symbol section %s\n",
                    msymbol->natural_name());
            fprintf(  fpop,
                    "minimum symbol section %s\n",
                    msymbol->natural_name());
            }
          AddSectionName(msymbol->natural_name());
          }
        }
      }
    }
  sort_paragraph_and_section_names();

  if( getenv("DUMP_NAME_NODES") )
    {
    FILE *f = fopen(getenv("DUMP_NAME_NODES"), "w");
    dump_names(f);
    fclose(f);
    }
  }

std::string
CobolVars::normalize_identifier(const char *lexptr)
  {
  // We are going to convert "AaA of BBb in ccC" to "aaa/bbb/ccc"
  std::string working = lexptr;
  size_t slen = working.size();

  // I don't know why there would be tabs or linefeeds in here.  But let's
  // never worry about it.
  for(size_t i=0; i<slen; i++)
    {
    if( isspace(working[i]) )
      {
      working[i] = ' ';
      }
    else
      {
      working[i] = tolower(working[i]);
      }
    }

  // Replace any occurrences of "of" or "in" with "/"
  for(;;)
    {
    size_t nfound;
    nfound = working.find(" of ");
    if( nfound != std::string::npos )
      {
      working[nfound+1] = '/';
      working[nfound+2] = ' ';
      continue;
      }
    nfound = working.find(" in ");
    if( nfound != std::string::npos )
      {
      working[nfound+1] = '/';
      working[nfound+2] = ' ';
      continue;
      }
    break;
    }

  // Squeeze away any spaces:
  std::string retval;
  for(size_t i=0; i<slen; i++)
    {
    char ch = working[i];
    if( ch != ' ' )
      {
      retval += ch;
      }
    }

  return retval;
  }

VSTRING
CobolVars::identifier_pieces(const std::string &s)
  {
  VSTRING retval;
  std::string piece = "/";
  const char *p = s.c_str();
  while(*p)
    {
    char ch = *p++;
    piece += ch;
    if(ch == '/')
      {
      retval.push_back(piece);
      piece = "/";
      continue;
      }
    }
  if( piece.size() > 1 )
    {
    piece += '/';
    retval.push_back(piece);
    piece = "/";
    }

  return retval;
  }

VBLOCK_SYMBOL
CobolVars::lookup_symbol(const char *lexptr)
  {
  VBLOCK_SYMBOL retval;
  if( *lexptr )
    {
    // Convert the user's input to aaa/bbb/ccc
    std::string s = normalize_identifier(lexptr);

    // Bust it up into digestable pieces:
    VSTRING pieces = identifier_pieces(s);

    // Use a binary search to find where the first matching element might be:

    NameToNode key;
    key.set_name(pieces[0]);

    std::vector<NameToNode *>::const_iterator it =
      std::lower_bound(m_name_nodes.begin(), m_name_nodes.end(), &key, name_node_ptr_compare);

    while( it != m_name_nodes.end() )
      {
      NameToNode *name_node = *it++;

      // Make sure this name_node starts with what we are looking for:
      std::string node_name = name_node->get_name();
      size_t nfound = node_name.find(pieces[0]);
      if( nfound != 0 )
        {
        // The first characters do not match, so we are done looking
        break;
        }

      // The beginning of node_name is the same as piece[0].  So, we need to march
      // through the rest of the pieces to see if they are all present in the
      // correct order:

      bool okay = true;
      for( size_t i=1; i<pieces.size(); i++ )
        {
        nfound = node_name.find(pieces[i], nfound+1);
        if( nfound == std::string::npos )
          {
          // A necessary piece was not found
          okay = false;
          break;
          }
        }

      if( okay )
        {
        // We have found a variable that matches the A OF B OF C requirements
        block_symbol bsym;
        CobolVarNode *cvn = name_node->get_node();
        bsym.symbol = cvn->get_sym();
        bsym.block  = m_block;
        retval.push_back(bsym);
        }
      }
    }
  return retval;
  }

VCOBOL_VAR_NODE
CobolVars::lookup_node(const char *lexptr)
  {
  VCOBOL_VAR_NODE retval;

  // Convert the user's input to aaa/bbb/ccc
  std::string s = normalize_identifier(lexptr);

  // Bust it up into digestable pieces:
  VSTRING pieces = identifier_pieces(s);

  // Use a binary search to find where the first matching element might be:

  NameToNode key;
  key.set_name(pieces[0]);

  std::vector<NameToNode *>::const_iterator it =
    std::lower_bound(m_name_nodes.begin(), m_name_nodes.end(), &key, name_node_ptr_compare);

  while( it != m_name_nodes.end() )
    {
    NameToNode *name_node = *it++;

    // Make sure this name_node starts with what we are looking for:
    std::string node_name = name_node->get_name();
    size_t nfound = node_name.find(pieces[0]);
    if( nfound != 0 )
      {
      // The first characters do not match, so we are done looking
      break;
      }

    // The beginning of node_name is the same as piece[0].  So, we need to march
    // through the rest of the pieces to see if they are all present in the
    // correct order:

    bool okay = true;
    for( size_t i=1; i<pieces.size(); i++ )
      {
      nfound = node_name.find(pieces[i], nfound+1);
      if( nfound == std::string::npos )
        {
        // A necessary piece was not found
        okay = false;
        break;
        }
      }

    if( okay )
      {
      // We have found a variable that matches the A OF B OF C requirements
      CobolVarNode *cvn = name_node->get_node();
      retval.push_back(cvn);
      }
    }
  return retval;
  }

CobolVarNode *
CobolVars::lookup_node_from_dotted_name(const std::string &dotted_name_)
  {
  CobolVarNode *retval = NULL;

  std::string dotted_name = lowered(dotted_name_);

  // Typically, this routine is called from get_node_from_value(), which means
  // we have the partially dotted name.  That is, www.13_12.1 shows up as
  // www.13_12

  // Use a binary search to find where the first matching element might be.

  size_t nfound = dotted_name.find('.');
  std::string name_root = dotted_name;
  NameToNode key;
  if( nfound != std::string::npos )
    {
    name_root = dotted_name.substr(0, nfound) ;
    }
  // The elements of name_nodes all start with '/', so we need to prepend
  // that to what we are looking for:
  name_root = "/" + name_root + "/";
  key.set_name(name_root);

  std::vector<NameToNode *>::const_iterator it =
    std::lower_bound( m_name_nodes.begin(),
                      m_name_nodes.end(),
                      &key, name_node_ptr_compare);

  while( it != m_name_nodes.end() )
    {
    // We look through the list until the name_node->get_name() has a different
    // root:
    NameToNode *name_node = *it++;
    //fprintf(stderr, "%s %s\n", name_root.c_str(), lowered(name_node->get_name()).c_str());

    if( lowered(name_node->get_name()).find(name_root) != 0 )
      {
      break;
      }

    // We have an element with the same root name; see if it has the same
    // dotted name.

    // dotted_value has the final .<program_id_number> removed; GDB does that.
    // Our name_nodes list has the program_id_numbers.  We need to trim them off

    std::string table_entry = lowered(name_node->get_node()->get_dotted_name());
    nfound = table_entry.find('.');
    if( nfound != std::string::npos )
      {
      nfound = table_entry.find('.', nfound+1);
      if( nfound != std::string::npos )
        {
        table_entry = table_entry.substr(0, nfound);
        }
      }

    //fprintf(stderr, "     %s %s\n", dotted_name.c_str(), table_entry.c_str());
    if( dotted_name == table_entry )
      {
      // It has the same name, so we have found what we came to find
      retval = name_node->get_node();
      break;
      }
    }
  return retval;
  }

static std::string
remove_program_id_number(const std::string &s)
  {
  std::string retval = s;
  size_t nfound = s.find('.');
  if( nfound != std::string::npos )
    {
    nfound = s.find('.', nfound+1);
    if( nfound != std::string::npos )
      {
      retval = retval.substr(0, nfound);
      }
    }
  return retval;
  }

static std::string
rehyphenate(const std::string &s)
  {
  // The gcobol mangler:
  // 1) identifiers starting with a digit get an underscore prepended;
  // 2) hyphens get converted to '$'
  // This routine undoes that.

  std::string retval = s;
  if( s.size() >= 2 )
    {
    if( s[0] == '_'  && isdigit(s[1]) )
      {
      retval = s.substr(1);
      }
    for(size_t i=0; i<retval.size(); i++)
      {
      if( retval[i] == '$' )
        {
        retval[i] = '-';
        }
      }
    retval = remove_program_id_number(retval);
    }
  return retval;
  }

CobolVarNode *
CobolVarBlocks::get_node_from_value(const struct value *val)
  {
  CobolVarNode *retval = NULL;

  // This magical code gets the information about a symbol
  // from its address in inferior memory:
  struct type *type = check_typedef(val->type());
  struct gdbarch *gdbarch = type->arch();
  std::string name;
  std::string filename;
  int unmapped = 0;
  int offset = 0;
  int line = 0;
  bool do_demangle = true;

  CORE_ADDR addr = val->address();
  if( build_address_symbolic( gdbarch,
                              addr,
                              do_demangle,
                              false,
                              &name,
                              &offset,
                              &filename,
                              &line,
                              &unmapped))
    {
    // Trouble at the mill
    return retval;
    }

  // This code was copied from parse_exp_in_context() in parse.c

  const struct block *expression_context_block = nullptr;
  CORE_ADDR expression_context_pc = 0;
  /* If no context specified, try using the current frame, if any.  */
  if( !expression_context_block )
    expression_context_block = get_selected_block (&expression_context_pc);

  CobolVars *cobol_var = variable_blocks.find(expression_context_block);
  gdb_assert(cobol_var);

  // We have recovered the dotted name, so find it:

  name = rehyphenate(name);
  retval = cobol_var->lookup_node_from_dotted_name(name);

 gdb_assert(retval);

  return retval;
  }

namespace cobol_stuff
{
std::string
trim(const std::string &str)
  {
  std::string retval;
  size_t left = std::string::npos;
  size_t right = std::string::npos;

  for( size_t i=0; i<str.size(); i++ )
    {
    if( str[i] != ' ' )
      {
      if( left == std::string::npos )
        {
        left = i;
        }
      right = i;
      }
    }
  if( right != std::string::npos )
    {
    retval = str.substr(left, right-left+1);
    }
  return retval;
  }

std::string
trimr(const std::string &str)
  {
  std::string retval;
  size_t right = std::string::npos;

  for( size_t i=0; i<str.size(); i++ )
    {
    if( str[i] != ' ' )
      {
      right = i;
      }
    }
  if( right != std::string::npos )
    {
    retval = str.substr(0, right+1);
    }
  return retval;
  }
}

VSTRING split(const std::string &str_, char ch)
  {
  // Splits the string using ch as the delimiter.  Double-quoted substrings are
  // treated as atomic; no attempt is made to check inside the quotes.

  VSTRING retval;

  bool shift_out = false;
  std::string str = cobol_stuff::trim(str_);
  std::string element;

  for(size_t i=0; i<str.size(); i++)
    {
    if( str[i] == '"' )
      {
      shift_out = !shift_out;
      }

    if( str[i] == ch && !shift_out )
      {
      if( !element.empty() )
        {
        retval.push_back(element);
        element.clear();
        }
      continue;
      }
    element += str[i];
    }
  if( !element.empty() )
    {
    retval.push_back(element);
    }
  return retval;
  }

std::string
CobolVars::find_paragraph( const std::string &paragraph,
                           const std::string &section)
  {
  std::string retval;

  int ncount = 0;

  // We are going to sweep through the section_names and paragraph_names looking
  // for matches that are valid for the current program.  If the final result
  // is a singleton, we return the original symbol.  Otherwise we report a
  // "there are too many" response, indicating that there are multiple sections
  // that have the same paragraph.

  if( section.empty() )
    {
    // without an explicit section, the paragraph might be a section or an
    // unqualified paragraph.

    // Let's look first for a matching section
    std::string look_for = lowered(paragraph);

    for(std::vector<CobolSectionName *>::const_iterator it = section_names.begin();
        it                                         != section_names.end();
        it++)
      {
      if( (*it)->section() == look_for )
        {
        retval = (*it)->original();
        ncount += 1;
        }
      }

    if( ncount == 0 )
      {
      // We didn't have a match on section name; let's check for a paragraph
      for(std::vector<CobolParagraphName* >::const_iterator it = paragraph_names.begin();
          it                                         != paragraph_names.end();
          it++)
        {
        if( (*it)->paragraph() == look_for )
          {
          retval = (*it)->original();
          ncount += 1;
          }
        }
      }
    }
  else
    {
    // We have a paragraph name qualified by a section name:
    std::string look_for_para = lowered(paragraph);
    std::string look_for_sect = lowered(section);

    for(std::vector<CobolParagraphName* >::const_iterator it = paragraph_names.begin();
        it                                         != paragraph_names.end();
        it++)
      {
      if(     (*it)->paragraph() == look_for_para
          &&  (*it)->section()   == look_for_sect)
        {
        retval = (*it)->original();
        ncount += 1;
        }
      }
    }

  if( ncount > 1 )
    {
    error(" There are %d possibilities for procedure \"%s\"",
            ncount,
            paragraph.c_str());
    retval.clear();
    }

  return retval;
  }

std::string CobolVars::find_minsym(unsigned long addr)
  {
  std::string retval;
  std::map<unsigned long, std::string>::const_iterator it =
    map_of_performs.find(addr);
  if( it != map_of_performs.end() )
    {
    retval = it->second;
    }
  return retval;
  }

unsigned long
string_to_pc(const char *str)
  {
  const char *locstr = xstrdup(str);
  const char *ploc = locstr;

  location_spec_up locspec = string_to_location_spec (&ploc,
                                                       current_language);
  free((void *)locstr);

  const struct breakpoint_ops *ops
          = breakpoint_ops_for_location_spec (locspec.get (),
                                              false /* is_tracepoint */);
  struct linespec_result canonical;
  location_spec *plocspec = locspec.get();
  struct program_space *search_pspace=NULL;
  ops->create_sals_from_location_spec (plocspec, &canonical, search_pspace);

  const linespec_sals &lsal = canonical.lsals[0];

  unsigned long retval = lsal.sals[0].pc;

  return retval;
  }

struct symtab_and_line
string_to_sal(const char *str)
  {
  const char *locstr = xstrdup(str);
  const char *ploc = locstr;

  location_spec_up locspec = string_to_location_spec (&ploc,
                                                       current_language);
  free((void *)locstr);

  const struct breakpoint_ops *ops
          = breakpoint_ops_for_location_spec (locspec.get (),
                                              false /* is_tracepoint */);
  struct linespec_result canonical;
  location_spec *plocspec = locspec.get();
  struct program_space *search_pspace = NULL;
  ops->create_sals_from_location_spec (plocspec, &canonical, search_pspace);

  const linespec_sals &lsal = canonical.lsals[0];

  unsigned long pc = lsal.sals[0].pc;

  struct symtab_and_line retval = find_pc_line (pc, 0);
  return retval;
  }

const char *
ProcedureCallAndReturn::MatchingReturnLocation(const char *s)
  {
  static char retval[24];
  std::unordered_map<std::string, size_t>::const_iterator it;

  it = m_the_map.find(s);
  if( it != m_the_map.end() )
    {
    sv_next_state_type = 1;
    sprintf(retval, "%s%ld", PROCRET_TAG, it->second);
    return retval;
    }
  
  it = m_the_other_map.find(s);
  if( it != m_the_other_map.end() )
    {
    sv_next_state_type = 2;
    sprintf(retval, "%s%ld", PROCRETB_TAG, it->second);
    return retval;
    }
  
  *retval = '\0';
  return retval;
  }

void
ProcedureCallAndReturn::Populate(const char *filename)
  {
  if( m_the_map.size() == 0 )
    {
    for( objfile *objfile : current_program_space->objfiles () )
      {
      for (minimal_symbol *msymbol : objfile->msymbols ())
        {
        if( strstr(msymbol->natural_name(), PROCCALL_TAG)
                  == msymbol->natural_name() )
          {
          // This is the special symbol created to mark the point in a program
          // where there is a PERFORM <proc> or PERFORM <proc1> THRU <proc2>

          // Extract the identifying symbol table index of the procedure
          size_t st_index = 0;
          const char *p = msymbol->natural_name();
          p = strchr(p, '.');
          if( p )
            {
            p += 1;
            st_index = atoi(p);
            }

          // Find the file:line of the proccall symbol:
          struct symtab_and_line pc_sal = string_to_sal(msymbol->natural_name());
          int line = pc_sal.line;
          filename = pc_sal.symtab->fullname();

          char ach[512];
          sprintf(ach,
                  "%s:%d",
                  filename,
                  line);
          // And put it into our memory book:
          if( m_the_map.find(ach) == m_the_map.end() )
            {
            m_the_map[ach] = st_index;
            }
          }
        if( strstr(msymbol->natural_name(), PROCCALLB_TAG)
                  == msymbol->natural_name() )
          {
          // This is the special symbol created to mark the point in a program
          // where there is a PERFORM <proc> or PERFORM <proc1> THRU <proc2>

          // Extract the identifying symbol table index of the procedure
          size_t st_index = 0;
          const char *p = msymbol->natural_name();
          p = strchr(p, '.');
          if( p )
            {
            p += 1;
            st_index = atoi(p);
            }

          // Find the file:line of the proccallb symbol:
          struct symtab_and_line pc_sal = string_to_sal(msymbol->natural_name());
          int line = pc_sal.line;
          filename = pc_sal.symtab->fullname();

          char ach[512];
          sprintf(ach,
                  "%s:%d",
                  filename,
                  line);
          // And put it into our memory book:
          if( m_the_other_map.find(ach) == m_the_other_map.end() )
            {
            m_the_other_map[ach] = st_index;
            }
          }
        }
      }
    }
  }

bool
ProcedureCallAndReturn::MatchesStash(const char *s)
  {
  bool retval = m_stashed == std::string(s);
  m_triggered = retval;
  return retval;
  }

bool
cobol_language::catch_next(bool initial_command, const char *count_string)
  {
  int count = count_string ? parse_and_eval_long (count_string) : 1;
  if( initial_command )
    {
    sv_state_of_next = 0;
    sv_waiting_for_temporary_breakpoint = 0;
    sv_mirrored_skip_subroutines = 1;
    sv_mirrored_single_inst      = 0;
    sv_mirrored_count            = count;
    }
  if(    current_language->la_language == language_cobol 
      && sv_mirrored_skip_subroutines  // This is NEXT or NEXTI
      && !sv_mirrored_single_inst      // but not NEXTI
      )
    {
    // Figure out where we are trapped currently:
    struct symtab_and_line cursal;
    frame_info_ptr frame = NULL;
    if( target_has_registers() )
      {
      frame = get_current_frame();
      }
    if( frame )
      {
      cursal = find_frame_sal (frame);
      }
    if( frame && cursal.symtab )
      {
      int line = cursal.line;
      const char *filename = cursal.symtab->fullname();
    
      // Make sure our map of PERFORM-to-return locations is populated:
      procedure_call_and_return.Populate(filename);
    
      char ach[512];
      sprintf(ach,
              "%s:%d",
              filename,
              line);
      const char *return_trap
                        = procedure_call_and_return.MatchingReturnLocation(ach);
      if( *return_trap )
        {
        // Find the file:line_number that will be reported at return_trap
        symtab_and_line ret_cursal = string_to_sal(return_trap);
        int ret_line = ret_cursal.line;
        const char *ret_filename = ret_cursal.symtab->fullname();

        // It is frequently the case when processing a procretb that the current
        // line and the line returned by return_trap are the same line, because
        // 

        sprintf(ach,
          "%s:%d",
          ret_filename,
          ret_line);
        // And stash it
        procedure_call_and_return.StashReturnLocation(ach);
    
        // What follows is essentially a duplication of the
        //     tbreak_command(return_trap, from_tty=0);
        // command.
        // Unfortunately, the the tbreak command generates terminal output, even
        // when the from_tty switch is off.  To supress that output, the
        // create_breakpoint's formal parameter "internal" must be set to 1, but as
        // of this writing, the GDB code provides is no way to do that.
    
        // Hence this code, cloned from break_command_1:
    
        location_spec_up locspec;
        locspec = string_to_location_spec (&return_trap, current_language);
        const struct breakpoint_ops *ops
          = breakpoint_ops_for_location_spec (locspec.get (),
                                              false /* is_tracepoint */);
        int tempflag = disp_del_at_next_stop;
        bptype type_wanted = bp_breakpoint;
        int from_tty = 0;
        int internal = 1;
        auto_boolean pending_break_support = AUTO_BOOLEAN_AUTO;
        create_breakpoint (get_current_arch (),
               locspec.get (),
               NULL,
               -1             /* thread */,
               -1             /* inferior */,
               "", false, 1   /* parse arg */,
               tempflag,
               type_wanted,
               0              /* Ignore count */,
               pending_break_support,
               ops,
               from_tty,
               1              /* enabled */,
               internal,
               0);
        sv_waiting_for_temporary_breakpoint = 1;
        return true;
        }
      }
    }
  return false;
  }

int
cobol_language::suppress_notify_normal_stop()
  {
  // This routine is called by normal_stop() in infrun.c.  It detects that
  // normal_stop() took place at the temporary breakpoint that was set when
  // we intercepted a "next" command at the point of PERFORM <paragraph> that
  // was noticed by catch_next() up above.
  //
  // When that detection takes place, we set_state_of_next
  // start the state machine that will cause a "next"
  // command to be issued.

  int retval = 0;
  if( current_language->la_language == language_cobol 
      && sv_mirrored_skip_subroutines  // This is NEXT or NEXTI
      && !sv_mirrored_single_inst      // but not NEXTI
      )
    {
    struct symtab_and_line cursal;
    frame_info_ptr frame = NULL;
    if( target_has_registers() )
      {
      frame = get_current_frame();
      }
    if( frame )
      {
      cursal = find_frame_sal (frame);
      }
    if( frame && cursal.symtab )
      {
      int line = cursal.line;
      const char *filename = cursal.symtab->fullname();
      char ach[512];
      sprintf(ach,
              "%s:%d",
              filename,
              line);
  
      // See if ach matches the stashed file:line
      retval = procedure_call_and_return.MatchesStash( ach );
      procedure_call_and_return.StashReturnLocation("");

      if( retval )
        {
        // Set the state machine, indicating that we are at the return point
        // of a paragraph or section, and that an extra NEXT needs to
        // be issued to bring the execution point back to the statement following
        // the PERFORM.
        set_state_of_next(sv_next_state_type);
        }
      }
    }
  return get_state_of_next();
  }

void
cobol_language::set_state_of_next(int n)
  {
  sv_state_of_next=n;
  }

int
cobol_language::get_state_of_next()
  {
  int retval = sv_state_of_next;
  return retval;
  }

int
cobol_language::should_we_issue_extra_next()
  {
  int retval = get_state_of_next();
  set_state_of_next(0);
  if(retval)
    {
    sv_waiting_for_temporary_breakpoint = 0;
    sv_mirrored_skip_subroutines = 0;
    sv_mirrored_single_inst      = 0;
    }
  return retval;
  }

void
cobol_language::mirrored_fsm_prepare( int skip_subroutines,
                                      int single_inst,
                                      int count)
  {
  if( current_language->la_language == language_cobol )
    {
    sv_mirrored_skip_subroutines = skip_subroutines;
    sv_mirrored_single_inst      = single_inst ;
    sv_mirrored_count            = count;
    }
  }

int
cobol_language::mirrored_should_stop()
  {
  int retval = 1;
  if( current_language->la_language == language_cobol )
    {
    if(    sv_mirrored_skip_subroutines  // This is NEXT or NEXTI
        && !sv_mirrored_single_inst      // but not NEXTI
        && !sv_waiting_for_temporary_breakpoint)
      {
      sv_mirrored_count -= 1;
      sv_mirrored_count = std::max(sv_mirrored_count, 0);
      retval = sv_mirrored_count ? 0 : 1;
      }
    }
  return retval;
  }

int
cobol_language::get_mirrored_count()
  {
  return sv_mirrored_count;
  }

int
cobol_language::waiting_for_temporary_breakpoint()
  {
  int retval = sv_waiting_for_temporary_breakpoint;
  return retval;
  }