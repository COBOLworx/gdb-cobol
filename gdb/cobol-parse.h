/*
 * Copyright (c) 2021-2023 Symas Corporation
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following disclaimer
 *   in the documentation and/or other materials provided with the
 *   distribution.
 * * Neither the name of the Symas Corporation nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
   Portions of this software were derived from pre-existing code in GDB,
   which contained this notice:

   Copyright (C) 1992-2022 Free Software Foundation, Inc.

   This file is part of GDB.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

#ifndef COBOL_EXP_H
#define COBOL_EXP_H

#include "expop.h"
#include "cobol-valprint.h"

extern struct value *eval_op_cobol_complement (struct type *expect_type,
    struct expression *exp,
    enum noside noside,
    enum exp_opcode opcode,
    struct value *value);
extern struct value *eval_op_cobol_array (struct type *expect_type,
    struct expression *exp,
    enum noside noside,
    enum exp_opcode opcode,
    struct value *ncopies,
    struct value *elt);
extern struct value *eval_op_cobol_struct_anon (struct type *expect_type,
    struct expression *exp,
    enum noside noside,
    int field_number,
    struct value *lhs);
extern struct value *eval_op_cobol_structop (struct type *expect_type,
    struct expression *exp,
    enum noside noside,
    struct value *lhs,
    const char *field_name);

namespace expr
{

using cobol_unop_compl_operation = unop_operation<UNOP_COMPLEMENT,
      eval_op_cobol_complement>;
using cobol_array_operation = binop_operation<OP_ARRAY,
      eval_op_cobol_array>;

/* Implement the address-of operation.  */
class cobol_unop_addr_operation
  : public maybe_constant_operation<operation_up>
  {
  public:

    using maybe_constant_operation::maybe_constant_operation;

    value *
    evaluate (struct type *expect_type,
              struct expression *exp,
              enum noside noside) override
      {
      // Pick up the argument whose cblc_field_t::data we are going to display
      value *val = std::get<0> (m_storage)->evaluate (nullptr, exp, noside);
      cblc_refer_t *val_refer = (cblc_refer_t *)val->get_auxdata();
      val_refer->address_of = true;

      //value *val = std::get<0> (m_storage)->evaluate_for_address (exp, noside);
      return val;
      }

    enum exp_opcode
    opcode () const override
      {
      return UNOP_ADDR;
      }

    /* Return the subexpression.  */
    const operation_up &
    get_expression () const
      {
      return std::get<0> (m_storage);
      }

  protected:

    void
    do_generate_ax (struct expression *exp,
                    struct agent_expr *ax,
                    struct axs_value *value,
                    struct type *cast_type)
    override
      {
      gen_expr_unop (exp, UNOP_ADDR,
                     std::get<0> (this->m_storage).get (),
                     ax, value);
      }
  };


/* Tuple field reference (using an integer).  */
class cobol_struct_anon
  : public tuple_holding_operation<int, operation_up>
  {
  public:

    using tuple_holding_operation::tuple_holding_operation;

    value *
    evaluate (struct type *expect_type,
              struct expression *exp,
              enum noside noside) override
      {
      value *lhs = std::get<1> (m_storage)->evaluate (nullptr, exp, noside);
      return eval_op_cobol_struct_anon (expect_type, exp, noside,
                                        std::get<0> (m_storage), lhs);

      }

    enum exp_opcode
    opcode () const override
      {
      return STRUCTOP_ANONYMOUS;
      }
  };

/* Compute the value of a variable.  */
class cobol_var_value_operation
  : public maybe_constant_operation<cobol_block_symbol>
  {
  public:

    using maybe_constant_operation::maybe_constant_operation;

    value *
    evaluate (struct type *expect_type,
              struct expression *exp,
              enum noside noside) override
      {
      //fprintf(stderr, "cobol_var_value_operation::%s\n", __func__);
      symbol *sym = std::get<0> (m_storage).bsym.symbol;
      value *retval
        = evaluate_var_value(noside, std::get<0> (m_storage).bsym.block, sym);

      // When I implementing GDB-COBOL, I concluded that I needed a place to
      // carry the cblc_refer_t information along with the struct value.  There
      // didn't seem to be any place to put it, so I augmented struct value with
      // aux_data, so that there would be such a place.  If there is a better,
      // more GDB-ish, way to accomplish this, I was unable to figure it out.
      retval->allocate_auxdata(sizeof(cblc_refer_t));
      memcpy( retval->get_auxdata(),
              &(std::get<0> (m_storage).refer),
              sizeof(cblc_refer_t));
      return retval;
      }

    value *
    evaluate_with_coercion (struct expression *exp,
                            enum noside noside) override
      {
      fprintf(stderr, "cobol_var_value_operation::%s(): We don't know what to do!", __func__ );
      return NULL;
      }

    value *
    evaluate_for_sizeof (struct expression *exp, enum noside noside)
    override
      {
      fprintf(stderr, "cobol_var_value_operation::%s(): We don't know what to do!", __func__ );
      return NULL;
      }

    value *
    evaluate_for_cast (struct type *expect_type,
                       struct expression *exp,
                       enum noside noside) override
      {
      fprintf(stderr, "cobol_var_value_operation::%s(): We don't know what to do!", __func__ );
      return NULL;
      }

    value *
    evaluate_for_address (struct expression *exp, enum noside noside)
    override
      {
      fprintf(stderr, "cobol_var_value_operation::%s(): We don't know what to do!", __func__ );
      return NULL;
      }

    value *
    evaluate_funcall (struct type *expect_type,
                      struct expression *exp,
                      enum noside noside,
                      const std::vector<operation_up> &args) override
      {
      fprintf(stderr, "cobol_var_value_operation::%s(): We don't know what to do!", __func__ );
      return NULL;
      }

    enum exp_opcode
    opcode () const override
      {
      return OP_VAR_VALUE;
      }

    /* Return the symbol referenced by this object.  */
    symbol *
    get_symbol () const
      {
      return std::get<0> (m_storage).bsym.symbol;
      }

  protected:

    void
    do_generate_ax (struct expression *exp,
                    struct agent_expr *ax,
                    struct axs_value *value,
                    struct type *cast_type)
    override
      {
      fprintf(stderr, "cobol_var_value_operation::%s(): We don't know what to do!", __func__ );
      return;
      }
  };

/* subscript_string operator for Cobol.  */
class cobol_subscript_string_operation
  : public maybe_constant_operation<operation_up, operation_up>
  {
  public:

    using maybe_constant_operation::maybe_constant_operation;

    value *
    evaluate (struct type *expect_type,
              struct expression *exp,
              enum noside noside) override
      {
      //fprintf(stderr, "cobol_subscript_string_operation::%s\n", __func__);

      // Pick up the value whose refer we will modify:
      value *arg1 = std::get<0> (m_storage)->evaluate (nullptr, exp, noside);
      cblc_refer_t *refer = (cblc_refer_t *)arg1->get_auxdata();

      // Pick up the subscript string
      value *subscript_string = std::get<1> (m_storage)->evaluate (nullptr, exp, noside);

      // Apply that subscript string to the refer:
      cobol_language::cobol_subscript (refer, subscript_string);

      return arg1;
      }

    enum exp_opcode
    opcode () const override
      {
      return BINOP_SUBSCRIPT;
      }
  };

/* cobol_subscript_entry_operation operator for Cobol.  */
class cobol_subscript_entry_operation
  : public maybe_constant_operation<operation_up, operation_up>
  {
  public:

    using maybe_constant_operation::maybe_constant_operation;

    value *
    evaluate (struct type *expect_type,
              struct expression *exp,
              enum noside noside) override
      {
      //fprintf(stderr, "cobol_subscript_entry_operation::%s\n", __func__);

      // Pick up the value whose refer we will modify:
      value *arg1 = std::get<0> (m_storage)->evaluate (nullptr, exp, noside);
      cblc_refer_t *refer = (cblc_refer_t *)arg1->get_auxdata();

      // Pick up the subscript string
      value *subscript_value = std::get<1> (m_storage)->evaluate (nullptr, exp, noside);
      int subscript = (int)value_as_long(subscript_value);

      // Add the new subscript to the existing array of subscripts
      for(int i=0; i<MAXIMUM_TABLE_DIMENSIONS; i++)
        {
        if( refer->subscripts[i] == -1 )
          {
          refer->subscripts[i] = subscript;
          break;
          }
        }
      refer->is_dirty = true;

      return arg1;
      }

    enum exp_opcode
    opcode () const override
      {
      return BINOP_SUBSCRIPT;
      }
  };


/* Refmod operator for Cobol.  */
class cobol_refmod_string_operation
  : public maybe_constant_operation<operation_up, operation_up>
  {
  public:

    using maybe_constant_operation::maybe_constant_operation;

    value *
    evaluate (struct type *expect_type,
              struct expression *exp,
              enum noside noside) override
      {
      //fprintf(stderr, "cobol_refmod_string_operation::%s\n", __func__);
      // Pick up the value we are modifying with the refer:
      value *arg1 = std::get<0> (m_storage)->evaluate (nullptr, exp, noside);
      cblc_refer_t *refer = (cblc_refer_t *)arg1->get_auxdata();

      // Pick up the refmod string
      value *refmod_string = std::get<1> (m_storage)->evaluate (nullptr, exp, noside);

      // Apply that refmod string to the refer:
      cobol_language::cobol_refmod(refer, refmod_string);

      return arg1;
      }

    enum exp_opcode
    opcode () const override
      {
      return BINOP_SUBSCRIPT;
      }
  };

/* cobol_refmod_from_operation operator for Cobol.  */
class cobol_refmod_from_operation
  : public maybe_constant_operation<operation_up, operation_up>
  {
  public:

    using maybe_constant_operation::maybe_constant_operation;

    value *
    evaluate (struct type *expect_type,
              struct expression *exp,
              enum noside noside) override
      {
      //fprintf(stderr, "cobol_refmod_from_operation::%s\n", __func__);

      // Pick up the value whose refer we will modify:
      value *arg1 = std::get<0> (m_storage)->evaluate (nullptr, exp, noside);
      cblc_refer_t *refer = (cblc_refer_t *)arg1->get_auxdata();

      value *refmod_from_value = std::get<1> (m_storage)->evaluate (nullptr, exp, noside);
      int refmod_from = (int)value_as_long(refmod_from_value);

      // Put the new refmod_from value into the value
      refer->refmod_from = refmod_from;
      refer->is_dirty = true;

      return arg1;
      }

    enum exp_opcode
    opcode () const override
      {
      return BINOP_SUBSCRIPT;
      }
  };

/* cobol_refmod_length_operation operator for Cobol.  */
class cobol_refmod_length_operation
  : public maybe_constant_operation<operation_up, operation_up>
  {
  public:

    using maybe_constant_operation::maybe_constant_operation;

    value *
    evaluate (struct type *expect_type,
              struct expression *exp,
              enum noside noside) override
      {
      //fprintf(stderr, "cobol_refmod_length_operation::%s\n", __func__);

      // Pick up the value whose refer we will modify:
      value *arg1 = std::get<0> (m_storage)->evaluate (nullptr, exp, noside);

      cblc_refer_t *refer = (cblc_refer_t *)arg1->get_auxdata();

      value *refmod_length_value = std::get<1> (m_storage)->evaluate (nullptr, exp, noside);
      int refmod_length = (int)value_as_long(refmod_length_value);

      // Put the new refmod_len into the value
      refer->refmod_len = refmod_length;
      refer->is_dirty = true;

      return arg1;
      }

    enum exp_opcode
    opcode () const override
      {
      return BINOP_SUBSCRIPT;
      }
  };

/* cobol assignment operation operator.  */
class cobol_assign_from_string_operation
  : public tuple_holding_operation<operation_up, operation_up>
  {
  public:

    using tuple_holding_operation::tuple_holding_operation;

    value * evaluate (struct type *expect_type,
                      struct expression *exp,
                      enum noside noside) override
      {
      //fprintf(stderr, "cobol_refmod_length_operation::%s\n", __func__);

      // Pick up the value whose data is going to be changed
      value *arg1 = std::get<0> (m_storage)->evaluate (nullptr, exp, noside);

      // Pick up the value on the right side:
      value *arg2 = std::get<1> (m_storage)->evaluate (nullptr, exp, noside);
      gdb_assert(arg2);

      char *str = (char *)arg2->contents().data ();
      gdb_assert(str);

      cobol_language::parse_assignment_string(arg1, str);

      return arg1;
      }

    enum exp_opcode
    opcode () const override
      {
      return BINOP_ASSIGN;
      }
  };


} /* namespace expr */

#endif /* COBOL_EXP_H */

