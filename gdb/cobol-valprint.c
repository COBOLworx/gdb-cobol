/*
 * Copyright (c) 2021-2023 Symas Corporation
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following disclaimer
 *   in the documentation and/or other materials provided with the
 *   distribution.
 * * Neither the name of the Symas Corporation nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
   Portions of this software were derived from pre-existing code in GDB,
   which contained this notice:

   Copyright (C) 1992-2022 Free Software Foundation, Inc.

   This file is part of GDB.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */


#include "defs.h"

#include <ctype.h>

#include "block.h"
#include "c-lang.h"
#include "c-lang.h"
#include "charset.h"
#include "cp-support.h"
#include "demangle.h"
#include "gdbarch.h"
#include "infcall.h"
#include "objfiles.h"
#include "psymtab.h"
#include "cobol-lang.h"
#include "typeprint.h"
#include "valprint.h"
#include "varobj.h"
#include <algorithm>
#include <string>
#include <vector>
#include "cli/cli-style.h"
#include "parser-defs.h"
#include "cobol-parse.h"
#include "cobol-valprint.h"
#include "cobol-vars.h"

// Evntually, we will somehow or other need to replace 
// this with the executable's belief about
// whether DECIMAL IS COMMA
static char decimal_point = '.';
static char decimal_separator = ',';
// The following constants reflect that this code was copied from the
// libgcobol.cc source code
static const char dollar_sign[] = "$";
static const char internal_minus = '-';
static const char internal_plus  = '+';
static const char internal_0     = '0';
static const char internal_9     = '9';
static const int  MAX_FIXED_POINT_DIGITS = 37;
static const char internal_e     = 'e';
static const char internal_E     = 'E';

static const char 
            *currency_signs[256] =  {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,  // 0x00
                                     0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,   // 0x10
                                     0,0,0,0,(char *)dollar_sign,0,0,0,0,0,0,0,0,0,0,0, // 0x20
                                     0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,   // 0x30
                                     0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,   // 0x40
                                     0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,   // 0x50
                                     0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,   // 0x60
                                     0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,   // 0x70
                                     0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,   // 0x80
                                     0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,   // 0x90
                                     0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,   // 0xa0
                                     0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,   // 0xb0
                                     0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,   // 0xc0
                                     0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,   // 0xd0
                                     0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,   // 0xe0
                                     0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,   // 0xf0
                                    };

//  The following code is very much cloned from the GCC/GCOBOL source code.

//////////////////////////////// START OF GCOBOL-based code

bool
cobol_language::binary_to_string(char *result, int digits, __int128 value)
  {
  // Our caller has to keep track of whether value was negative:
  if( value < 0 )
    {
    value = -value;
    }

  if(digits)
    {
    // The result is not terminated, because this routine can be used
    // to put information directly into cblc_field_t::data
    result += digits-1 ;
    while( digits-- )
      {
      *result-- = value%10 + '0';
      value /= 10;
      }
    // Should value be non-zero, it means we potentially have a size error
    }
  else
    {
    // Because 'digits' is zero, we are dealing with a FldNumericBin5
    // that's something like BINARY-LONG.  We just have to convert 'value' to 
    // a string.  And we simply have to believe that the caller gave us more
    // than enough room to work in:
    size_t i = 0 ;
    result[i++] = value%10 + '0';
    value /= 10;
    while( value )
      {
      result[i++] = value%10 + '0';
      value /= 10;
      }
    result[i++] = '\0';
    i = 0;
    size_t j = strlen(result)-1;
    while( i < j )
      {
      std::swap(result[i++], result[j--]);
      }
    }
  return value != 0;
  }

__int128
cobol_language::cobol_language::power_of_ten(int n)
  {
  // 2** 64 = 1.8E19
  // 2**128 = 3.4E38
  __int128 retval = 1;
  static const int MAX_POWER = 19 ;
  __int128 pos[MAX_POWER+1] =
    {
    1ULL,                       // 00
    10ULL,                      // 01
    100ULL,                     // 02
    1000ULL,                    // 03
    10000ULL,                   // 04
    100000ULL,                  // 05
    1000000ULL,                 // 06
    10000000ULL,                // 07
    100000000ULL,               // 08
    1000000000ULL,              // 09
    10000000000ULL,             // 10
    100000000000ULL,            // 11
    1000000000000ULL,           // 12
    10000000000000ULL,          // 13
    100000000000000ULL,         // 14
    1000000000000000ULL,        // 15
    10000000000000000ULL,       // 16
    100000000000000000ULL,      // 17
    1000000000000000000ULL,     // 18
    10000000000000000000ULL,    // 19
    };
  if( n < 0 || n>MAX_POWER*2)     // The most we can handle is 10**38
    {
    fprintf(stderr, "Trying to raise 10 to %d as an int128, which we can't do.\n", n);
    fprintf(stderr, "The problem is in %s.\n", __func__);
    exit(1);
    }
  if( n <= MAX_POWER )
    {
    // Up to 10**18 we do directly:
    retval = pos[n];
    }
  else
    {
    // 19 through 38:
    retval = pos[n/2];
    retval *= retval;
    if( n & 1 )
      {
      retval *= 10;
      }
    }
  return retval;
  }

static
__int128
little_endian_to_binary_signed(
  const unsigned char *psource,
  int capacity
)
  {
  // This subroutine takes a little-endian value of "capacity" bytes and
  // converts it to a signed INT128.  The highest order bit of the little-endian
  // value determines whether or not the highest-order bits of the INT128
  // return value are off or on.

  __int128 result;

  // Set all the bits of the result based on the sign of the source:
  if( psource[capacity-1] >= 128 )
    {
    result = -1;
    }
  else
    {
    result = 0;
    }

  // Copy the low-order bytes into place:
  memcpy(&result, psource, capacity);
  return result;
  }

static
__int128
little_endian_to_binary_unsigned(
  const unsigned char *psource,
  int capacity
)
  {
  __int128 result = 0;

  // Copy the low-order bytes into place:
  memcpy(&result, psource, capacity);
  return result;
  }

static
__int128
big_endian_to_binary_unsigned(
  const unsigned char *psource,
  int   capacity
)
  {
  // This subroutine takes an unsigned big-endian value of "capacity" bytes and
  // converts it to an INT128.

  __int128 retval = 0 ;

  // move the bytes of psource into retval, flipping them end-to-end
  unsigned char *dest = (unsigned char *)&retval;
  while(capacity > 0)
    {
    *dest++ = psource[--capacity];
    }
  return retval;
  }

static
__int128
big_endian_to_binary_signed(
  const unsigned char *psource,
  int   capacity
)
  {
  // This subroutine takes a big-endian value of "capacity" bytes and
  // converts it to a signed INT128.  The highest order bit of the big-endian
  // value determines whether or not the highest-order bits of the INT128
  // return value are off or on.

  __int128 retval;
  if( *psource >= 128 )
    {
    retval = -1;
    }
  else
    {
    retval = 0;
    }

  // move the bytes of psource into retval, flipping them end-to-end
  unsigned char *dest = (unsigned char *)&retval;
  while(capacity > 0)
    {
    *dest++ = psource[--capacity];
    }
  return retval;
  }


static __int128
edited_to_binary (
  const char *ps,
  int length,
  int *rdigits
)
  {
  // This routine is used for converting NumericEdited strings to
  // binary.

  // Numeric edited strings can have all kinds of crap in them: spaces,
  // slashes, dollar signs...you name it.  It might have a minus sign at
  // the beginning or end, or it might have CR or DB at the end.

  // We are going to look for '-' 'D' or 'd' and use that to flag the result
  // as negative.  We are going to look for a decimal point and count up
  // the numerical digits to the right of it.  And we are going to pretend
  // that nothing else matters.

  int hyphen = 0;
  *rdigits = 0;

  // index into the ps string
  int index = 0;

  // Create a delta_r for counting digits to the right of
  // any decimal point.  If and when we encounter a decimal point,
  // we'll set this to one, otherwise it'll stay zero.
  int delta_r = 0;

  __int128 result = 0;

  char ch;

  // We need to check the last two characters.  If CR or DB, then the result
  // is negative:
  if( length >= 2)
    {
    if( (ps[length-2] == 'D' || ps[length-2] == 'd') && (ps[length-1] == 'B' || ps[length-1] == 'b') )
      {
      hyphen = 1;
      }
    else if( (ps[length-2] == 'C' || ps[length-2] == 'c') &&  (ps[length-1] == 'R' || ps[length-1] == 'r') )
      {
      hyphen = 1;
      }
    }

  while( index < length )
    {
    ch = ps[index++];
    switch(ch)
      {
      case '*' :
      case 'Z' :
      case 'z' :
      case '+' :
        continue;
        break;

      case '-' :
        hyphen = 1;
        continue;
        break;

      case '0' :
      case '1' :
      case '2' :
      case '3' :
      case '4' :
      case '5' :
      case '6' :
      case '7' :
      case '8' :
      case '9' :
        result *= 10;
        result += ch - '0' ;
        *rdigits += delta_r ;
        continue;
        break;
      }

    if( ch == decimal_point )
      {
      delta_r = 1;
      }
    }

  if( result == 0 )
    {
    hyphen = 0;
    }
  else if( hyphen )
    {
    result = -result;
    }
  return result;
  }

__int128
cobol_language::dirty_to_binary(const char *dirty,
                                int length,
                                int *rdigits)
  {
  // This routine is used for converting uncontrolled strings to a
  // a 128-bit signed binary number.

  // The string can start with a plus or minus
  // It can contain a single embedded dot
  // The rest of the characters have to be [0-9]
  // Any other character, including a second dot, ends processing.

  // So, a "1ABC" will yield 1; "ABC" will yield 0.

  // It also can handle 12345E-2 notation.

  // It returns the binary result. So, 1031.2 returns 10312 and rdigits=1

  // The binary number, if signed, is returned as a negative number.

  // We are limiting the number of digits in the number to MAX_FIXED_POINT_DIGITS

  __int128 retval = 0;

  int digit_count = 0;
  int hyphen = 0;
  *rdigits = 0;

  // Create a delta_r for counting digits to the right of
  // any decimal point.  If and when we encounter a decimal separator,
  // we'll set this to one, otherwise it'll stay zero.
  int delta_r = 0;

  // We now loop over the remaining input characters:
  unsigned char ch = '\0';

  if(length-- > 0)
    {
    ch = *dirty++;
    if( ch == internal_minus )
      {
      hyphen = 1;
      }
    else if( ch == internal_plus )
      {
      // A plus sign is okay
      }
    else if( ch == decimal_point )
      {
      delta_r = 1;
      }
    else if( ch >= internal_0 && ch <= internal_9 )
      {
      retval = ch - internal_0 ;
      if( retval )
        {
        digit_count += 1;
        }
      }
    else
      {
      // Because didn't start with minus, plus, a decimal_place or a digit,
      // this isn't a number
      length = 0;
      ch = '\0';
      }
    }

  while( length-- > 0 )
    {
    ch = *dirty++;
    if( ch == decimal_point && delta_r == 0 )
      {
      // This is the first decimal point we've seen, so we
      // can start counting rdigits:
      delta_r = 1;
      continue;
      }
    if( ch < internal_0 || ch > internal_9 )
      {
      // When we hit something that isn't a digit, then we are done
      break;
      }
    if( digit_count < MAX_FIXED_POINT_DIGITS )
      {
      retval *= 10;
      retval += ch - internal_0 ;
      *rdigits += delta_r;
      if( retval )
        {
        digit_count += 1;
        }
      }
    }

  // Let's check for an exponent:
  if( ch == internal_E || ch == internal_e )
    {
    int exponent = 0;
    int exponent_sign = 1;
    if( length > 0  )
      {
      ch = *dirty;
      if( ch == internal_plus)
        {
        length -= 1;
        dirty += 1;
        }
      else if (ch == internal_minus)
        {
        exponent_sign = -1;
        length -= 1;
        dirty += 1;
        }
      }
    while(length-- > 0)
      {
      ch = *dirty++;
      if( ch < internal_0 || ch > internal_9 )
        {
        // When we hit something that isn't a digit, then we are done
        break;
        }
      exponent *= 10;
      exponent += ch - internal_0 ;
      }
    exponent *= exponent_sign;
    // We need to adjust the retval and the rdigits based on the exponent.
    if( exponent < 0)
      {
      *rdigits += -exponent;
      }
    else if(exponent > 0)
      {
      if( exponent <= *rdigits )
        {
        *rdigits -= exponent;
        }
      else
        {
        // Exponent is > rdigits
        retval *= cobol_language::power_of_ten(exponent - *rdigits);
        *rdigits = 0;
        }
      }
    }

  if( !retval )
    {
    // Because the result is zero, there can't be a minus sign
    hyphen = 0;
    }
  if( hyphen )
    {
    // We saw a minus sign, so negate the result
    retval = -retval;
    }
  return retval;
  }

static
__int128
get_binary_value_internal(  int               *rdigits,
                            cobol_value_field *field,
                            unsigned char     *resolved_location,
                            size_t             resolved_length)
  {
  __int128 retval;

  int attr = field->attr();

  char ch;
  switch( field->type() )
    {
    case FldGroup :
    case FldAlphanumeric :
      // Read the data area as a dirty string:
      retval = cobol_language::dirty_to_binary( (const char *)resolved_location,
                                      resolved_length,
                                      rdigits );
      break;

    case FldNumericDisplay :
      if( resolved_location[resolved_length-1] == 0xFF )
        {
        // We are dealing with HIGH-VALUE.  Don't screw around

        // Turn all the bits on
        memset( &retval, 0xFF, sizeof(retval) );

        // Make it positive
        ((unsigned char *)&retval)[sizeof(retval)-1] = 0x3F;
        *rdigits = field->rdigits();
        }
      else
        {
        // Pick up the sign byte, and force our value to be positive
        unsigned char *sign_byte_location;
        if( (attr) & separate_e && (attr & leading_e) )
          {
          sign_byte_location = resolved_location;
          ch = *sign_byte_location;
          *sign_byte_location = '+';
          }
        else if( (attr & separate_e) && !(attr & leading_e) )
          {
          sign_byte_location = resolved_location + resolved_length - 1;
          ch = *sign_byte_location;
          *sign_byte_location = '+';
          }
        else if( (attr & leading_e) )
          {
          sign_byte_location = resolved_location;
          ch = *sign_byte_location;
          *sign_byte_location &= ~NUMERIC_DISPLAY_SIGN_BIT;
          }
        else // if( !(attr & leading_e) )
          {
          sign_byte_location = resolved_location + resolved_length - 1;
          ch = *sign_byte_location;
          *sign_byte_location &= ~NUMERIC_DISPLAY_SIGN_BIT;
          }

        // We know where the decimal point is because of rdigits.  Because
        // we know that it a clean string of ASCII digits, we can use the
        // dirty converter:
        retval = cobol_language::dirty_to_binary( (const char *)resolved_location,
                                        resolved_length,
                                        rdigits );
        *rdigits = field->rdigits();

        // Restore the sign byte
        *sign_byte_location = ch;

        if( ch == '-' || (ch & NUMERIC_DISPLAY_SIGN_BIT) )
          {
          retval = -retval;
          }
        }
      break;

    case FldNumericEdited :
      retval = edited_to_binary(  (const char *)resolved_location,
                                  resolved_length,
                                  rdigits);
      break;

    case FldNumericBinary :
      if( attr & signable_e)
        {
        retval = big_endian_to_binary_signed( (const unsigned char *)resolved_location,
                                              resolved_length);
        }
      else
        {
        retval = big_endian_to_binary_unsigned(   (const unsigned char *)resolved_location,
                 resolved_length);
        }
      *rdigits = field->rdigits();
      break;

    case FldNumericBin5:
    case FldIndex:
      if( attr & signable_e)
        {
        retval = little_endian_to_binary_signed(  (const unsigned char *)resolved_location,
                 resolved_length);
        }
      else
        {
        retval = little_endian_to_binary_unsigned(  (const unsigned char *)resolved_location,
                 resolved_length);
        }
      *rdigits = field->rdigits();
      break;

    case FldLiteralN:
      if( (attr & FIGCONST_MASK) == zero_value_e )
        {
        retval = 0;
        *rdigits = 0;
        }
      else
        {
        retval = little_endian_to_binary_unsigned(  (const unsigned char *)field->data(),
                 field->capacity());
        *rdigits = field->rdigits();
        }
      break;

    case FldPointer:
      retval = little_endian_to_binary_unsigned((const unsigned char *)resolved_location,
                                                resolved_length);
      *rdigits = 0;
      break;

    case FldPacked:
      {
      bool negative = false;
      retval = 0;
      for(int i=0; i<2*field->capacity(); i++)
        {
        int nybble;
        if( !(i & 1) )
          {
          nybble = field->data()[i/2] >> 4;
          }
        else
          {
          nybble = field->data()[i/2] & 0x0f ;
          }
        if(    nybble == 0xb
            || nybble == 0xd )
          {
          negative = true;
          }
        if( nybble >= 0xa )
          {
          continue;
          }
        retval *= 10;
        retval += nybble;
        }
      if( negative )
        {
        retval = -retval;
        }
      *rdigits = field->rdigits();
      break;
      }

    default:
      error( "%s() Unknown conversion for field type %s\n", 
              __func__, 
              cobol_language::field_type_as_string(field->type()));
      break;
    }

  if( attr & scaled_e )
    {
    // Here's where we handle a P-scaled number.

    if( field->rdigits() >= 0)
      {
      // We might be dealing with a source with a PICTURE string of
      // PPPPPP999, which means retval is a three-digit number
      // and resolved_field.rdigits() is +6.  That means we need to divide retval by
      // 10**9, and we need to make rdigits 9
      *rdigits = field->digits() + field->rdigits();
      }
    else
      {
      // We have a source with a PIC string like 999PPPPPP, which is
      // a capacity of 3 and a resolved_field.rdigits() of -6.  We need to multiply
      // retval by +6, and make rdigits zero:
      retval *= cobol_language::power_of_ten( -field->rdigits() );
      *rdigits = 0;
      }
    }

  return retval;
  }

static int
var_is_refmod( cobol_value_field *field )
  {
  return (field->attr() & refmod_e) != 0;
  }

static void
remove_trailing_zeroes(char *psz)
  {
  char *p = psz + strlen(psz) -1 ;
  while( p > psz && *p == '0' )
    {
    *p-- = '\0';
    }
  }

static char *
format_for_display_internal(cobol_value_field *field,
                            unsigned char *actual_location,
                            int   actual_length,
                            bool pointer_override = false)
  {
  // This routine creates a buffer with malloc() and fills it with the
  // formatted display for the variable.  It's up to the user to free
  // that buffer when done with it.  The generated string is null-terminated

  char *retval;

  int source_rdigits = field->rdigits();
  cbl_field_type_t field_type = field->type();
  int attr = field->attr();

  if( field->attr() & scaled_e )
    {
    source_rdigits = 0;
    }

  if( field_type == FldPointer && pointer_override )
    {
    field_type = FldNumericBin5;
    }

  switch( field_type )
    {
    case FldGroup:
    case FldAlphanumeric:
    case FldNumericEdited:
    case FldAlphaEdited:
      retval = (char *)malloc(actual_length+1);
      if( actual_location )
        {
        memcpy(retval, actual_location, actual_length);
        }
      else
        {
        memset(retval, '?', actual_length);
        }
      retval[actual_length] = '\0';
      break;

    case FldLiteralN:
      retval = strdup(field->initial());
      break;

    case FldNumericDisplay:
      {
      unsigned char *running_location = actual_location;
      int digit_length = actual_length;

      int nsize = actual_length+1;  // Extra 1 for terminating NULL
      int index = 0;
      if( source_rdigits )
        {
        // We need room for the inside decimal point
        nsize += 1;
        }
      if( (attr & signable_e) && !(attr & separate_e) )
        {
        // We need room for a leading sign
        nsize += 1;
        }
      retval = (char *)malloc(nsize);

      if( actual_location)
        {
        if( attr & signable_e )
          {
          if( attr & separate_e )
            {
            // Reduce the digit location count by the +/- character
            digit_length -= 1;
            if( attr & leading_e )
              {
              // The first character is the sign character
              retval[index++] = *running_location++;
              }
            }
          else
            {
            // The sign character is not separate
            size_t sign_location = attr & leading_e ? 0 : actual_length-1 ;
            if( actual_location[sign_location] & NUMERIC_DISPLAY_SIGN_BIT)
              {
              retval[index++] = '-';
              }
            else
              {
              retval[index++] = '+';
              }
            }
          }

        if( var_is_refmod(field) )
          {
          memcpy(retval+index, actual_location, actual_length);
          index += actual_length;
          }
        else
          {
          // copy over the characters to the left of the decimal point:
          for(int i=0; i<digit_length - source_rdigits; i++ )
            {
            char ch = *running_location++;
            if( retval[index-1] != (char)0xFF )
              {
              ch &= ~NUMERIC_DISPLAY_SIGN_BIT;
              }
            retval[index++] = ch;
            }
          if( source_rdigits )
            {
            // Lay down a decimal point
            retval[index++] = decimal_point;

            // And the digits to the right
            for(int i=0; i<source_rdigits; i++ )
              {
              char ch = *running_location++;
              if( retval[index-1] != (char)0xFF )
                {
                ch &= ~NUMERIC_DISPLAY_SIGN_BIT;
                }
              retval[index++] = ch;
              }
            }
          }
        if( retval[index-1] != (char)0xFF )
          {
          // If we aren't dealing with HIGH-VALUES, make sure that
          // any sign bit isn't in the string.  Whether leading, trailing,
          // or separate, these operations are harmless
          retval[0]       &= 0x3F;
          retval[index-1] &= 0x3F;
          }
        if( attr & signable_e && attr & separate_e && !(attr & leading_e) )
          {
          retval[index++] = actual_location[actual_length-1];
          }

        retval[index++] ='\0';
        }
      else
        {
        memset(retval, '?', nsize-1);
        retval[nsize] = '\0';
        }
      }
    break;

    case FldNumericBinary:
    case FldNumericBin5:
    case FldIndex:
    case FldPacked:
      {
      int digits;
      __int128 value = get_binary_value_internal( &digits,
                       field,
                       actual_location,
                       actual_length);
      // We have to unscale any p-scaled values with negative rdigits:
      if( (field->attr() & scaled_e) && field->rdigits() < 0 )
        {
        value /= cobol_language::power_of_ten(-field->rdigits());
        }

      char ach[128];
      cobol_language::binary_to_string(ach, field->digits(), value);

      // We are dealing with field->digits() in the case of a PICTURE string, or
      // else just the length of ach for something like USAGE BINARY-LONG
      int source_digits = field->digits() ? field->digits() : strlen(ach);

      int nsize = source_digits+1;
      int index = 0;
      if( source_rdigits )
        {
        // We need room for the inside decimal point
        nsize += 1;
        }
      if( attr & signable_e )
        {
        // We need room for the leading sign
        nsize += 1;
        }
      retval = (char *)malloc(nsize);

      bool is_signed = value < 0;

      if( attr & signable_e )
        {
        if( is_signed )
          {
          retval[index++] = '-';
          }
        else
          {
          retval[index++] = '+';
          }
        }
      // copy over the characters to the left of the decimal point:
      memcpy(retval+index, ach, source_digits - source_rdigits);
      index += source_digits - source_rdigits;
      if( source_rdigits )
        {
        retval[index++] = decimal_point;
        memcpy(retval+index, ach+(source_digits-source_rdigits), source_rdigits);
        index += source_rdigits;
        }
      retval[index++] ='\0';
      }
    break;

    case FldClass:
      {
      if( field->level() != 88 )
        {
        size_t retsize = 16;
        retval = (char *)malloc(retsize);
        memset(retval, 0, retsize);
        strcpy(retval, "<CLASS>");
        }
      else
        {
        // This is a LEVEL 88 variable
        size_t retsize = 16;
        retval = (char *)malloc(retsize);
        memset(retval, 0, retsize);
        strcpy(retval, "<LEVEL88>");
        }

      break;
      }

    case FldPointer:
      {
      int digits;
      __int128 value = get_binary_value_internal( &digits,
                       field,
                       actual_location,
                       actual_length);
      char ach[128];
      sprintf(ach, "%p", (void *)value);
      retval = strdup(ach);
      break;
      }

    case FldFloat:
      {
      switch(field->capacity())
        {
        case 4:
          {
          // We will convert based on the fact that for float32, any seven-digit
          // number converts to float32 and then back again unchanged.

          // We will also format numbers so that we produce 0.01 and 1E-3 on the low
          // side, and 9999999 and then 1E+7 on the high side
          // 10,000,000 = 1E7
          char ach[64];
          _Float32 floatval = *(_Float32 *)actual_location;
          strfromf32(ach, sizeof(ach), "%.9E", floatval);
          char *p = strchr(ach, 'E');
          if( !p )
            {
            // Probably INF -INF NAN or -NAN, so ach has our result
            }
          else
            {
            p += 1;
            int exp = atoi(p);
            if( exp >= 6 || exp <= -5 )
              {
              // We are going to stick with the E notation, so ach has our result
              }
            else
              {
              // We are going to produce our number in such a way that we specify
              // seven signicant digits, no matter where the decimal point lands.
              // Note that exp is in the range of 6 to -2

              int precision = 9 - exp;
              sprintf(ach, "%.*f", precision, (double)floatval );
              }
            remove_trailing_zeroes(ach);
            }
          retval = strdup(ach);
          break;
          }

       case 8:
          {
          // We will convert based on the fact that for float32, any 15-digit
          // number converts to float64 and then back again unchanged.

          // We will also format numbers so that we produce 0.01 and 1E-3 on the low
          // side, and 9999999 and then 1E+15 on the high side
          char ach[64];
          _Float64 floatval = *(_Float64 *)actual_location;
          strfromf64(ach, sizeof(ach), "%.17E", floatval);
          char *p = strchr(ach, 'E');
          if( !p )
            {
            // Probably INF -INF NAN or -NAN, so ach has our result
            }
          else
            {
            p += 1;
            int exp = atoi(p);
            if( exp >= 6 || exp <= -5 )
              {
              // We are going to stick with the E notation, so ach has our result
              }
            else
              {
              // We are going to produce our number in such a way that we specify
              // seven signicant digits, no matter where the decimal point lands.
              // Note that exp is in the range of 6 to -2

              int precision = 17 - exp;

              sprintf(ach, "%.*f", precision, (double)floatval );
              }
            remove_trailing_zeroes(ach);
            }
          retval = strdup(ach);
          break;
          }

       case 16:
          {
          // We will convert based on the fact that for float32, any 15-digit
          // number converts to float64 and then back again unchanged.

          // We will also format numbers so that we produce 0.01 and 1E-3 on the low
          // side, and 9999999 and then 1E+15 on the high side
          char ach[128];
          _Float128 floatval = *(_Float128 *)actual_location;
          strfromf128(ach, sizeof(ach), "%.36E", floatval);
          char *p = strchr(ach, 'E');
          if( !p )
            {
            // Probably INF -INF NAN or -NAN, so ach has our result
            }
          else
            {
            p += 1;
            int exp = atoi(p);
            if( exp >= 6 || exp <= -5 )
              {
              // We are going to stick with the E notation, so ach has our result
              }
            else
              {
              // We are going to produce our number in such a way that we specify
              // seven signicant digits, no matter where the decimal point lands.
              // Note that exp is in the range of 6 to -2

              int precision = 36 - exp;
              char achFormat[24];
              sprintf(achFormat, "%%.%df", precision);
              strfromf128(ach, sizeof(ach), achFormat, floatval);
              }
            remove_trailing_zeroes(ach);
            }

          retval = strdup(ach);
          break;
          }
        }
      break;
      }

    default:
      error("Unknown conversion for type %s in format_for_display_internal\n",
            cobol_language::field_type_as_string(field->type()) );
      break;
    }

  if( attr & scaled_e )
    {
    if( field->rdigits() > 0)
      {
      // We have something like 123 or +123.  We need to insert a decimal
      // point and a rdigits zeroes to make it +.000000123
      size_t new_length = strlen(retval) + field->rdigits() + 1 + 1;
      char *new_ret = (char *)malloc(new_length);
      memset(new_ret, '0', new_length);
      char *p = new_ret;
      char *s = retval;
      if( !isdigit(retval[0]) )
        {
        *p++ = retval[0];
        s += 1;
        }
      *p++ = decimal_point;
      p += field->rdigits();  // Skip over the zeroes
      strcpy(p, s);
      free(retval);
      retval = new_ret;
      }
    else if(   field->type() == FldNumericDisplay
            && (field->attr() & separate_e) 
            && !(field->attr() & leading_e))
      {
      // we have something like 123+.  We need to put some zeroes in between
      // the '3' and the '+'
      char sign_ch = retval[strlen(retval)-1];
      retval[strlen(retval)-1] = '\0';

      size_t new_length = strlen(retval) + -field->rdigits() + 2;
      char *new_ret = (char *)malloc(new_length);
      memset(new_ret, '0', new_length);
      memcpy(new_ret, retval, strlen(retval));
      new_ret[new_length-2]   = sign_ch;
      new_ret[new_length-1] = '\0';
      free(retval);
      retval = new_ret;
      }
    else // field.rdigits() < 0
      {
      // We have something like 123 or +123.  All we need to do is
      // add zeroes to the end:
      size_t new_length = strlen(retval) + -field->rdigits() + 1;
      char *new_ret = (char *)malloc(new_length);
      memset(new_ret, '0', new_length);
      new_ret[new_length-1] = '\0';
      memcpy(new_ret, retval, strlen(retval));
      free(retval);
      retval = new_ret;
      }
    }

  return retval;
  }

//////////////////////////////// END OF GCOBOL-based coe

// cf. gcc/cobol/util.cc::cbl_field_type_str

const char *
cobol_language::field_type_as_string(cbl_field_type_t type)
  {
  const char *retval = "???";
  switch(type)
    {
    case FldInvalid         :
      retval = "FldInvalid";
      break;
    case FldGroup           :
      retval = "FldGroup";
      break;
    case FldAlphanumeric    :
      retval = "FldAlphanumeric";
      break;
    case FldNumericBinary   :
      retval = "FldNumericBinary";
      break;
    case FldNumericBin5     :
      retval = "FldNumericBin5";
      break;
    case FldPacked          :
      retval = "FldPacked";
      break;
    case FldNumericDisplay  :
      retval = "FldNumericDisplay";
      break;
    case FldNumericEdited   :
      retval = "FldNumericEdited";
      break;
    case FldAlphaEdited     :
      retval = "FldAlphaEdited";
      break;
    case FldLiteralA         :
      retval = "FldLiteralA";
      break;
    case FldLiteralN         :
      retval = "FldLiteralN";
      break;
    case FldClass           :
      retval = "FldClass";
      break;
    case FldConditional     :
      retval = "FldConditional";
      break;
    case FldForward         :
      retval = "FldForward";
      break;
    case FldIndex           :
      retval = "FldIndex";
      break;
    case FldSwitch          :
      retval = "FldSwitch";
      break;
    case FldDisplay         :
      retval = "FldDisplay";
      break;
    case FldPointer         :
      retval = "FldPointer";
      break;
    case FldFloat           :
      retval = "FldFloat";
      break;
    }
  return retval;
  }

static void
tack_onto_string(std::string &s, const char *piece)
  {
  if(!s.empty())
    {
    s += ", ";
    }
  s += piece;
  }

std::string
cobol_field_attr_as_string(cbl_field_attr_t attr)
  {
  std::string retval;

  switch( attr & FIGCONST_MASK )
    {
    case low_value_e    :
      tack_onto_string(retval, "LOW-VALUE");
      break;
    case zero_value_e   :
      tack_onto_string(retval, "ZERO");
      break;
    case space_value_e  :
      tack_onto_string(retval, "SPACE");
      break;
    case quote_value_e  :
      tack_onto_string(retval, "QUOTE");
      break;
    case high_value_e   :
      tack_onto_string(retval, "HIGH-VALUE");
      break;
    }

  if( attr & initialized_e)
    {
    tack_onto_string(retval, "initialized");
    }
  if( attr & rjust_e)
    {
    tack_onto_string(retval, "rjust");
    }
  if( attr & ljust_e)
    {
    tack_onto_string(retval, "ljust");
    }
  if( attr & zeros_e)
    {
    tack_onto_string(retval, "zero_fill");
    }
  if( attr & signable_e)
    {
    tack_onto_string(retval, "signable");
    }
  if( attr & constant_e)
    {
    tack_onto_string(retval, "constant");
    }
  if( attr & function_e)
    {
    tack_onto_string(retval, "function");
    }
  if( attr & quoted_e)
    {
    tack_onto_string(retval, "quoted");
    }
  if( attr & filler_e)
    {
    tack_onto_string(retval, "filler");
    }
  if( attr & intermediate_e)
    {
    tack_onto_string(retval, "intermediate");
    }
  if( attr & scaled_e)
    {
    tack_onto_string(retval, "scaled");
    }
  if( attr & refmod_e)
    {
    tack_onto_string(retval, "refmod");
    }
  if( attr & global_e)
    {
    tack_onto_string(retval, "global_scope");
    }
  if( attr & external_e)
    {
    tack_onto_string(retval, "external");
    }
  if( attr & blank_zero_e)
    {
    tack_onto_string(retval, "blank_when_zero");
    }
  if( attr & linkage_e)
    {
    tack_onto_string(retval, "linkage");
    }
  if( attr & local_e)
    {
    tack_onto_string(retval, "local");
    }
  if( attr & leading_e)
    {
    tack_onto_string(retval, "sign_leading");
    }
  if( attr & separate_e)
    {
    tack_onto_string(retval, "sign_separate");
    }
  if( attr & embiggened_e)
    {
    tack_onto_string(retval, "expended_to_64_bit");
    }
  if( attr & all_alpha_e)
    {
    tack_onto_string(retval, "all_alpha");
    }
  if( attr & all_x_e)
    {
    tack_onto_string(retval, "all_X");
    }
  if( attr & prog_ptr_e)
    {
    tack_onto_string(retval, "program-pointer");
    }
  if( attr & based_e)
    {
    tack_onto_string(retval, "based");
    }
  if( attr & any_length_e)
    {
    tack_onto_string(retval, "any_length");
    }
  if( attr & envar_e)
    {
    tack_onto_string(retval, "environment_var");
    }
  if( attr & bool_encoded_e)
    {
    tack_onto_string(retval, "boolean");
    }
  if( attr & hex_encoded_e)
    {
    tack_onto_string(retval, "hex_encoded");
    }
  if( attr & depends_on_e)
    {
    tack_onto_string(retval, "contains_depending_on");
    }
  if( attr & has_value_e)
    {
    tack_onto_string(retval, "has_value");
    }
  if( attr & ieeedec_e)
    {
    tack_onto_string(retval, "IEEE754(Decimal)");
    }
  if( attr & big_endian_e)
    {
    tack_onto_string(retval, "big_endian");
    }
  if( attr & same_as_e)
    {
    tack_onto_string(retval, "same_as");
    }
  if( attr & record_key_e)
    {
    tack_onto_string(retval, "record_key");
    }
  if( attr & typedef_e)
    {
    tack_onto_string(retval, "typedef");
    }

  return retval;
  }

void
cobol_language::print_val_as_list(struct value *val,
                                  struct ui_file *stream,
                                  const struct value_print_options *options)
const
  {
  // This is the implemention of print/r "raw".

  // incoming 'val' is a cblc_field_t structure
  cobol_value_field variable(val);

  // We need a changeable copy of the const options
  struct value_print_options local_options = *options;

  struct type *type = val->type();
  struct gdbarch *gdbarch = type->arch ();
  gdb_puts("{\n");

  // Print out our location and our ELF symbol
  fputs_styled("variable: ", variable_name_style.style (), stream);
  CORE_ADDR val_addr = val->address();
  fputs_styled (paddress (gdbarch, val_addr), address_style.style (), stream);
  print_address_symbolic (gdbarch, val_addr, stream, true, " ");
  fprintf_styled( stream,
                  variable_name_style.style(), " %2.2d %s",
                  variable.level(),
                  variable.name() );

  if(    variable.refer.subscripts[0] > -1
      || variable.refer.refmod_from 
      ||  variable.refer.refmod_len )
    {
     gdb_puts(" ", stream);
    }

  // Tack on any subscripts
  if( variable.refer.subscripts[0] > -1 )
    {
    // We've got us some subscripts:
    fprintf_styled(stream, variable_name_style.style(), "(");
    for(int i=0; i<MAXIMUM_TABLE_DIMENSIONS; i++)
      {
      if( variable.refer.subscripts[i] < 1 )
        {
        break;
        }
      if( i )
        {
        fprintf_styled(stream, variable_name_style.style(), " ");
        }
      fprintf_styled( stream,
                      variable_name_style.style(),
                      "%d",
                      variable.refer.subscripts[i] );
      }
    fprintf_styled(stream, variable_name_style.style(), ")");
    }

  // Tack on any refmod:
  if( variable.refer.refmod_from ||  variable.refer.refmod_len )
    {
    fprintf_styled(stream, variable_name_style.style(), "(");
    if( variable.refer.refmod_from )
      {
      fprintf_styled( stream, variable_name_style.style(),
                      "%d",
                      variable.refer.refmod_from);
      }
    fprintf_styled(stream, variable_name_style.style(), ":");
    if( variable.refer.refmod_len )
      {
      fprintf_styled( stream, variable_name_style.style(),
                      "%d",
                      variable.refer.refmod_len);
      }
    fprintf_styled(stream, variable_name_style.style(), ")");
    }


  gdb_puts(",\n", stream);

  for(int i=0; i < type->num_fields(); ++i)
    {
    const char *field_name = type->field(i).name();
    if( strcmp(field_name, "dummy") == 0 )
      {
      // We use a "dummy" element to fill out the cblc_field_t to a multiple of
      // 8 bytes.  Ignore it here.
      continue;
      }

    fputs_styled(field_name, variable_name_style.style(), stream);
    gdb_puts(": ", stream);

    int save_format = options->format;

    if( local_options.format == 'p' )
      {
      local_options.format = 0;
      }

    if( strcmp(field_name, "attr") == 0 )
      {
      local_options.format = 'x';
      // We want to print out the version with the masked off the initialized_e
      // bit.  It's not relevant at run time.
      gdb_printf(stream, "0x%lx", variable.attr());
      }
    else if( strcmp(field_name, "data") == 0 )
      {
      // There are a couple of things about "data" that are slightly different
      // from what we get from common_val_print, so we handle it explicitly.
      
      // We are already at "data: ", so we want to print out the variable.data()
      // value, which is the COBOL variables cblc_field_t::data element modified
      // by possible array subscripts and refmods
      fprintf_styled(stream, address_style.style(), "0x%lx", variable.core_addr());
      // Use that same value for the symbolic output:
      print_address_symbolic( gdbarch,
                              (CORE_ADDR)variable.core_addr(),
                              stream,
                              true,
                              " ");
      // We will actually display the data down below.
      }
    else
      {
      common_val_print( value_field(val, i),
                        stream,
                        0,
                        &local_options,
                        this);
      }
    local_options.format = save_format;

    if( strcmp(field_name, "data") == 0 )
      {
      unsigned int print_max_chars = get_print_max_chars(&local_options);
      // print_max_chars is the maximum length, in character positions, the 
      // user is willing to put up with.  Divide by three because we are doing
      // repeated " %2.2x"
      size_t jmax = print_max_chars/3;
      for(size_t j=0; j<variable.capacity(); j++)
        {
        if( j >= jmax )
          {
          gdb_printf(stream, "...");        
          break;
          }
        gdb_printf(stream, " %2.2x", variable.data()[j]);        
        }

      gdb_puts(",\n", stream);
      fputs_styled("display", variable_name_style.style(), stream);
      gdb_puts(": ", stream);
      variable.value_print(stream, &local_options);
      }

    if( strcmp(field_name, "type") == 0 )
      {
      gdb_puts(" (", stream);
      cbl_field_type_t fld_type = (cbl_field_type_t)value_as_long(value_field(val, i));
      gdb_puts(field_type_as_string(fld_type), stream);
      gdb_puts(")", stream);
      }

    if( (strcmp(field_name, "attr") == 0) )
      {
      // cbl_field_attr_t fld_attr = 
        // (cbl_field_attr_t)(value_as_long(value_field(val, i)) & ~initialized_e);
      cbl_field_attr_t fld_attr = variable.attr();
      if( fld_attr )
        {
        gdb_printf(stream, " (%s)", cobol_field_attr_as_string(fld_attr).c_str());
        }
      }

    if( (strcmp(field_name, "picture") == 0) )
      {
      CORE_ADDR addr = (CORE_ADDR)value_as_long(value_field(val, i));
      if( addr )
        {
        gdb_printf(stream, " \"%s\"", variable.picture());
        }
      }

    if( (strcmp(field_name, "initial") == 0) )
      {
      CORE_ADDR addr = (CORE_ADDR)value_as_long(value_field(val, i));
      if( addr )
        {
        // struct value *capacity = value_struct_elt(&val, {}, "capacity", NULL, "cobol_field_t");
        // long len = value_as_long(capacity);

        struct value *base = value_struct_elt(&val, {}, "initial", NULL, "cobol_field_t");
        char *str = variable.initial();
        const char *encoding = ENCODING;
        int force_ellipses = 0;
        struct type *type2 = base->type()->target_type();
        gdb_puts(" ", stream);
        generic_printstr( stream, 
                          type2, 
                          (unsigned char *)str, 
                          variable.original_capacity(), 
                          encoding, 
                          force_ellipses,
                          '"', 0, options);
        }
      }

    if( (strcmp(field_name, "name") == 0) )
      {
      struct value *base = value_struct_elt(&val, {}, "name", NULL, "cobol_field_t");
      char *str = variable.name();
      if( str )
        {
        size_t len = strlen(str);
        const char *encoding = ENCODING;
        int force_ellipses = 0;
        struct type *type2 = base->type()->target_type();
        gdb_puts(" ", stream);
        generic_printstr( stream, type2, (unsigned char *)str, len, encoding, force_ellipses,
                          '"', 0, options);
        }
      }

    if( (strcmp(field_name, "parent") == 0) )
      {
      cblc_field_t *parent = variable.parent();
      if( parent )
        {
        char *name = cobol_fetch_string_from((CORE_ADDR)parent->name);
        fprintf_styled( stream,
                        variable_name_style.style(),
                        " %2.2d %s",
                        parent->level,
                        name);
        free(name);
        }
      }

    if( i < type->num_fields()-1)
      {
      gdb_puts(",", stream);
      }
    gdb_puts("\n", stream);
    }
  gdb_puts("}", stream);
  }

void *
cobol_fetch_bytes_from(CORE_ADDR addr, size_t len)
  {
  char *retval;

  if( addr && len )
    {
    retval = (char *)malloc(len);

    int errcode = target_read_memory(addr, (unsigned char *)retval, len);
    if( errcode )
      {
      error("memory not yet available in this context");
      }
    }
  else
    {
    retval = NULL;
    }

  return retval;
  }

char *
cobol_fetch_string_from(CORE_ADDR addr)
  {
  size_t size = 64;
  size_t bytes_read = 0;

  unsigned char *retval = (unsigned char *)malloc(size);

  for(;;)
    {
    if( bytes_read >= size/2)
      {
      size *= 2;
      retval = (unsigned char *)realloc(retval, size);
      }
    unsigned char *p = retval + bytes_read;
    target_read_memory(addr++, p, 1);
    bytes_read += 1;
    if( *p == '\0' )
      {
      break;
      }
    }
  retval = (unsigned char *)realloc(retval, bytes_read);

  return (char *)retval;
  }

static void
value_as_hex( struct ui_file *stream,
              __int128        value)
  {
  gdb_printf(stream, "0x");
  if( value == 0 )
    {
    gdb_printf(stream, "0");
    return;
    }
  bool started = false;
  unsigned char nybble;
  for(int i=0; i<32; i++)
    {
    nybble = (unsigned char)(value>>(128-4)) & 0xF;
    value <<= 4;
    if( started || nybble )
      {
      gdb_printf(stream, "%x", nybble);
      started = true;
      }
    }
  }

static void
value_as_octal( struct ui_file *stream,
                __int128        value)
  {
  gdb_printf(stream, "0");
  if( value == 0 )
    {
    return;
    }
  bool started = false;
  unsigned char odigit;

  odigit = (unsigned char)(value>>(128-2)) & 0x3;
  value <<= 2;

  if( started || odigit )
    {
    gdb_printf(stream, "%d", odigit);
    started = true;
    }

  for(int i=0; i<42; i++)
    {
    odigit = (unsigned char)(value>>(128-3)) & 0x7;
    value <<= 3;
    if( started || odigit )
      {
      gdb_printf(stream, "%d", odigit);
      started = true;
      }
    }
  }

static void
value_as_binary(struct ui_file *stream,
                __int128        value)
  {
  if( value == 0 )
    {
    gdb_printf(stream, "0");
    return;
    }
  bool started = false;
  unsigned char byte;

  for(int i=0; i<128; i++)
    {
    byte = ((unsigned char *)(&value))[15] & 0x80;
    value <<= 1;

    if( started || byte )
      {
      gdb_printf(stream, "%d", byte ? 1 : 0);
      started = true;
      }
    else if( started )
      {
      gdb_printf(stream, "0");
      }
    }
  }

#define NULLCH '\0'
#define internal_space ' '
#define internal_zero '0'
#define ascii_H 'H'
#define ascii_L 'L'
#define ascii_Q 'Q'
#define ascii_Z 'Z'

static
int
cstrncmp(   char const * const left_,
            char const * const right_,
            size_t count)
  {
  const char *left  = left_;
  const char *right = right_;
  // This is the version of strncmp() that uses the current collation
  
  // It also is designed to handle strings with embedded NUL characters, so
  // it treats NULs like any other characters.
  int retval = 0;
  while( count-- )
    {
    unsigned char chl = *left++;
    unsigned char chr = *right++;
    retval = chl - chr;
    if( retval )
      {
      break;
      }
    }
  return retval;
  }

static int
compare_88( const char    *list,
            const char    *list_e,
            bool           fig_const,
            unsigned char *conditional_location,
            int            conditional_length)
  {
  int list_len = (int)(list_e-list);
  int test_len;
  char *test;
  if( fig_const )
    {
    // We are working with a figurative constant

    test = (char *)malloc(conditional_length);
    test_len = conditional_length;
    // This is where we handle the zero-length strings that
    // nonetheless can magically be expanded into figurative
    // constants:

    int ch = internal_space;
    // Check for the strings starting with 0xFF whose second character
    // indicates a figurative constant:
    if( list[0] == ascii_Z )
      {
      ch = internal_zero;
      }
    else if( list[0] == ascii_H )
      {
      ch = 0xFF;
      }
    else if( list[0] == ascii_Q )
      {
      ch = '"';
      }
    else if( list[0] == ascii_L )
      {
      ch = 0x00;
      }
    memset( test, ch, conditional_length );
    }
  else if( list_len < conditional_length )
    {
    // 'list' is too short; we have to right-fill with spaces:
    test = (char *)malloc(conditional_length);
    test_len = conditional_length;
    memset(test, internal_space, conditional_length);
    memcpy(test, list, list_len);
    }
  else
    {
    test = (char *)malloc(list_len);
    test_len = list_len;
    memcpy(test, list, list_len);
    }

  int cmpval;

  if( test[0] == NULLCH && conditional_location[0] == 0)
    {
    cmpval = 0;
    }
  else
    {
    cmpval = cstrncmp(test, (char *)conditional_location, conditional_length);
    if( cmpval == 0 && (int)strlen(test) != conditional_length )
      {
      // When strncmp returns 0, the actual smaller string is the
      // the shorter of the two:
      cmpval = test_len - conditional_length;
      }
    }

  free(test);

  if( cmpval < 0 )
    {
    cmpval = -1;
    }
  else if(cmpval > 0)
    {
    cmpval = +1;
    }
  return cmpval;
  }

static _Float128
dirty_to_float( const char *dirty,
                      int length)
  {
  // This routine is used for converting uncontrolled strings to a
  // a _Float128

  // The string can start with a plus or minus
  // It can contain a single embedded dot
  // The rest of the characters have to be [0-9]
  // Any other character, including a second dot, ends processing.

  // So, a "1ABC" will yield 1; "ABC" will yield 0.

  // It also can handle 12345E-2 notation.

  _Float128 retval = 0;

  int rdigits = 0;
  int hyphen  = 0;

  // Create a delta_r for counting digits to the right of
  // any decimal point.  If and when we encounter a decimal separator,
  // we'll set this to one, otherwise it'll stay zero.
  int delta_r = 0;

  // We now loop over the remaining input characters:
  char ch = '\0';

  if(length-- > 0)
    {
    ch = *dirty++;
    if( ch == internal_minus )
      {
      hyphen = 1;
      }
    else if( ch == internal_plus )
      {
      // A plus sign is okay
      }
    else if( ch == decimal_point )
      {
      delta_r = 1;
      }
    else if( ch >= internal_0 && ch <= internal_9 )
      {
      retval = ch - internal_0 ;
      }
    else
      {
      // Because didn't start with minus, plus, a decimal_place or a digit,
      // this isn't a number.  Set length to zero to prevent additional
      // processing
      length = 0;
      ch = '\0';
      }
    }

  while( length-- > 0 )
    {
    ch = *dirty++;
    if( ch == decimal_point && delta_r == 0 )
      {
      // This is the first decimal point we've seen, so we
      // can start counting rdigits:
      delta_r = 1;
      continue;
      }
    if( ch < internal_0 || ch > internal_9 )
      {
      // When we hit something that isn't a digit, then we are done
      break;
      }
    retval *= 10;
    retval += ch - internal_0 ;
    rdigits += delta_r;
    }

  // Let's check for an exponent:
  int exponent = 0;
  if( ch == internal_E || ch == internal_e )
    {
    int exponent_sign = 1;
    if( length > 0  )
      {
      ch = *dirty;
      if( ch == internal_plus)
        {
        length -= 1;
        dirty += 1;
        }
      else if (ch == internal_minus)
        {
        exponent_sign = -1;
        length -= 1;
        dirty += 1;
        }
      }
    while(length-- > 0)
      {
      ch = *dirty++;
      if( ch < internal_0 || ch > internal_9 )
        {
        // When we hit something that isn't a digit, then we are done
        break;
        }
      exponent *= 10;
      exponent += ch - internal_0 ;
      }
    exponent *= exponent_sign;
    }

  // We need to adjust the retval based on rdigits and exponent.
  // Notice that 123.45E2 properly comes out to be 12345
  if( exponent - rdigits >= 0 )
    {
    retval *= cobol_language::power_of_ten(exponent - rdigits);
    }
  else
    {
    retval /= cobol_language::power_of_ten(rdigits - exponent);
    }

  if( !retval )
    {
    // Because the result is zero, there can't be a minus sign
    hyphen = 0;
    }
  if( hyphen )
    {
    // We saw a minus sign, so negate the result
    retval = -retval;
    }
  return retval;
  }

static _Float128
get_float128( cobol_value_field *field,
              unsigned char *location )
  {
  _Float128 retval=0;
  if(field->type() == FldFloat )
    {
    switch( field->capacity() )
      {
      case 4:
        retval = *(_Float32 *)location;
        break;
      case 8:
        retval = *(_Float64 *)location;
        break;
      case 16:
        // retval = *(_Float128 *)location; doesn't work, because the SSE
        // registers need the source on a 16-byte boundary, and we can't
        // guarantee that.
        memcpy(&retval, location, 16);
        break;
      }
    }
  else if( field->type() == FldLiteralN )
    {
    if( decimal_point == '.' )
      {
      retval = strtof128(field->initial(), NULL);
      }
    else
      {
      // We need to replace any commas with periods
      static size_t size = 128;
      static char *buffer = (char *)malloc(size);
      while( strlen(field->initial())+1 > size )
        {
        size *= 2;
        buffer = (char *)malloc(size);
        }
      strcpy(buffer, field->initial());
      char *p = strchr(buffer, ',');
      if(p)
        {
        *p = '.';
        }
      retval = strtof128(buffer, NULL);
      }
    }
  else
    {
    fprintf(stderr, "What's all this then?\n");
    abort();
    }
  return retval;
  }

static
int
compare_field_class(cobol_value_field *conditional,
                    unsigned char     *conditional_location,
                    int                conditional_length,
                    char              *list)
  {
  int retval = 1; // Zero means equal
  int rdigits;

  // list points to a superstring: a double-null terminated
  // string containing pairs of strings.  We are looking for equality.

  switch( conditional->type() )
    {
    case FldNumericDisplay:
    case FldNumericEdited:
    case FldNumericBinary:
    case FldPacked:
    case FldNumericBin5:
    case FldIndex:
      {
      __int128 value;
      value = get_binary_value_internal(&rdigits,
                                        conditional,
                                        conditional_location,
                                        conditional_length);
      char *walker = list;
      while(*walker)
        {
        char   left_flag;
        size_t left_len;
        char * left;

        char   right_flag;
        size_t right_len;
        char * right;

        char *pend;
        left_len = strtoull(walker, &pend, 10);
        left_flag = *pend;
        left = pend+1;
        
        right = left + left_len;
        right_len = strtoull(right, &pend, 10);
        right_flag = *pend;
        right = pend+1;

        walker = right + right_len;

        int left_rdigits;
        int right_rdigits;

        __int128 left_value;
        if( left_flag == 'F' && left[0] == 'Z' )
          {
          left_value = 0;
          left_rdigits = 0;
          }
        else
          {
          left_value = cobol_language::dirty_to_binary(
                                  left,
                                  left_len,
                                  &left_rdigits);
          }

        __int128 right_value;
        if( right_flag == 'F' && right[0] == 'Z' )
          {
          right_value = 0;
          right_rdigits = 0;
          }
        else
          {
          right_value = cobol_language::dirty_to_binary(
                                   right,
                                   right_len,
                                   &right_rdigits);
          }

        // Normalize all three numbers to the same rdigits
        int max = std::max(rdigits, left_rdigits);
        max = std::max(max, right_rdigits);

        if( max > rdigits )
          {
          value *= cobol_language::power_of_ten(max - rdigits);
          }
        if( max > left_rdigits )
          {
          left_value *= cobol_language::power_of_ten(max - left_rdigits);
          }
        if( max > right_rdigits )
          {
          right_value *= cobol_language::power_of_ten(max - right_rdigits);
          }
        if( left_value <= value && value <= right_value )
          {
          retval = 0;
          break;
          }
        }
      break;
      }

    case FldGroup:
    case FldAlphanumeric:
    case FldLiteralA:
      {
      char *walker = list;
      while(*walker)
        {
        bool fig1;
        bool fig2;
        char *first;
        char *last;
        char *first_e;
        char *last_e;
        size_t first_len;
        size_t last_len;

        char *pend;

        first = walker;
        first_len = strtoull(first, &pend, 10);
        fig1 = *pend == 'F';
        first = pend+1;
        first_e = first + first_len;
        
        last = first_e;

        last_len = strtoull(last, &pend, 10);
        fig2 = *pend == 'F';
        last = pend+1;
        last_e = last + last_len;

        walker = last_e;

        int compare_result;

        compare_result = compare_88(first,
                                    first_e,
                                    fig1,
                                    conditional_location,
                                    conditional_length);
        if( compare_result > 0 )
          {
          // First is > conditional, so this is no good
          continue;
          }
        compare_result = compare_88(last,
                                    last_e,
                                    fig2,
                                    conditional_location,
                                    conditional_length);
        if( compare_result < 0 )
          {
          // Last is < conditional, so this is no good
          continue;
          }

        // conditional is inclusively between first and last
        retval = 0;
        break;
        }
      break;
      }

    case FldFloat:
      {
      _Float128 value = get_float128(conditional, conditional_location) ;
      char *walker = list;
      while(*walker)
        {
        char   left_flag;
        size_t left_len;
        char * left;

        char   right_flag;
        size_t right_len;
        char * right;

        char *pend;
        left_len = strtoull(walker, &pend, 10);
        left_flag = *pend;
        left = pend+1;
        
        right = left + left_len;
        right_len = strtoull(right, &pend, 10);
        right_flag = *pend;
        right = pend+1;

        walker = right + right_len;

        _Float128 left_value;
        if( left_flag == 'F' && left[0] == 'Z' )
          {
          left_value = 0;
          }
        else
          {
          left_value = dirty_to_float(left,
                                      left_len);
          }

        _Float128 right_value;
        if( right_flag == 'F' && right[0] == 'Z' )
          {
          right_value = 0;
          }
        else
          {
          right_value = dirty_to_float( right,
                                        right_len);
          }

        if( left_value <= value && value <= right_value )
          {
          retval = 0;
          break;
          }
        }
      break;
      }

    default:
      printf( "%s(): doesn't know what to do with %s\n",
              __func__,
              conditional->name());
      abort();
    }

  return retval;
  }

#define APPEND()if(strlen(buffer)+strlen(ach)+1 > bufsize)     \
                    {                                          \
                    bufsize *= 2;                              \
                    buffer = (char *)realloc(buffer, bufsize); \
                    }                                          \
                  strcat(buffer, ach);
void
cobol_value_field::value_print( struct ui_file *stream,
                                const struct value_print_options *options)
  {
  cbl_field_type_t field_type = this->type();
  struct value *base = value_struct_elt(&val, {}, "data", NULL, "cobol_field_t");
  unsigned char *data = this->data();
  size_t len = this->capacity();
  unsigned int print_max_chars = get_print_max_chars(options);
  const char *encoding = ENCODING;
  int force_ellipses = 0;
  struct type *type = base->type()->target_type();

  switch(field_type)
    {
    case FldGroup:
    case FldAlphanumeric:
    case FldAlphaEdited:
    case FldNumericEdited:
      {
      switch( options->format )
        {
        case 'd':
        case 'u':
        case 'o':
        case 'x':
        case 't':
          {
          size_t bufsize = 1024;
          char *buffer = (char *)malloc(bufsize);
          *buffer = '\0';
          char ach[32];
          snprintf(ach, sizeof(ach), "{");
          APPEND();

          size_t element=0;
          while( element < len )
            {
            if( strlen(buffer) > print_max_chars )
              {
              // We've gone past the length the user is willing to tolerate
              break;
              }
            if( options->format == 't' )
              {
              unsigned char byte = this->data()[element];
              *ach = '\0';
              unsigned char mask = 0x80;
              while(mask)
                {
                if( mask & byte )
                  {
                  strcat(ach, "1");
                  }
                else if ( *ach )
                  {
                  strcat(ach, "0");
                  }
                mask >>= 1;
                }
              if( !*ach )
                {
                strcat(ach, "0");
                }
              strcat(ach, ", ");
              }
            else
              {
              switch(options->format)
                {
                case 'd':
                  snprintf(ach, sizeof(ach), "%d, ", this->data()[element]);
                  break;
                case 'u':
                  snprintf(ach, sizeof(ach), "%u, ", this->data()[element]);
                  break;
                case 'o':
                  if(this->data()[element] > 7)
                    {
                    snprintf(ach, sizeof(ach), "0%o, ", this->data()[element]);
                    }
                  else
                    {
                    snprintf(ach, sizeof(ach), "%o, ", this->data()[element]);
                    }
                  break;
                case 'x':
                  snprintf(ach, sizeof(ach), "0x%x, ", this->data()[element]);
                  break;
                }

              }
            APPEND();
            element += 1;
            }
          // We have string ending in ", ".  Trim that off
          buffer[strlen(buffer) - 2] = '\0';
          if( element < len )
            {
            strcpy(ach, "...");
            APPEND();
            }
          snprintf(ach, sizeof(ach), "}");
          APPEND();
          gdb_printf(stream, "%s", buffer);
          free(buffer);
          break;
          }

        default:
          {
          generic_printstr( stream,
                            type,
                            data,
                            len,
                            encoding,
                            force_ellipses,
                            '"', 
                            0,
                            options);
          break;
          }
        }
      break;
      }

    case FldNumericDisplay:
    case FldNumericBinary:
    case FldNumericBin5:
    case FldPacked:
    case FldPointer:
    case FldIndex:
      {
      switch( options->format )
        {
        case 'o':
        case 'x':
        case 't':
          {
          int digits;
          __int128 value = get_binary_value_internal(&digits,
                                                     this,
                                                     this->data(),
                                                     this->capacity() );
          // We have a 128-bit value that the user wants in a bitwise fashion.
          // Let's trim it based on the capacity, mainly to throw away high-
          // order 0xFF bytes for negative value:
          unsigned __int128 mask = -1;
          mask >>= (16-this->capacity() ) * 8;
          value &= mask;
          switch(options->format)
            {
            case 'o':
              value_as_octal(stream, value);
              break;
            case 'x':
              value_as_hex(stream, value);
              break;
            case 't':
              value_as_binary(stream, value);
              break;
            }
          break;
          }

        default:
          {
          bool pointer_override = 
                    (options->format == 'd') || (options->format == 'u');
          char *display_me = format_for_display_internal(this,
                                                         this->data(),
                                                         this->capacity(),
                                                         pointer_override);
          if( options->format == 'u' && *display_me == '-' )
            {
            // Don't show the minus sign for print/u
            gdb_printf(stream, "%s", display_me+1);
            }
          else
            {
            gdb_printf(stream, "%s", display_me);
            }
          free(display_me);
          break;
          }
        }
      break;
      }

    case FldFloat:
      {
      switch( options->format )
        {
        case 'o':
        case 'x':
        case 't':
          {
          unsigned __int128 value = 0;
          memcpy(&value, this->data(), this->capacity());
          switch(options->format)
            {
            case 'o':
              value_as_octal(stream, value);
              break;
            case 'x':
              value_as_hex(stream, value);
              break;
            case 't':
              value_as_binary(stream, value);
              break;
            }

          break;
          }

        default:
          {
          char *display_me = format_for_display_internal(this,
                                                         this->data(),
                                                         this->capacity() );
          if( options->format == 'u' && *display_me == '-' )
            {
            // Don't show the minus sign for print/u
            gdb_printf(stream, "%s", display_me+1);
            }
          else
            {
            gdb_printf(stream, "%s", display_me);
            }
          free(display_me);
          break;
          }
        }
      break;
      }

    case FldClass:
      {
      cblc_field_t *parent_ = this->parent();
      gdb_assert(parent_);
      cobol_value_field parent(parent_);
      // We can't use .initial, because the capacity is zero
      char *list = cobol_fetch_string_from(this->initial_address());
      int compare_result = compare_field_class( &parent,
                                                parent.data(),
                                                parent.capacity(),
                                                list);
      if( compare_result )
        {
        gdb_printf(stream, "false");
        }
      else
        {
        gdb_printf(stream, "true");
        }
      break;
      }

    default:
      error("%s:%d: We don't know how to print a %s",
            __FILE__,
            __LINE__,
            cobol_language::field_type_as_string(field_type));
      break;
    }
  }

void
cobol_language::print_cblc_field_t(struct value *val,
                                   struct ui_file *stream,
                               const struct value_print_options *options) const
  {
  // Arriving here means we know that val is a cblc_field_t

  // Let's create our field from the value:
  cobol_value_field field(val);

  field.value_print(stream, options);
  }
  
void
cobol_language::print_string(struct value *val,
                             struct ui_file *stream,
                             const struct value_print_options *options) const
  {
  // Arriving here is a little bit of a stunt. I wrote this when there was 
  // exactly one variable -- the _cobol_entry_point -- that I needed to be able
  // to get at.  And because it was not a cblc_field_t, I gave it the name 
  // "string", and, well, here we are 

  CORE_ADDR addr = val->address();
  char *str = cobol_fetch_string_from(addr);
  gdb_printf(stream, "\"%s\"", str);
  free(str);
  }

void
cobol_language::walk_and_print( const struct block *block,
                                CobolVarNode *cvn,
                                int print_level,
                                struct ui_file *stream,
                                const struct value_print_options *options) const
  {
  struct symbol *sym = cvn->get_sym();
  if(cvn && sym)
    {
    value *val = evaluate_var_value (EVAL_NORMAL, block, sym);
    gdb_assert(val);
    cobol_value_field field(val);

    // Indent the display by the print level:
    for(int i=0; i<print_level; i++)
      {
      gdb_puts(" ", stream);
      }

    // Follow that with the COBOL variable level:
    gdb_printf(stream, "%2.2d ", field.level());

    // After that, we need the variable name:
    gdb_printf(stream, "%s ", cvn->get_name().c_str());

    // Follow that with the actual representation of the value:
    field.value_print(stream, options);

    // And end with a newline:
    gdb_puts("\n", stream);

    // Having completed our line, we need to invoke our children:

    for(size_t i=0; i<cvn->number_of_children(); i++)
      {
      walk_and_print( block,
                      cvn->get_child(i),
                      print_level + 1,
                      stream,
                      options);
      }
    }
  }

void
cobol_language::print_data_address(struct value *val,
    struct ui_file *stream,
    const struct value_print_options *options)
const
  {
  cobol_value_field variable(val);
  fprintf_styled(stream, address_style.style(), "0x%lx", variable.core_addr());

  }

void
cobol_language::print_cblc_field_t_heirarchical(struct value *val,
    struct ui_file *stream,
    const struct value_print_options *options)
const
  {
  // Arriving here means we know that val is a cblc_field_t.

  // Our mission is to print an heirarchical display of the COBOL data.  We
  // accomplish this by doing a depth-first traversal starting with our node:
  CobolVarNode *cvn = variable_blocks.get_node_from_value(val);
  gdb_assert(cvn);

  const struct block *expression_context_block = nullptr;
  CORE_ADDR expression_context_pc = 0;
  /* If no context specified, try using the current frame, if any.  */
  if( !expression_context_block )
    expression_context_block = get_selected_block (&expression_context_pc);

  gdb_printf(stream, "{\n");

  int initial_print_level = 0;
  walk_and_print( expression_context_block,
                  cvn,
                  initial_print_level,
                  stream,
                  options);

  gdb_printf(stream, "}");
  }

void
cobol_value_field::resolve_refer()
  {
  if( refer.is_dirty )
    {
    refer.is_dirty = false;

    refer.additional_offset = 0;
    if( refer.subscripts[0] != -1 )
      {
      // We have to process subscripts.

      // In order to do that, we have to be able to walk from this
      // variable up to our ultimate ancestor.

      const struct block *expression_context_block = nullptr;
      CORE_ADDR expression_context_pc = 0;
      /* If no context specified, try using the current frame, if any.  */
      if( !expression_context_block )
        expression_context_block = get_selected_block (&expression_context_pc);

      // Pick up our node to start with:
      CobolVarNode *cvn = variable_blocks.get_node_from_value(val);
      gdb_assert(cvn);

      // Find the index of the rightmost subscript:
      int nsubscript = MAXIMUM_TABLE_DIMENSIONS-1 ;
      for(;;)
        {
        // We know there is at least one, so this loop is safe
        if( refer.subscripts[nsubscript] != -1 )
          {
          break;
          }
        nsubscript -= 1;
        }

      // Loop through cvn and its ancestors until the root node is encountered
      while( cvn && cvn->get_sym() )
        {
        // See if the current node's value has an occurs_upper:
        struct symbol *sym = cvn->get_sym();
        gdb_assert(sym);
        value *val = evaluate_var_value (EVAL_NORMAL, expression_context_block, sym);
        gdb_assert(val);
        size_t occurs_upper = (size_t)value_as_long(
                                value_struct_elt(&val, {}, "occurs_upper", NULL, "cobol_field_t"));
        if( occurs_upper )
          {
          // We augment the additional_offset by this value's capacity times
          // the current subscript:
          size_t capacity = (size_t)value_as_long(
                              value_struct_elt(&val, {}, "capacity", NULL, "cobol_field_t"));
          if( nsubscript >= 0 )
            {
            int this_subscript = refer.subscripts[nsubscript];
            if( this_subscript < 1 || this_subscript > occurs_upper)
              {
              const char *ordinal = "???";
              switch(nsubscript+1)
                {
                case 1:
                  ordinal = "first";
                  break;
                case 2:
                  ordinal = "second";
                  break;
                case 3:
                  ordinal = "third";
                  break;
                case 4:
                  ordinal = "fourth";
                  break;
                case 5:
                  ordinal = "fifth";
                  break;
                case 6:
                  ordinal = "sixth";
                  break;
                case 7:
                  ordinal = "seventh";
                  break;
                }
              error("<error: The %s subscript is out-of-range>", ordinal);
              break;
              }
            refer.additional_offset += capacity * (this_subscript-1);
            }
          nsubscript -= 1;
          }
        cvn = cvn->get_parent();
        }

      // Because the number of subscripts has to match the number of nodes
      // with non-zero occurs_upper values, nsubscript, at this point, should
      // be -1
      if( nsubscript > -1 )
        {
        error (_("<error: Too many subscripts>"));
        }
      else if( nsubscript < -1 )
        {
        error (_("<error: Too few subscripts>"));
        }
      }

    if( refer.refmod_from )
      {
      refer.additional_offset += refer.refmod_from - 1;
      }
    }
  }

std::string
cobol_value_field::format_for_display()
  {
  char *display_me = format_for_display_internal(this,
                                                 this->data(),
                                                 this->capacity() );
  std::string retval = display_me;
  free(display_me);
  return retval;
  }

static int
expand_picture(char *dest, const char *picture)
  {
  // 'dest' must be of adequate length to hold the expanded picture string,
  // including any extra characters due to a CURRENCY SIGN expansion.
  int ch;
  int prior_ch = '\0';
  char *d = dest;
  const char *p = picture;

  long repeat;

  int currency_symbol = '\0';

  while( (ch = (*p++ & 0xFF) ) )
    {
    if( ch == '(' )
      {
      // Pick up the number after the left parenthesis
      char *endchar;
      repeat = strtol(p, &endchar, 10);

      // We subtract one because we know that the character just before
      // the parenthesis was already placed in dest
      repeat -= 1;

      // Update p to the character after the right parenthesis
      p = endchar + 1;
      while(repeat--)
        {
        *d++ = prior_ch;
        }
      }
    else
      {
      prior_ch = ch;
      *d++ = ch;
      }

    if( currency_signs[ch] )
      {
      // We are going to be mapping ch to a string in the final result:
      prior_ch = ch;
      currency_symbol = ch;
      }
    }

  size_t dest_length = d-dest;

  // We have to take into account the possibility that the currency symbol
  // mapping might be to a string of more than one character:

  if( currency_symbol )
    {
    size_t sign_length = strlen(currency_signs[currency_symbol]) - 1;
    if( sign_length )
      {
      char *pcurrency = strchr(dest, currency_symbol);
      gdb_assert(pcurrency);
      memmove(    pcurrency + sign_length,
                  pcurrency,
                  dest_length - (pcurrency-dest));
      for(size_t i=0; i<sign_length; i++)
        {
        pcurrency[i] = 'B';
        }
      dest_length += sign_length;
      }
    }

  return (int)(dest_length);
  }

void
cobol_language::string_to_alpha_edited( char *dest,
                                        const char *source,
                                        int slength,
                                        char *picture)
  {
  // Put the PICTURE into the data area.  If the caller didn't leave enough
  // room, well, poo on them.  Said another way; if they specify disaster,
  // disaster is what they will get.

  int destlength = expand_picture(dest, picture);

  int dindex = 0;
  int sindex = 0;

  while( dindex < destlength )
    {
    char dch = dest[dindex];
    char sch;
    switch(dch)
      {
      case 'b':   // 'B' gets replaced with SPACE
      case 'B':
        dest[dindex] = SPACE;
        break;

      case '0':   // These are left alone:
      case '/':
        break;

      default:
        // We assume that the parser isn't giving us a bad PICTURE
        // string, which means this character should be 'X', 'A', or '9'
        // We don't check; we just replace it:
        if(sindex < slength)
          {
          sch = source[sindex++];
          }
        else
          {
          sch = SPACE;
          }
        dest[dindex] = sch;
      }
    dindex += 1;
    }
  }

static int
Lindex(const char *dest, int length, char ch)
  {
  int retval = -1;
  for(int i=0; i<length; i++)
    {
    if( dest[i] == ch )
      {
      // Finds the leftmost
      retval = i;
      break;
      }
    }
  return retval;
  }

static int
Rindex(const char *dest, int length, char ch)
  {
  int retval = -1;
  for(int i=0; i<length; i++)
    {
    if( dest[i] == ch )
      {
      // Finds the rightmost
      retval = i;
      }
    }
  return retval;
  }

bool
cobol_language::string_to_numeric_edited(   char *dest,
                            char *source,
                            int rdigits,
                            int is_negative,
                            const char *picture)
  {
  // We need to expand the picture string.  We assume that the caller left
  // enough room in dest to take the expanded picture string.

  int dlength = expand_picture(dest, picture);

  // This is the length of the source string, which is all digits, and has
  // an implied decimal point at the rdigits place.  We assume that any
  // '.' or 'V' in the picture is in the right place
  const int slength = (int)strlen(source);

  // As a setting up exercise, let's deal with the possibility of a CR/DB:
  if( dlength >= 2 )
    {
    // It's a positive number, so we might have to get rid of a CR or DB:
    char ch1 = toupper(dest[dlength-2]);
    char ch2 = toupper(dest[dlength-1]);
    if(     (ch1 == 'D' && ch2 == 'B')
            ||  (ch1 == 'C' && ch2 == 'R') )
      {
      if( !is_negative )
        {
        // Per the spec, because the number is positive, those two
        // characters become blank:
        dest[dlength-2] = SPACE;
        dest[dlength-1] = SPACE;
        }
      // Trim the dlength by two to reflect those two positions at the
      // right edge, and from here on out we can ignore them.
      dlength -= 2;
      }
    }

  // We need to know if we have a currency picture symbol in this string:
  unsigned char currency_picture = '\0';        // This is the currency character in the PICTURE
  const char *currency_sign = NULL;   // This is the text we output when encountering
  //                                  // the currency_picture character
  // Note that the currency_picture can be upper- or lower-case, which is why
  // we can't treat dest[] to toupper.  That makes me sad, because I have
  // to do some tests for 'z' || 'Z', and so on.
  for(int i=0; i<dlength; i++)
    {
    int ch = (unsigned int)dest[i] & 0xFF;
    if( currency_signs[ch] )
      {
      currency_picture = ch;
      currency_sign = currency_signs[ch];
      break;
      }
    }

  // Calculate the position of the decimal point:
  int decimal_point_index = slength - rdigits;

  // Find the source position of the leftmost non-zero digit in source
  int nonzero_index;
  for(nonzero_index=0; nonzero_index<slength; nonzero_index++)
    {
    if( source[nonzero_index] != '0' )
      {
      break;
      }
    }

  bool is_zero = (nonzero_index == slength);

  // Push nonzero_index to the left to account for explicit '9' characters:
  int non_zero_characters = slength - nonzero_index;

  // Count up the number of nines
  int nines = 0;
  for(int i=0; i<dlength; i++)
    {
    if( dest[i] == '9' )
      {
      nines += 1;
      }
    }
  if( nines > non_zero_characters )
    {
    non_zero_characters = nines;
    nonzero_index = slength - non_zero_characters;
    if( nonzero_index < 0   )
      {
      nonzero_index = 0;
      }
    }
  // nonzero_index is now the location of the leftmost digit that we will
  // output as a digit.  Everything to its left is a leading zero, and might
  // get replaced with a floating replacement.

  // We are now in a position to address specific situations:

  // This is the case of leading zero suppression
  if( (strchr(picture, 'Z')) || (strchr(picture, 'z')) )
    {
    int leftmost_indexA = Lindex(dest, dlength, 'Z');
    int leftmost_indexB = Lindex(dest, dlength, 'z');
    if( leftmost_indexA == -1 )
      {
      leftmost_indexA = leftmost_indexB;
      }
    if( leftmost_indexB == -1 )
      {
      leftmost_indexB = leftmost_indexA;
      }

    int rightmost_indexA = Lindex(dest, dlength, 'Z');
    int rightmost_indexB = Lindex(dest, dlength, 'z');
    if( rightmost_indexA == -1 )
      {
      rightmost_indexA = leftmost_indexB;
      }
    if( rightmost_indexB == -1 )
      {
      rightmost_indexB = leftmost_indexA;
      }

    int leftmost_index  = std::min(leftmost_indexA,  leftmost_indexB);
    int rightmost_index = std::max(rightmost_indexA, rightmost_indexB);

    // We are doing replacement editing: leading zeroes get replaced with
    // spaces.
    if( is_zero && nines == 0 )
      {
      // Corner case:  The value is zero, and all numeric positions
      // are suppressed.  The result is all spaces:
      memset(dest, SPACE, dlength);
      }
    else
      {
      int index_s = slength-1;    // Index into source string of digits
      int index_d = dlength-1;    // Index into the destination
      bool reworked_string = false;

      while(index_d >=0)
        {
        // Pick up the destination character that we will replace:
        char ch_d = dest[index_d];

        if( ch_d == currency_picture )
          {
          // We are going to lay down the currency string.  Keep
          // in mind that our caller nicely left room for it
          size_t sign_len = strlen(currency_sign);
          while(sign_len > 0)
            {
            dest[index_d--] = currency_sign[--sign_len];
            }
          continue;
          }

        char ch_s;
        if( index_s < 0 )
          {
          // I don't think this can happen, but just in case:
          ch_s = '0';
          }
        else
          {
          ch_s = source[index_s];
          }

        if( index_s <= nonzero_index && !reworked_string)
          {
          reworked_string = true;
          // index_s is the location of the leftmost non-zero
          // digit.

          // So, we are about to enter the world of leading
          // zeroes.

          // The specification says, at this point, that
          // all B 0 / , and . inside the floating string
          // are to be considered part of the floating string:

          // So, we edit dest[] to make that true:
          int rlim = rightmost_index > index_d ? index_d : rightmost_index;

          for(int i=leftmost_index; i<rlim; i++)
            {
            if(     dest[i] == 'b'
                    ||  dest[i] == 'B'
                    ||  dest[i] == '/'
                    ||  dest[i] == '0'
                    ||  dest[i] == decimal_separator )
              {
              dest[i] = SPACE;
              }
            }
          // Any B 0 / , immediately to the right are
          // also part of the floating_character string

          for(int i=rlim+1; i<index_d; i++)
            {
            if( !(      dest[i] == 'b'
                        ||  dest[i] == 'B'
                        ||  dest[i] == '/'
                        ||  dest[i] == '0'
                        ||  dest[i] == decimal_separator))
              {
              break;
              }
            dest[i] = SPACE;
            }
          }


        if( index_s >= decimal_point_index )
          {
          // We are to the right of the decimal point, and so we
          // don't do any replacement.  We either insert a character,
          // or we replace with a digit:
          switch(ch_d)
            {
            // We are to the right of the decimal point, so Z is
            // a character position
            case 'z':
            case 'Z':
            case '9':
              index_s -= 1;
              break;
            case 'b':
            case 'B':
              ch_s = SPACE;
              break;
            case '+':
              if( !is_negative )
                {
                ch_s = '+';
                }
              else
                {
                ch_s = '-';
                }
              break;
            case '-':
              if( !is_negative )
                {
                ch_s = SPACE;
                }
              else
                {
                ch_s = '-';
                }
              break;
            case 'P':
            case 'p':
              // P-scaling has been handled by changing the value
              // and the number of rdigits, so these characters
              // are ignored here:
              break;
            default:
              // Valid possibilities are  0  /  ,
              // Just leave them be
              ch_s = ch_d;
              break;
            }
          dest[index_d] = ch_s;
          }
        else
          {
          // We are to the left of the decimal point:
          if( ch_d == decimal_point )
            {
            ch_d = '.';
            }
          switch(ch_d)
            {
            case '9':
              index_s -= 1;
              break;
            case '.':
            case 'v':
            case 'V':
              ch_s = decimal_point;
              break;
            case '+':
              if( !is_negative )
                {
                ch_s = '+';
                }
              else
                {
                ch_s = '-';
                }
              break;
            case '-':
              if( !is_negative )
                {
                ch_s = SPACE;
                }
              else
                {
                ch_s = '-';
                }
              break;

            case 'z':
            case 'Z':
              if( index_s < nonzero_index)
                {
                // We are in the leading zeroes, so they are
                // replaced with a space
                ch_s = SPACE;
                }
              index_s -= 1;
              break;

            case 'b':
            case 'B':
              ch_s = SPACE;
              break;

            default:
              // Valid possibilities are  0 / , which
              // at this point all get replaced with spaces:
              if( index_s < nonzero_index)
                {
                // We are in the leading zeroes, so they are
                // replaced with a space
                ch_s = SPACE;
                }
              else
                {
                // We still have digits to send out, so the output
                // is a copy of the PICTURE string
                ch_s = ch_d;
                }
            }
          dest[index_d] = ch_s;
          }

        index_d -= 1;
        }
      }
    }

  // This is the case of leading zero replacement:
  else if( strchr(picture, '*') )
    {
    int leftmost_index  = Lindex(dest, dlength, '*');
    int rightmost_index = Rindex(dest, dlength, '*');
    // We are doing replacement editing: leading zeroes get replaced with
    // asterisks, except that any decimal point is put into place:
    if( is_zero && nines == 0 )
      {
      // We need to re-initialize dest, because of the possibility
      // of a CR/DB at the end of the line
      dlength = expand_picture(dest, picture);

      for(int i=0; i<dlength; i++)
        {
        if(     dest[i] == 'v'
                ||  dest[i] == 'V'
                ||  dest[i] == decimal_point )
          {
          dest[i] = decimal_point;
          }
        else
          {
          dest[i] = '*';
          }
        }
      }
    else
      {
      int index_s = slength-1;    // Index into source string of digits
      int index_d = dlength-1;    // Index into the destination
      bool reworked_string = false;

      while(index_d >=0)
        {
        // Pick up the destination character that we will replace:
        char ch_d = dest[index_d];

        if( ch_d == currency_picture )
          {
          // We are going to lay down the currency string.  Keep
          // in mind that our caller nicely left room for it
          size_t sign_len = strlen(currency_sign);
          while(sign_len > 0)
            {
            dest[index_d--] = currency_sign[--sign_len];
            }
          continue;
          }

        char ch_s;
        if( index_s < 0 )
          {
          // I don't think this can happen, but just in case:
          ch_s = '0';
          }
        else
          {
          ch_s = source[index_s];
          }

        if( index_s <= nonzero_index && !reworked_string)
          {
          reworked_string = true;
          // index_s is the location of the leftmost non-zero
          // digit.

          // So, we are about to enter the world of leading
          // zeroes.

          // The specification says, at this point, that
          // all B 0 / , and . inside the floating string
          // are to be considered part of the floating string:

          // So, we edit dest[] to make that true:
          int rlim = rightmost_index > index_d ? index_d : rightmost_index;

          for(int i=leftmost_index; i<rlim; i++)
            {
            if(     dest[i] == 'b'
                    ||  dest[i] == 'B'
                    ||  dest[i] == '/'
                    ||  dest[i] == '0'
                    ||  dest[i] == decimal_separator )
              {
              dest[i] = '*';
              }
            }
          // Any B 0 / , immediately to the right are
          // also part of the floating_character string

          for(int i=rlim+1; i<index_d; i++)
            {
            if( !(      dest[i] == 'b'
                        ||  dest[i] == 'B'
                        ||  dest[i] == '/'
                        ||  dest[i] == '0'
                        ||  dest[i] == decimal_separator))
              {
              break;
              }
            dest[i] = '*';
            }
          }

        if( index_s >= decimal_point_index )
          {
          // We are to the right of the decimal point, and so we
          // don't do any replacement.  We either insert a character,
          // or we replace with a digit:
          switch(ch_d)
            {
            // We are to the right of the decimal point, so '*' is
            // a character position
            case '*':
            case '9':
              index_s -= 1;
              break;
            case 'b':
            case 'B':
              ch_s = SPACE;
              break;
            case '+':
              if( !is_negative )
                {
                ch_s = '+';
                }
              else
                {
                ch_s = '-';
                }
              break;
            case '-':
              if( !is_negative )
                {
                ch_s = SPACE;
                }
              else
                {
                ch_s = '-';
                }
              break;
            default:
              // Valid possibilities are  0  /  ,
              // Just leave them be
              ch_s = ch_d;
              break;
            }
          dest[index_d] = ch_s;
          }
        else
          {
          // We are to the left of the decimal point:
          if( ch_d == decimal_point )
            {
            ch_d = '.';
            }
          switch(ch_d)
            {
            case '9':
              index_s -= 1;
              break;
            case '.':
            case 'v':
            case 'V':
              ch_s = decimal_point;
              break;
            case '+':
              if( !is_negative )
                {
                ch_s = '+';
                }
              else
                {
                ch_s = '-';
                }
              break;
            case '-':
              if( !is_negative )
                {
                ch_s = SPACE;
                }
              else
                {
                ch_s = '-';
                }
              break;

            case '*':
              if( index_s < nonzero_index)
                {
                // We are in the leading zeroes, so they are
                // replaced with an asterisk
                ch_s = '*';
                }
              index_s -= 1;
              break;

            case 'b':
            case 'B':
              ch_s = SPACE;
              break;

            default:
              // Valid possibilities are  0 / , which
              // at this point all get replaced with spaces:
              if( index_s < nonzero_index)
                {
                // We are in the leading zeroes, so they are
                // replaced with our suppression character
                ch_s = '*';
                }
              else
                {
                // We still have digits to send out, so the output
                // is a copy of the PICTURE string
                ch_s = ch_d;
                }
            }
          dest[index_d] = ch_s;
          }

        index_d -= 1;
        }
      }
    }
  else
    {
    // At this point, we check for a floating $$, ++, or --
    unsigned char floating_character = 0;

    int leftmost_index;
    int rightmost_index;

    leftmost_index  = Lindex(dest, dlength, '+');
    rightmost_index = Rindex(dest, dlength, '+');
    if( rightmost_index > leftmost_index)
      {
      floating_character = '+';
      goto got_float;
      }

    leftmost_index  = Lindex(dest, dlength, '-');
    rightmost_index = Rindex(dest, dlength, '-');
    if( rightmost_index > leftmost_index)
      {
      floating_character = '-';
      goto got_float;
      }

    leftmost_index  = Lindex(dest, dlength, currency_picture);
    rightmost_index = Rindex(dest, dlength, currency_picture);
    if( rightmost_index > leftmost_index)
      {
      floating_character = currency_picture;
      goto got_float;
      }
got_float:

    if( floating_character )
      {
      if( is_zero && nines == 0 )
        {
        // Special case:
        memset(dest, SPACE, dlength);
        }
      else
        {
        const char *decimal_location = index(dest, decimal_point);
        if( !decimal_location )
          {
          decimal_location = index(dest, 'v');
          }
        if( !decimal_location )
          {
          decimal_location = index(dest, 'V');
          }
        if( !decimal_location )
          {
          decimal_location = dest + dlength;
          }
        int decimal_index = (int)(decimal_location - dest);

        if( rightmost_index > decimal_index )
          {
          rightmost_index = decimal_index -1;
          }

        int index_s = slength-1;    // Index into source string of digits
        int index_d = dlength-1;    // Index into the destination
        bool in_float_string = false;
        bool reworked_string = false;

        while(index_d >=0)
          {
          // Pick up the destination character that we will replace:
          unsigned char ch_d = dest[index_d];
          char ch_s;

          if( index_d == leftmost_index )
            {
            // At this point ch_d is the leftmost floating_character,
            // which means it *must* go into the output stream,
            // and that means we are truncating any remaining input.

            // Setting nonzero_index to be one character to the right
            // means that the following logic will think that any
            // source characters from here on out are zeroes
            nonzero_index = index_s+1;
            }

          if( ch_d != floating_character && ch_d == currency_picture )
            {
            // This is a non-floating currency_picture characger
            // We are going to lay down the currency string.  Keep
            // in mind that our caller nicely left room for it
            size_t sign_len = strlen(currency_sign);
            while(sign_len > 0)
              {
              dest[index_d--] = currency_sign[--sign_len];
              }
            continue;
            }
          if( ch_d != floating_character && ch_d == '+' )
            {
            // This is a non-floating '+'
            if( !is_negative )
              {
              ch_s = '+';
              }
            else
              {
              ch_s = '-';
              }
            dest[index_d--] = ch_s;
            continue;
            }
          if( ch_d != floating_character && ch_d == '-' )
            {
            // This is a non-floating '-'
            if( !is_negative )
              {
              ch_s = SPACE;
              }
            else
              {
              ch_s = '-';
              }
            dest[index_d--] = ch_s;
            continue;
            }

          if( index_s < 0 )
            {
            // I don't think this can happen, but just in case:
            ch_s = '0';
            }
          else
            {
            ch_s = source[index_s];
            if( index_s <= nonzero_index && !reworked_string)
              {
              reworked_string = true;
              // index_s is the location of the leftmost non-zero
              // digit.

              // So, we are about to enter the world of leading
              // zeroes.

              // The specification says, at this point, that
              // all B 0 / , and . inside the floating string
              // are to be considered part of the floating string:

              // So, we edit dest[] to make that true:
              int rlim = rightmost_index > index_d ? index_d : rightmost_index;

              for(int i=leftmost_index; i<rlim; i++)
                {
                if(     dest[i] == 'b'
                        ||  dest[i] == 'B'
                        ||  dest[i] == '/'
                        ||  dest[i] == '0'
                        ||  dest[i] == decimal_separator
                        ||  dest[i] == decimal_point )
                  {
                  dest[i] = floating_character;
                  }
                }
              // Any B 0 / , immediately to the right are
              // also part of the floating_character string

              for(int i=rlim+1; i<index_d; i++)
                {
                if( !(      dest[i] == 'b'
                            ||  dest[i] == 'B'
                            ||  dest[i] == '/'
                            ||  dest[i] == '0'
                            ||  dest[i] == decimal_separator))
                  {
                  break;
                  }
                dest[i] = floating_character;
                }
              }
            }
          if( index_s >= decimal_point_index )
            {
            // We are to the right of the decimal point, and so we
            // don't do any replacement.  We either insert a character,
            // or we replace with a digit:
            switch(ch_d)
              {
              case '9':
                index_s -= 1;
                break;
              case 'b':
              case 'B':
                ch_s = SPACE;
                break;
              default:
                if( ch_d == floating_character )
                  {
                  // We are laying down a digit
                  index_s -= 1;
                  }
                else
                  {
                  // Valid possibilities are  0  /  ,
                  // Just leave them be
                  ch_s = ch_d;
                  }
                break;
              }
            dest[index_d] = ch_s;
            }
          else
            {
            // We are to the left of the decimal point:

            if( ch_d == decimal_point )
              {
              ch_d = '.';
              }
            else if (ch_d == floating_character)
              {
              if( index_s < nonzero_index )
                {
                // We are in the leading zeroes.
                if( !in_float_string )
                  {
                  in_float_string = true;
                  // We have arrived at the rightmost floating
                  // character in the leading zeroes

                  if( floating_character == currency_picture )
                    {
                    size_t sign_len = strlen(currency_sign);
                    while(sign_len > 0)
                      {
                      dest[index_d--] = currency_sign[--sign_len];
                      }
                    continue;
                    }
                  if( floating_character  == '+' )
                    {
                    if( !is_negative )
                      {
                      ch_s = '+';
                      }
                    else
                      {
                      ch_s = '-';
                      }
                    dest[index_d--] = ch_s;
                    continue;
                    }
                  if( floating_character == '-' )
                    {
                    if( !is_negative )
                      {
                      ch_s = SPACE;
                      }
                    else
                      {
                      ch_s = '-';
                      }
                    dest[index_d--] = ch_s;
                    continue;
                    }
                  }
                else
                  {
                  // We are in the leading zeros and the
                  // floating character location is to our
                  // right.  So, we put down a space:
                  dest[index_d--] = SPACE;
                  continue;
                  }
                }
              else
                {
                // We hit a floating character, but we aren't
                // yet in the leading zeroes
                index_s -= 1;
                dest[index_d--] = ch_s;
                continue;
                }
              }

            switch(ch_d)
              {
              case '9':
                index_s -= 1;
                break;
              case '.':
              case 'v':
              case 'V':
                ch_s = decimal_point;
                break;
              case 'b':
              case 'B':
                ch_s = SPACE;
                break;

              default:
                // Valid possibilities are  0 / , which
                // at this point all get replaced with spaces:
                if( index_s < nonzero_index)
                  {
                  // We are in the leading zeroes, so they are
                  // replaced with our suppression character
                  ch_s = ' ';
                  }
                else
                  {
                  // We still have digits to send out, so the output
                  // is a copy of the PICTURE string
                  ch_s = ch_d;
                  }
              }
            dest[index_d] = ch_s;
            }

          index_d -= 1;
          }
        }
      }
    else
      {
      // Simple replacement editing
      int index_s = slength-1;    // Index into source string of digits
      int index_d = dlength-1;    // Index into the destination

      while(index_d >=0)
        {
        // Pick up the destination character that we will replace:
        char ch_d = dest[index_d];

        if( ch_d == currency_picture )
          {
          // We are going to lay down the currency string.  Keep
          // in mind that our caller nicely left room for it
          size_t sign_len = strlen(currency_sign);
          while(sign_len > 0)
            {
            dest[index_d--] = currency_sign[--sign_len];
            }
          continue;
          }

        char ch_s;
        if( index_s < 0 )
          {
          // I don't think this can happen, but just in case:
          ch_s = '0';
          }
        else
          {
          ch_s = source[index_s];
          }
        switch(ch_d)
          {
          // We are to the right of the decimal point, so Z is
          // a character position
          case '9':
            index_s -= 1;
            break;
          case 'b':
          case 'B':
            ch_s = SPACE;
            break;
          case '+':
            if( !is_negative )
              {
              ch_s = '+';
              }
            else
              {
              ch_s = '-';
              }
            break;
          case '-':
            if( !is_negative )
              {
              ch_s = SPACE;
              }
            else
              {
              ch_s = '-';
              }
            break;
          default:
            // Valid possibilities are  0  /  ,
            // Just leave whatever is here alone
            ch_s = ch_d;
            break;
          }
        dest[index_d--] = ch_s;
        }
      }
    }
  bool retval = false;
  return retval;
  }

