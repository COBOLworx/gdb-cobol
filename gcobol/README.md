# Incorporating COBOL into GDB

This directory was created for test and development of adding COBOL capability to GDB.  At the present writing, the master branch of binutils-gdb is on version 13

## Compiling the debugger

Because I want to be developing and debugging gdb,

	mkdir build
	cd build
	CFLAGS="-ggdb -O0" CXXFLAGS="-ggdb -O0" ../configure --prefix=/usr/local/gdb
	make -j8

On a four-core x64_86 the compilation takes about four minutes.

## Installing the debugger

I found that just running the resulting ./binutils-gdb/build/gdb/gdb executable resulted in all kinds of obscure failures.

This was ameliorated by running `make install`, which installed the program and all kinds of stuff into /usr/local/gdb.  After that, launching `./binutils-gdb/build/gdb/gdb` seemed to work pretty sensibly.

## Debugging the debugger

To facilitate debugging the version of gdb under development, on my system I created the alias

	alias ggg='gdb ~/repos/binutils-gdb/build/gdb/gdb'

On my system, that command launches the default `/usr/local/bin/gdb`, which is version 8.3.1.  That, in turn, loads `~/repos/binutils-gdb/build/gdb/gdb`, and displays the prompt `(top-gdb)`.  Entering the command `run` causes the development version gdb-13 to launch.  That version shows the prompt `(gdb)`.

Hats off to the gdb developers; somehow or other gdb recognizes when gdb is the inferior, and it takes appropriate action.
