# The assumption is that this Makefile is in <repos>/gdb-cobol/gcobol, which is
# adjacent to <repos>/gcc-cobol

GDB_TOP_LEVEL=$(shell git rev-parse --show-toplevel)
GCC_TOP_LEVEL=$(shell realpath $(GDB_TOP_LEVEL)/../gcc-cobol)

#
# BUILD_ROOT is the path to place where the build took place:
#
BUILD_ROOT ?= $(GCC_TOP_LEVEL)/build

# GCC_BIN is the relative path to the executables built by gcc-cobol/make
GCC_BIN = $(BUILD_ROOT)/gcc

ARCH=$(shell arch)

PREFIX_GCC = $(GCC_TOP_LEVEL)/build/gcc
PREFIX_LIB = $(GCC_TOP_LEVEL)/build/$(ARCH)-linux-gnu/libgcobol/.libs
PREFIX_GDB = $(GDB_TOP_LEVEL)/build/gdb
GCC    = xgcc
GPP    = xg++
GFORTRAN  = gfortran
GDB    = gdb
GCOBOL = gcobol
COBOL1 = cobol1
DEBUG ?= -ggdb -O0

LIBCOBOL_A=$(PREFIX_LIB)/libcobol.a
LIBCOBOL_PATH = $(dir $(LIBCOBOL_A))
LIBROOT = $(subst /libgcobol/.libs,,$(PREFIX_LIB))
LIBSTDC_PATH = $(LIBROOT)/libstdc++-v3/src/.libs
comma = ,
RPATH = $(addprefix -Wl$(comma)-rpath=,$(PREFIX_LIB) $(LIBSTDC_PATH))
COBOL_RUNTIME_LIBRARY = $(RPATH)

SEARCH_PATHS = \
 -B $(GCC_BIN) \
 -L $(LIBSTDC_PATH) \
 -L $(LIBCOBOL_PATH) \
 $(END)

.PHONY: all
all:
	@echo "These routines use the executables at $(PREFIX_GCC) and $(PREFIX_GDB)"
	@echo ' '
	@echo '"make cob"         compiles cobol.cbl with GCOBOL'
	@echo '"make cobd"        is the same, and launches the cobol executable using the gdb debugging chain'
	@echo '"make cobdd"       uses system GDB to debug GCOBOL as it compiles cobol.cbl'
    
	@echo '"make c"           compiles ctest.c with GCC'
	@echo '"make cd"          is the same, and launches the C executable using the gdb debugging chain'
	@echo '"make cc"          compiles ctest.cc with GCC'
	@echo '"make ccd"         is the same, and launches the C++ executable using the gdb debugging chain'
	@echo '"make f"           compiles ftest.f90 with GCC'
	@echo '"make fd"          is the same, and launches the FORTRAN executable using the gdb debugging chain'
	@echo 'The source files for cob, c, cd, and f can be overridded with "make cob SOURCE=other.cbl'
	@echo '"make clean"       removes intermediate and executable files in this directory'
	@echo '"make reconfigure" wipes and recreates ./build and runs ../configure for the debug version of GDB'
	@echo '"make b"           builds the debug version of gdb'

COBOL_SOURCE =$(SOURCE) 
FORTRAN_SOURCE =$(SOURCE) 
C_SOURCE =$(SOURCE) 
CC_SOURCE =$(SOURCE) 

ifeq ($(origin SOURCE), undefined)
    COBOL_SOURCE =cobol.cbl 
    C_SOURCE =ctest.c
    CC_SOURCE =ctest.cc
    FORTRAN_SOURCE =ftest.f90
endif

.PHONY: cob
cob: 
	$(GCC_BIN)/$(GCOBOL) $(SEARCH_PATHS) $(DEBUG) $(GCOPTIONS) -S -o $(basename $(COBOL_SOURCE)).s $(COBOL_SOURCE)
	$(GCC_BIN)/$(GCOBOL) $(SEARCH_PATHS) $(DEBUG) $(GCOPTIONS)    -o $(basename $(COBOL_SOURCE))   $(COBOL_SOURCE) \
    $(COBOL_RUNTIME_LIBRARY) -Wa,-a | sed -e '/^\f.*$$/d' -e '/^$$/d' >$(basename $(COBOL_SOURCE)).lst

.PHONY: cobd
cobd: cob
	$(GDB) -ex "set print thread-events off" -ex "run" --args $(PREFIX_GDB)/$(GDB) -ex "set print thread-events off" --data-directory=$(PREFIX_GDB)/data-directory --args $(basename $(COBOL_SOURCE))

.PHONY: cobdd
cobdd:
	gdb -q --args $(PREFIX_GCC)/$(COBOL1) $(GCOPTIONS) $(COBOL_SOURCE) \
        -quiet -dumpbase $(COBOL_SOURCE) -mtune=generic -march=x86-64 \
        -ggdb -O0 -version -o $(basename $(COBOL_SOURCE)).s

.PHONY: c
c:
	$(PREFIX_GCC)/$(GCC) -B $(PREFIX_GCC) -ggdb -O0 -S -o ctest.s ctest.c
	$(PREFIX_GCC)/$(GCC) -B $(PREFIX_GCC) -ggdb -O0    -o ctest   ctest.c -rdynamic -ldl

.PHONY: cd
cd: c
	$(GDB) -ex "run" --args $(PREFIX_GDB)/$(GDB) --data-directory=$(PREFIX_GDB)/data-directory --args ctest

.PHONY: cc
cc:
	$(PREFIX_GCC)/$(GPP) $(SEARCH_PATHS) -B $(PREFIX_GCC) -ggdb -O0 -S -o ctest.s ctest.cc
	$(PREFIX_GCC)/$(GPP) $(SEARCH_PATHS) -B $(PREFIX_GCC) -ggdb -O0    -o ctest   ctest.cc -rdynamic -ldl

.PHONY: ccd
ccd: cc
	$(GDB) -ex "run" --args $(PREFIX_GDB)/$(GDB) --data-directory=$(PREFIX_GDB)/data-directory --args ctest

.PHONY: f
f:
	$(PREFIX_GCC)/$(GFORTRAN) -B $(PREFIX_GCC) -ggdb -O0 -S -o ftest.s ftest.f90
	$(PREFIX_GCC)/$(GFORTRAN) -B $(PREFIX_GCC) -ggdb -O0    -o ftest   ftest.f90 -rdynamic -ldl

.PHONY: fd
fd: f
	$(GDB) -ex "run" --args $(PREFIX_GDB)/$(GDB) --data-directory=$(PREFIX_GDB)/data-directory --args ftest

ELF_FILES = $(shell find * -maxdepth 0 -exec file {} \; | grep ELF | sed 's/^\(.*\): .*$$/\1/g')

.PHONY: clean
clean:
	@rm -f $(ELF_FILES) *.s *.gimple *.tags *.o *.taglist dump.txt

.PHONY: b
b:
	$(MAKE) -C ../build -j12 YFLAGS=-Wno-yacc

.PHONY: test
test:
	$(MAKE) -C UAT test

