        IDENTIFICATION DIVISION.
        PROGRAM-ID. test.

        DATA DIVISION.
        WORKING-STORAGE SECTION.

        01 W1 PIC 9         VALUE  1 .
        01 W2 PIC 99        VALUE  2 .
        01 W3 PIC S99V9     VALUE -3 .
        01 W4 PIC $$,$$9.99 VALUE "    $1.23" .
        01 W5 PIC X(6)  VALUE "Shorty".
        01 W6 PIC X(82) VALUE "Longer ------------------------------------------------------------- alphanumeric".
        01 microns PIC PPP999.
        01 kilometers PIC 999PPP.

        01 OurTable.
          10 OuterTable OCCURS 10 TIMES.
            20 MiddleTable OCCURS 10 TIMES.
              30 InnerTable OCCURS 10 TIMES.
                40 Payload PIC b999.

        01 val PIC 9999 VALUE 0.
        01 i PIC 99.
        01 j PIC 99.
        01 k PIC 99.

        01 Alphax.
          05 Bravo    PIC X(5) VALUE "bravo".
          05 Charlie  PIC X(7) VALUE "charlie".
          05 Delta.
            10 Echox  PIC X(4) VALUE "echo".
            10 Foxtrot PIC X(7) VALUE "foxtrot".
            10 Golf.
                15 Hotel PIC X(5) VALUE "hotel".
                15 India PIC X(5) VALUE "india".
            10 Juliet  PIC X(6) VALUE "juliet".

        PROCEDURE DIVISION.
        PERFORM   varying i from 1 by 1 until i > 10
          PERFORM   varying   j from 1 by 1 until j > 10
            PERFORM   varying   k from 1 by 1 until k > 10
              move val to Payload( i j k )
              add 1 to val
              END-PERFORM
            END-PERFORM
          END-PERFORM
        MOVE 0.000123 TO microns
        MOVE 123000   TO kilometers
        DISPLAY Alphax.
        DISPLAY "The value of W1  is " W1
        DISPLAY "The value of W2  is " W2
        DISPLAY "The value of W3  is " W3
        DISPLAY "The value of W4  is " '"' W4 '"'
        DISPLAY "The value of W5  is " W5
        DISPLAY "The value of W6  is " W6
        DISPLAY "The value of microns    is " microns
        DISPLAY "The value of kilometers is " kilometers
        PERFORM BBB.
        PERFORM CCC.
        PERFORM DDD.
    AAA.
        DISPLAY "I am AAA".
    BBB.
        DISPLAY "I am BBB -- Line 1".
        DISPLAY "I am BBB -- Line 2".
        PERFORM CCC
        DISPLAY "I am BBB -- Line 3".
        DISPLAY "I am BBB -- Line 4".
    CCC.
        DISPLAY "I am CCC - Line 1".
        DISPLAY "I am CCC - Line 2".
    DDD.
        DISPLAY "I am DDD - Line 1".
        DISPLAY "I am DDD - Line 2".
    Dummy.
      *>  PERFORM BBB.

        DISPLAY "Thanks for playing".
        STOP RUN.

        END PROGRAM test.

