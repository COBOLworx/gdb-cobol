        IDENTIFICATION DIVISION.
        PROGRAM-ID. test.

        DATA DIVISION.
        WORKING-STORAGE SECTION.

        77 W1 PIC PPP9.
        77 W2 PIC 9PPP.
        77 W3 PIC X(6)  VALUE "Shorty".
        77 W4 PIC X(82) VALUE "Longer ------------------------------------------------------------- alphanumeric".
        77 W5 PIC 9999  VALUE 123.
        77 W6 PIC 9999V99 USAGE DISPLAY VALUE 123.4 .
        77 W7 PIC 9999V99 USAGE BINARY VALUE 123.4 .
        77 W8 PIC 9999V99 USAGE COMPUTATIONAL VALUE 123.4 .
        77 W9 PIC 9999V99 USAGE COMP-5 VALUE 123.4 .

        01 OurTable.
          10 OuterTable OCCURS 10 TIMES.
             20 MiddleTable OCCURS 10 TIMES.
                30 InnerTable OCCURS 10 TIMES.
                   40 Payload PIC 9999.

        01 i PIC 99.
        01 j PIC 99.
        01 k PIC 99.

        01 Alphax.
          05 Bravo    PIC X(5) VALUE "bravo".
          05 Charlie  PIC X(7) VALUE "charlie".
          05 Delta.
            10 Echox  PIC X(4) VALUE "echo".
            10 Foxtrot PIC X(7) VALUE "foxtrot".
            10 Golf.
                15 Hotel PIC X(5) VALUE "hotel".
                15 India PIC X(5) VALUE "india".
            10 Juliet  PIC X(6) VALUE "juliet".

        01 var-ne PIC $9,999.99 VALUE 1.23.
        01 val PIC 9999 VALUE 0.
        01 001-val PIC 9999 VALUE 1234.

        PROCEDURE DIVISION.
        PERFORM   varying i from 1 by 1 until i > 10
          PERFORM   varying   j from 1 by 1 until j > 10
            PERFORM   varying   k from 1 by 1 until k > 10
              move val to Payload(  i, j, k )

              add 1 to val

              END-PERFORM
            END-PERFORM
          END-PERFORM
        MOVE .0001 to W1
        MOVE 1000 TO W2.

        DISPLAY Alphax.
        move 1.23 to var-ne.
        DISPLAY var-ne.

        DISPLAY "Thanks for playing1".





        DISPLAY "Thanks for playing2".
        




        PERFORM ABCDEFG







        CALL "test2".

        STOP RUN.

        ABCDEFG.
            DISPLAY "I am PARAGRAPH ABCDEFG - 1".
            DISPLAY "I am PARAGRAPH ABCDEFG - 2".
            DISPLAY "I am PARAGRAPH ABCDEFG - 3".
            DISPLAY "I am PARAGRAPH ABCDEFG - 4".

        IDENTIFICATION DIVISION.
        PROGRAM-ID. test2.

        DATA DIVISION.
        WORKING-STORAGE SECTION.
        01 RunningTotal   PIC 9(5) VALUE ZERO.
        01 PrnTotal       PIC ZZ,ZZ9.

        PROCEDURE DIVISION.
        First-Section SECTION.
        ABCD-EFG.
            DISPLAY W3
            DISPLAY "test2 says: Thank you, thank you very much".
            EXIT PROGRAM.
        SecondSection SECTION.
            Dummy.
        END PROGRAM test2.
        END PROGRAM test.

