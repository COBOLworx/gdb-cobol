ID DIVISION.
PROGRAM-ID. cl10.
ENVIRONMENT DIVISION.
INPUT-OUTPUT SECTION.
  FILE-CONTROL.
    SELECT pgm-input ASSIGN TO input-file-name
      ORGANIZATION IS LINE SEQUENTIAL.

DATA DIVISION.
FILE SECTION.
  FD pgm-input.
    01 input-record pic x(200).
    01 input-data redefines input-record.
     05 connection  PIC 9(7) VALUE ZERO.
     05 FILLER      PIC X VALUE " ".
     05 operation   PIC 9(7) VALUE ZERO.
     05 FILLER      PIC X VALUE " ".
     05 line-number PIC 9(7) VALUE ZERO.
     05 FILLER      PIC X VALUE " ".
     05 inverb      PIC X(12) VALUE SPACE.
     05 FILLER      PIC X VALUE " ".
     05 tag         PIC 999 VALUE ZERO.
     05 FILLER      PIC X VALUE " ".
     05 retcode     PIC 999 VALUE ZERO.
     05 FILLER      PIC X VALUE " ".
     05 nentries    PIC 999999 VALUE ZERO.
     05 FILLER      PIC X VALUE " ".
     05 qtime       PIC 999v999999 VALUE ZERO.
     05 FILLER      PIC X VALUE " ".
     05 etime       PIC 999v999999 VALUE ZERO.
     05 FILLER      PIC X VALUE " ".
     05 thetext     PIC X(80) VALUE SPACE. *> length is conjectural

WORKING-STORAGE SECTION.
  01 file-related-variables.
     05 input-file-name pic X(80) value "mdt.t2".
     05 input-file-status PIC x VALUE SPACE.
       88 end-of-input VALUE   high-value
          when set TO false is low-value.
  01 process-related-variables.
    05 last-conn pic 9(7) value spaces.
    05 last-op   pic 9(7) value spaces.
  01 output-record.
     05 line-number PIC 9(7) VALUE ZERO.
     05 FILLER      PIC X VALUE " ".
     05 connection  PIC 9(7) VALUE ZERO.
     05 FILLER      PIC X VALUE " ".
     05 operation   PIC 9(7) VALUE ZERO.
     05 FILLER      PIC X VALUE " ".
     05 outverb     PIC X(12) VALUE SPACE.
     05 FILLER      PIC X VALUE " ".
     05 tag         PIC 999 VALUE ZERO.
     05 FILLER      PIC X VALUE " ".
     05 retcode     PIC 999 VALUE ZERO.
     05 FILLER      PIC X VALUE " ".
     05 nentries    PIC 999999 VALUE ZERO.
     05 FILLER      PIC X VALUE " ".
     05 qtime       PIC 999.999999 VALUE ZERO.
     05 FILLER      PIC X VALUE " ".
     05 etime       PIC 999.999999 VALUE ZERO.
     05 FILLER      PIC X VALUE " ".
     05 thetext     PIC X(80) VALUE SPACE. *> length is conjectural
     05 out-lineno  PIC 9(7) VALUE ZERO.
     05 FILLER      PIC X VALUE " ".

PROCEDURE DIVISION.
MAIN-PARAGRAPH.
  PERFORM do-initialization.
  PERFORM do-read.
  PERFORM until end-of-input
    PERFORM process-line
    PERFORM do-read
  END-PERFORM.
  CLOSE pgm-input.
  GOBACK.

do-initialization.
  SET end-of-input TO FALSE.
  OPEN INPUT pgm-input.
  IF FILE-STATUS NOT EQUAL TO ZERO THEN
    DISPLAY "Couldn't open the file"
    STOP RUN
    END-IF.

do-read.
  READ pgm-input AT END SET end-of-input TO TRUE.

process-line.
  IF inverb in input-data = "RESULT" THEN 
    IF outverb IN output-record IS NOT EQUAL SPACES THEN
      PERFORM do-result
    ELSE
      EXIT PARAGRAPH
    END-IF
    EXIT PARAGRAPH
  END-IF.
  IF inverb IN input-data(1:3) IS EQUAL "be_" THEN
    PERFORM do-be
    EXIT PARAGRAPH
  END-IF.
  *> NOT RESULT or be_ ... for now "MUST" be an op
  PERFORM do-op.

do-op.
  if last-conn equals connection in input-data and
    last-op = operation in input-data 
  THEN
    exit paragraph  *> a line with addition data of no interest
  ELSE  *> Not the same as the last (normal)
    if outverb in output-record NOT EQUAL TO SPACES
    THEN
      initialize output-record
    END-IF
  END-IF.
  move corresponding input-data to output-record.
  move inverb to outverb.

do-result.
  move line-number in input-data to out-lineno in output-record.
  move retcode in input-data to retcode in output-record.
  move tag in input-data to tag in output-record.
  move qtime in input-data to qtime in output-record.
  move etime in input-data to etime in output-record.
  move thetext in input-data to thetext in output-record.
  display output-record.

do-be.
  move corresponding input-data to output-record.
  move line-number in input-data to out-lineno in output-record.
  move inverb to outverb.
  display output-record.

END PROGRAM cl10.
