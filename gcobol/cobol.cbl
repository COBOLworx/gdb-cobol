        identification      division.
        program-id.         prog.
        procedure division.
        declaratives.
        declaratives-ec-all section.
            use after exception condition ec-all.
                display "      declarative for ec-all".
           end declaratives.
        main section.
        call "program-id-1"
        call "PROG-2"
        call "prog3"
        goback.
        end program         prog.

        identification      division.
        program-id.         program-id-1.
        procedure division.
        display "I" space with no advancing
        display "am" space with no advancing
        display "program" space with no advancing
        display "one"
        continue.
        quit.
        goback.
        end program         program-id-1.

        identification      division.
        program-id.         Prog-2.
        procedure division.
        display "I" space with no advancing
        display "am" space with no advancing
        display "program" space with no advancing
        display "two"
        continue.
        quit.
        goback.
        end program         prog-2.

        identification      division.
        program-id.         prog3.
        procedure division.
        display "I" space with no advancing
        display "am" space with no advancing
        display "program" space with no advancing
        display "three"
        continue.
        quit.
        goback.
        end program         prog3.
