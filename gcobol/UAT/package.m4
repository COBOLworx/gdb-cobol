# Package.m4 for GDB-COBOL

# Signature of the current package.
m4_define([AT_PACKAGE_NAME],      [GDB-COBOL])
# TODO - definitive TARNAME and Version (replacing preview)
m4_define([AT_PACKAGE_TARNAME],   [gdb-cobol.tar])
m4_define([AT_PACKAGE_VERSION],   [preview])
m4_define([AT_PACKAGE_STRING],    [GDB-COBOL])
m4_define([AT_PACKAGE_BUGREPORT], [bugs@cobolworx.com])
m4_define([AT_PACKAGE_URL],       [https://www.cobolworx.com])

